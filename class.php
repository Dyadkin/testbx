<?php require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");
use Bitrix\Highloadblock\HighloadBlockTable as HLBT;
use Bitrix\Main\Loader;
/**
 * Class setnews
 * CBitrixComponent - является оболочкой компонента. Для каждого подключаемого компонента создаётся свой экземпляр класса
 */
class SetNews extends CBitrixComponent
{
    public function executeComponent()
    {
        Loader::includeModule('iblock');
        Loader::includeModule('highloadblock');

        $elements = [];
        $id_hl = 5;

        //запрос основных парметров инфоблока--------------------------------------------------------------------------
        $news = CIBlockElement::GetList(
            [
                'CREATED' => "ASC",
            ],
            [
                'IBLOCK_CODE' => 'news',
                ">=" . 'PROPERTY_RATING' => (int)$_GET['RATING'],
            ],
            false,
            [
                "nPageSize" => 3,
            ],
            ['ID', 'IBLOCK_ID', 'NAME',
                'PREVIEW_PICTURE', 'PREVIEW_TEXT',
                'PREVIEW_PICTURE', 'DETAIL_TEXT',
                'TIMESTAMP_X', 'PROPERTY_RATING',
                'PROPERTY_TAGS', 'CREATED_USER_NAME'
            ]
        );

        $news->NavStart(0);

        while ($element = $news->GetNextElement()) {
            $array['FIELDS'] = $element->GetFields();
            $properties = $element->GetProperties();
            //$properties['TAGS']['VALUE']
            //$elem[] = $array['FIELDS']['PROPERTY_TAGS_VALUE'];
            $array['FIELDS']['PREVIEW_PICTURE'] = CFile::GetPath($array['FIELDS']['PREVIEW_PICTURE']);
            $elements[] = $array;
        }

        foreach ($elements as $elem) {
             $curTags = [];
            foreach ($elem['FIELDS']['PROPERTY_TAGS_VALUE'] as $tagId) {
                $tagIds[] = $tagId;
                $curTags[] = $tagId;
            }
               $res[$elem['FIELDS']['ID']] = $curTags;
        }
        $result = array_unique($tagIds);
        ?><pre><?// print_r($res); ?></pre><?

        $hlblock = HLBT::getById($id_hl)->fetch();
        $entity = HLBT::compileEntity($hlblock);
        $entity_data_class = $entity->getDataClass();

        $rsData = $entity_data_class::getList(array(
            'select' => array('UF_LINK'),
            'filter' => array('UF_XML_ID' => $tagIds),
        ));

        $tags = [];

        while ($el = $rsData->Fetch()) {

                $tags[] = $el;
        }

        $revTags = array_reverse($tags);
        ?><pre><?//var_dump($revTags); ?></pre><?

        foreach ( $revTags as $tag){
            foreach ($tag as $val){
                $links[] =$val;
            }
        }

        $resultTags = array_combine((array_values($result)), $links);

        $res['VALUE'] = $resultTags;

        ?><pre><?var_dump($res); ?></pre><?


        $this->arResult['elements'] = $elements;
        $this->arResult['nav'] = $news;
        $this->arResult['tags'] = $res;
        $this->arResult['links'] =  $resultTags;

        $this->includeComponentTemplate();

    }
}


