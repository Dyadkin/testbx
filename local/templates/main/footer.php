</div>
<div class="sidebar_right js-sidebar_right sidebar_content-area">
    <!-- /235032688/HH/HH01_ATF_Poster -->
    <div class="dfp-slot dfp-slot_top is_visible">
        <div class="dfp-slot__banner" id="div-gpt-hh-atf">
            <script>
                window.display_dfp_slot('div-gpt-hh-atf');
            </script>
        </div>
        <div class="dfp-slot__placeholder">
            <span class="dfp-slot__placeholder-text">Экспресс среди ивентов: TechTrain прибывает на IT-платформу<br/><br/> <a href="https://u.tmtm.ru/a-jug-itp-080819" class="btn btn_large btn_blue" style="display:inline-flex!important;" target="_blank">Запрыгнуть в поезд</a></span>
        </div>
        <a href="https://tmtm.ru/services/advertising/" target="_blank" class="dfp-slot__label">Реклама</a>

    </div>

    <div class="default-block default-block_sidebar">
        <div class="default-block__header">
            <h3 class="default-block__header-title">Спонсоры сообщества</h3>
        </div>

        <div class="default-block__content">
            <ul class="content-list content-list_partners-block">
                <li class="content-list__item content-list__item_partners-block">
                    <a href="https://u.tmtm.ru/avito_sponsor" rel="nofollow" class="partner-info">
                        <div class="partner-info__head">
                            <span class="partner-info__title">Авито</span>
                            <img src="https://habrastorage.org/webt/ak/c6/dj/akc6djcfuaqk9mroyelve5gdnl0.png" class="partner-info__image">
                        </div>

                        <div class="partner-info__description">
                            Главные по&nbsp;объявлениям в&nbsp;России. Укрощают высокие нагрузки.
                        </div>
                    </a>
                </li>
                <li class="content-list__item content-list__item_partners-block">
                    <a href="https://u.tmtm.ru/ruvds_sponsor" rel="nofollow" class="partner-info">
                        <div class="partner-info__head">
                            <span class="partner-info__title">RUVDS</span>
                            <img src="https://habrastorage.org/getpro/habr/company/5f8/ece/f58/5f8ecef58d48004476cb1e01c12f914e.png" class="partner-info__image">
                        </div>

                        <div class="partner-info__description">
                            Облачный провайдер. Поддерживает нас во&nbsp;всех начинаниях, но&nbsp;часто втягивает в&nbsp;авантюры.
                        </div>
                    </a>
                </li>
                <li class="content-list__item content-list__item_partners-block ">
                    <a href="https://u.tmtm.ru/tochka_sponsor" rel="nofollow" class="partner-info">
                        <div class="partner-info__head">
                            <span class="partner-info__title">Точка&nbsp;&mdash; банк для&nbsp;предпринимателей</span>
                            <img src="https://habrastorage.org/webt/ua/lg/h-/ualgh-uxdjkhu1ldizzbg1pclaw.jpeg" class="partner-info__image">
                        </div>

                        <div class="partner-info__description">
                            Помогает международному сотрудничеству айтишников своими валютными счетами.
                        </div>
                    </a>
                </li>
            </ul>
        </div>

        <div class="default-block__footer">
            <a href="https://habr.com/ru/info/help/sponsorship-info/" class="default-block__footer_link">Как стать спонсором?</a>
        </div>
    </div>


    <div class="default-block default-block_sidebar">
        <div class="default-block__header">
            <h3 class="default-block__header-title">Вклад авторов</h3>
        </div>
        <div class="default-block__content">
            <ul class="content-list сontent-list_top-users">
                <li class="content-list__item content-list__item_top-users">
                    <a href="https://habr.com/ru/users/pronskiy/" class="media-obj media-obj_link">
                        <div class="media-obj__image">
                            <img src="//habrastorage.org/getpro/habr/avatars/4ff/b7e/1e6/4ffb7e1e672b59a5d54622150d6d1039.jpg" class="user-pic__img" width="30" height="30" />
                        </div>

                        <div class="media-obj__body">
                            <div class="rating-info rating-info_top-1">
                                <span class="rating-info__title">pronskiy</span>
                                <span class="rating-info__stat">7198.8</span>
                            </div>
                            <div class="rating-info__progress">
                                <div class="rating-info__progress-scale" style="width:100%;"></div>
                            </div>
                        </div>
                    </a>
                </li>
                <li class="content-list__item content-list__item_top-users">
                    <a href="https://habr.com/ru/users/AloneCoder/" class="media-obj media-obj_link">
                        <div class="media-obj__image">
                            <img src="//habrastorage.org/getpro/habr/avatars/741/45e/bea/74145ebeab7f222cce402aed2683f9d7.png" class="user-pic__img" width="30" height="30" />
                        </div>

                        <div class="media-obj__body">
                            <div class="rating-info rating-info_top-2">
                                <span class="rating-info__title">AloneCoder</span>
                                <span class="rating-info__stat">1688.2</span>
                            </div>
                            <div class="rating-info__progress">
                                <div class="rating-info__progress-scale" style="width:23%;"></div>
                            </div>
                        </div>
                    </a>
                </li>
                <li class="content-list__item content-list__item_top-users">
                    <a href="https://habr.com/ru/users/AntonShevchuk/" class="media-obj media-obj_link">
                        <div class="media-obj__image">
                            <img src="//habrastorage.org/getpro/habr/avatars/fe2/421/237/fe2421237f78146f904b2c4659e9c447.jpg" class="user-pic__img" width="30" height="30" />
                        </div>

                        <div class="media-obj__body">
                            <div class="rating-info rating-info_top-3">
                                <span class="rating-info__title">AntonShevchuk</span>
                                <span class="rating-info__stat">815</span>
                            </div>
                            <div class="rating-info__progress">
                                <div class="rating-info__progress-scale" style="width:11%;"></div>
                            </div>
                        </div>
                    </a>
                </li>
                <li class="content-list__item content-list__item_top-users">
                    <a href="https://habr.com/ru/users/SamDark/" class="media-obj media-obj_link">
                        <div class="media-obj__image">
                            <img src="//habrastorage.org/getpro/habr/avatars/2df/fce/7ee/2dffce7ee42c7b79cce513e382cec05c.jpg" class="user-pic__img" width="30" height="30" />
                        </div>

                        <div class="media-obj__body">
                            <div class="rating-info rating-info_top-4">
                                <span class="rating-info__title">SamDark</span>
                                <span class="rating-info__stat">760</span>
                            </div>
                            <div class="rating-info__progress">
                                <div class="rating-info__progress-scale" style="width:11%;"></div>
                            </div>
                        </div>
                    </a>
                </li>
                <li class="content-list__item content-list__item_top-users">
                    <a href="https://habr.com/ru/users/youROCK/" class="media-obj media-obj_link">
                        <div class="media-obj__image">
                            <img src="//habrastorage.org/getpro/habr/avatars/371/51f/213/37151f213dce4543faf649cbb6522063.jpg" class="user-pic__img" width="30" height="30" />
                        </div>

                        <div class="media-obj__body">
                            <div class="rating-info rating-info_top-5">
                                <span class="rating-info__title">youROCK</span>
                                <span class="rating-info__stat">666</span>
                            </div>
                            <div class="rating-info__progress">
                                <div class="rating-info__progress-scale" style="width:9%;"></div>
                            </div>
                        </div>
                    </a>
                </li>
                <li class="content-list__item content-list__item_top-users">
                    <a href="https://habr.com/ru/users/zapimir/" class="media-obj media-obj_link">
                        <div class="media-obj__image">
                    <span class="default-image default-image_small default-image_green">
                      <svg class="icon-svg" width="24" height="24" viewBox="0 0 24 24" aria-hidden="true" version="1.1" role="img"><path d="M21.5 24h-19c-1.379 0-2.5-1.122-2.5-2.5v-19c0-1.379 1.122-2.5 2.5-2.5h19c1.379 0 2.5 1.122 2.5 2.5v19c0 1.379-1.122 2.5-2.5 2.5zm-19-23c-.827 0-1.5.673-1.5 1.5v19c0 .827.673 1.5 1.5 1.5h19c.827 0 1.5-.673 1.5-1.5v-19c0-.827-.673-1.5-1.5-1.5h-19zM15.598 12.385zM19.438 15.417l-.002-.005v-.001c-.875-2.226-2.484-3.054-3.445-3.549l-.273-.143c.029-.497-.025-1.034-.167-1.599l-.128.032.123-.044c-.765-2.152-1.757-2.585-2.632-2.967l-.006-.003-.535-2.121c.357-.065.628-.375.628-.752.001-.423-.342-.765-.765-.765s-.766.342-.766.765c0 .358.248.657.581.74l-.825 1.654-.014-.003-.024-.003c-1.053-.033-1.842.369-2.5.947-.633-.322-1.515-.729-2.158-1.814.107-.12.174-.276.174-.45 0-.375-.303-.678-.678-.678s-.678.303-.678.678.303.678.678.678l.221-.04c.416.597 1.09 1.181 1.347 2.828l-.072.091.104.081-.112-.067c-1.157 1.914-.793 4.248.207 5.37-.998 2.546-1.035 4.681-.097 5.868l.002.002.003.003c.119.162.313.233.524.233.189 0 .39-.057.559-.154.312-.179.441-.459.326-.713l-.12.054.119-.056c-.581-1.243-.474-2.713.314-4.37.4.131.778.208 1.145.234l.139.73c.264 1.418.514 2.757 1.297 4.006.132.264.453.387.777.387.122 0 .245-.018.357-.051.385-.116.591-.399.537-.738l-.129.021.125-.042c-.204-.606-.431-1.146-.649-1.67-.373-.894-.725-1.742-.891-2.737.407-.042.797-.129 1.161-.261.825.692 1.661 1.492 2.743 3.406h.001c.072.14.224.215.41.215.105 0 .222-.024.339-.073.365-.155.652-.531.477-1.006v-.001c-.432-1.849-1.426-2.778-2.428-3.547.162-.175.311-.366.442-.576.75.399 1.878 1.005 3.127 2.766l.047.067.011-.008c.151.156.317.24.48.24.096 0 .191-.027.279-.084.306-.194.439-.662.29-1.005zm-8.878-2.493c-.947 0-1.713-.767-1.713-1.713s.767-1.713 1.713-1.713c.947 0 1.713.767 1.713 1.713s-.767 1.713-1.713 1.713zm6.587 4.648l-.084.021v-.001l.084-.02zm-2.007-5.312zm.022 1.006zM11.225 11.604c0 .385-.312.697-.697.697s-.697-.312-.697-.697c0-.385.312-.697.697-.697s.697.312.697.697z"/></svg>
                    </span>
                        </div>

                        <div class="media-obj__body">
                            <div class="rating-info rating-info_top-6">
                                <span class="rating-info__title">zapimir</span>
                                <span class="rating-info__stat">559</span>
                            </div>
                            <div class="rating-info__progress">
                                <div class="rating-info__progress-scale" style="width:8%;"></div>
                            </div>
                        </div>
                    </a>
                </li>
                <li class="content-list__item content-list__item_top-users">
                    <a href="https://habr.com/ru/users/alexzfort/" class="media-obj media-obj_link">
                        <div class="media-obj__image">
                            <img src="//habrastorage.org/getpro/habr/avatars/d2a/e8a/5ac/d2ae8a5ac9876f838ebdd45f3ea3bc3b.png" class="user-pic__img" width="30" height="30" />
                        </div>

                        <div class="media-obj__body">
                            <div class="rating-info rating-info_top-7">
                                <span class="rating-info__title">alexzfort</span>
                                <span class="rating-info__stat">558</span>
                            </div>
                            <div class="rating-info__progress">
                                <div class="rating-info__progress-scale" style="width:8%;"></div>
                            </div>
                        </div>
                    </a>
                </li>
                <li class="content-list__item content-list__item_top-users">
                    <a href="https://habr.com/ru/users/gnomeby/" class="media-obj media-obj_link">
                        <div class="media-obj__image">
                            <img src="//habrastorage.org/getpro/habr/avatars/f38/995/2ce/f389952ce5920bdf77a34835500f142d.png" class="user-pic__img" width="30" height="30" />
                        </div>

                        <div class="media-obj__body">
                            <div class="rating-info rating-info_top-8">
                                <span class="rating-info__title">gnomeby</span>
                                <span class="rating-info__stat">483</span>
                            </div>
                            <div class="rating-info__progress">
                                <div class="rating-info__progress-scale" style="width:7%;"></div>
                            </div>
                        </div>
                    </a>
                </li>
                <li class="content-list__item content-list__item_top-users">
                    <a href="https://habr.com/ru/users/iGusev/" class="media-obj media-obj_link">
                        <div class="media-obj__image">
                            <img src="//habrastorage.org/getpro/habr/avatars/398/182/212/39818221233f641a20d067615a7e0e01.jpg" class="user-pic__img" width="30" height="30" />
                        </div>

                        <div class="media-obj__body">
                            <div class="rating-info rating-info_top-9">
                                <span class="rating-info__title">iGusev</span>
                                <span class="rating-info__stat">466.2</span>
                            </div>
                            <div class="rating-info__progress">
                                <div class="rating-info__progress-scale" style="width:6%;"></div>
                            </div>
                        </div>
                    </a>
                </li>
                <li class="content-list__item content-list__item_top-users">
                    <a href="https://habr.com/ru/users/phpclub/" class="media-obj media-obj_link">
                        <div class="media-obj__image">
                            <img src="//habrastorage.org/getpro/habr/avatars/a0a/877/7f0/a0a8777f02da3892635eafbd61cb31f0.jpg" class="user-pic__img" width="30" height="30" />
                        </div>

                        <div class="media-obj__body">
                            <div class="rating-info rating-info_top-10">
                                <span class="rating-info__title">phpclub</span>
                                <span class="rating-info__stat">447</span>
                            </div>
                            <div class="rating-info__progress">
                                <div class="rating-info__progress-scale" style="width:6%;"></div>
                            </div>
                        </div>
                    </a>
                </li>
            </ul>
        </div>
        <div class="default-block__footer">
            <a href="https://habr.com/ru/hub/php/subscribers/invest/" class="default-block__footer-link" rel="nofollow">100 лучших авторов</a>
        </div>
    </div>



    <div class="js-ad_sticky">
        <div class="default-block">
            <div class="default-block__header">
                <h3 class="default-block__header-title">Читают сейчас</h3>
            </div>

            <div class="default-block__content default-block__content_most-read" id="broadcast_most-read">
                <ul class="content-list content-list_most-read">
                    <li class="content-list__item content-list__item_devided post-info">
                        <a href="https://habr.com/ru/post/464603/" class="post-info__title" onclick="if (typeof ga === 'function') { ga('send', 'event', 'tm_block', 'most_read', 'sidebar', '1'); }">Алкоголизм последней стадии</a>
                        <div class="post-info__meta">
                    <span class="post-info__meta-item">
                      <svg class="post-info__meta-icon icon-svg_views" width="16" height="9"><use xlink:href="https://habr.com/images/1566547990/common-svg-sprite.svg#eye" /></svg><span class="post-info__meta-counter post-info__meta-counter_small">77,8k</span>
                    </span>
                            <a href="https://habr.com/ru/post/464603/#comments" class="post-info__meta-item" rel="nofollow">
                                <svg class="post-info__meta-icon icon-svg_comments" width="14" height="13"><use xlink:href="https://habr.com/images/1566547990/common-svg-sprite.svg#comment" /></svg><span class="post-info__meta-counter post-info__meta-counter_small">314</span>
                            </a>
                        </div>
                    </li>
                    <li class="content-list__item content-list__item_devided post-info">
                        <a href="https://habr.com/ru/post/464595/" class="post-info__title" onclick="if (typeof ga === 'function') { ga('send', 'event', 'tm_block', 'most_read', 'sidebar', '2'); }">Алкоголь и математик(а)</a>
                        <div class="post-info__meta">
                    <span class="post-info__meta-item">
                      <svg class="post-info__meta-icon icon-svg_views" width="16" height="9"><use xlink:href="https://habr.com/images/1566547990/common-svg-sprite.svg#eye" /></svg><span class="post-info__meta-counter post-info__meta-counter_small">45k</span>
                    </span>
                            <a href="https://habr.com/ru/post/464595/#comments" class="post-info__meta-item" rel="nofollow">
                                <svg class="post-info__meta-icon icon-svg_comments" width="14" height="13"><use xlink:href="https://habr.com/images/1566547990/common-svg-sprite.svg#comment" /></svg><span class="post-info__meta-counter post-info__meta-counter_small">302</span>
                            </a>
                        </div>
                    </li>
                    <li class="content-list__item content-list__item_devided post-info">
                        <a href="https://habr.com/ru/news/t/464669/" class="post-info__title" onclick="if (typeof ga === 'function') { ga('send', 'event', 'tm_block', 'most_read', 'sidebar', '3'); }">АЗАПИ хочет навечно заблокировать Internet Archive</a>
                        <div class="post-info__meta">
                    <span class="post-info__meta-item">
                      <svg class="post-info__meta-icon icon-svg_views" width="16" height="9"><use xlink:href="https://habr.com/images/1566547990/common-svg-sprite.svg#eye" /></svg><span class="post-info__meta-counter post-info__meta-counter_small">18,6k</span>
                    </span>
                            <a href="https://habr.com/ru/news/t/464669/#comments" class="post-info__meta-item" rel="nofollow">
                                <svg class="post-info__meta-icon icon-svg_comments" width="14" height="13"><use xlink:href="https://habr.com/images/1566547990/common-svg-sprite.svg#comment" /></svg><span class="post-info__meta-counter post-info__meta-counter_small">67</span>
                            </a>
                        </div>
                    </li>
                    <li class="content-list__item content-list__item_devided post-info">
                        <a href="https://habr.com/ru/post/464599/" class="post-info__title" onclick="if (typeof ga === 'function') { ga('send', 'event', 'tm_block', 'most_read', 'sidebar', '4'); }">Когда история ненастоящая: винзавод «Коктебель», фейковые вина и уроки маркетинга</a>
                        <div class="post-info__meta">
                    <span class="post-info__meta-item">
                      <svg class="post-info__meta-icon icon-svg_views" width="16" height="9"><use xlink:href="https://habr.com/images/1566547990/common-svg-sprite.svg#eye" /></svg><span class="post-info__meta-counter post-info__meta-counter_small">35,1k</span>
                    </span>
                            <a href="https://habr.com/ru/post/464599/#comments" class="post-info__meta-item" rel="nofollow">
                                <svg class="post-info__meta-icon icon-svg_comments" width="14" height="13"><use xlink:href="https://habr.com/images/1566547990/common-svg-sprite.svg#comment" /></svg><span class="post-info__meta-counter post-info__meta-counter_small">147</span>
                            </a>
                        </div>
                    </li>
                    <li class="content-list__item content-list__item_devided post-info">
                        <a href="https://habr.com/ru/post/464621/" class="post-info__title" onclick="if (typeof ga === 'function') { ga('send', 'event', 'tm_block', 'most_read', 'sidebar', '5'); }">Как программист банк выбирал и договора читал</a>
                        <div class="post-info__meta">
                    <span class="post-info__meta-item">
                      <svg class="post-info__meta-icon icon-svg_views" width="16" height="9"><use xlink:href="https://habr.com/images/1566547990/common-svg-sprite.svg#eye" /></svg><span class="post-info__meta-counter post-info__meta-counter_small">39,6k</span>
                    </span>
                            <a href="https://habr.com/ru/post/464621/#comments" class="post-info__meta-item" rel="nofollow">
                                <svg class="post-info__meta-icon icon-svg_comments" width="14" height="13"><use xlink:href="https://habr.com/images/1566547990/common-svg-sprite.svg#comment" /></svg><span class="post-info__meta-counter post-info__meta-counter_small">265</span>
                            </a>
                        </div>
                    </li>
                    <li class="content-list__item content-list__item_devided post-info">
                        <a href="https://habr.com/ru/post/464591/" class="post-info__title" onclick="if (typeof ga === 'function') { ga('send', 'event', 'tm_block', 'most_read', 'sidebar', '6'); }">Химия жареной курочки. Детальный разбор</a>
                        <div class="post-info__meta">
                    <span class="post-info__meta-item">
                      <svg class="post-info__meta-icon icon-svg_views" width="16" height="9"><use xlink:href="https://habr.com/images/1566547990/common-svg-sprite.svg#eye" /></svg><span class="post-info__meta-counter post-info__meta-counter_small">24,1k</span>
                    </span>
                            <a href="https://habr.com/ru/post/464591/#comments" class="post-info__meta-item" rel="nofollow">
                                <svg class="post-info__meta-icon icon-svg_comments" width="14" height="13"><use xlink:href="https://habr.com/images/1566547990/common-svg-sprite.svg#comment" /></svg><span class="post-info__meta-counter post-info__meta-counter_small">29</span>
                            </a>
                        </div>
                    </li>
                    <li class="content-list__item content-list__item_devided post-info">
                        <a href="https://u.tmtm.ru/r-jug-itp-080819" class="post-info__title" onclick="if (typeof ga === 'function') { ga('send', 'event', 'tm_block', 'most_read', 'prmlink'); }">TechTrain прибывает на IT-платформу</a>
                        <div class="post-info__meta-label">
                            Картинки
                        </div>
                    </li>
                </ul>
            </div>
        </div>
    </div>

</div>
</div>

</div>
</div>

<div class="layout__row layout__row_promo-blocks">
    <div class="layout__cell">
        <div class="column-wrapper column-wrapper_bottom column-wrapper_bordered">
            <div class="content_left">


                <div class="default-block default-block_content">
                    <div class="default-block__header default-block__header_large">
                        <h2 class="default-block__header-title default-block__header-title_large">Самое читаемое</h2>
                    </div>
                    <div class="default-block__content default-block__content_most-read" id="broadcast_tabs_posts">
                        <ul class="toggle-menu toggle-menu__most-read">
                            <li class="toggle-menu__item">
                                <a href="#broadcast_posts_today" class="toggle-menu__item-link active" rel="nofollow">Сутки</a>
                            </li>
                            <li class="toggle-menu__item">
                                <a href="#broadcast_posts_week" class="toggle-menu__item-link" rel="nofollow">Неделя</a>
                            </li>
                            <li class="toggle-menu__item">
                                <a href="#broadcast_posts_month" class="toggle-menu__item-link" rel="nofollow">Месяц</a>
                            </li>
                        </ul>

                        <div class="tabs__content tabs__content_reading" id="broadcast_posts_today">
                            <ul class="content-list content-list_most-read">
                                <li class="content-list__item content-list__item_devided post-info">
                                    <a href="https://habr.com/ru/post/464603/" class="post-info__title post-info__title_large" onclick="if (typeof ga === 'function') { ga('send', 'event', 'tm_block', 'most_read', 'today'); }">Алкоголизм последней стадии</a>
                                    <div class="post-info__meta">
                    <span class="post-info__meta-item">
                      <svg class="post-info__meta-icon icon-svg_rating" width="11" height="13"><use xlink:href="https://habr.com/images/1566547990/common-svg-sprite.svg#arrow-bold" /></svg><span class="post-info__meta-counter">&plus;125</span>
                    </span>
                                        <span class="post-info__meta-item">
                      <svg class="post-info__meta-icon icon-svg_views" width="16" height="9"><use xlink:href="https://habr.com/images/1566547990/common-svg-sprite.svg#eye" /></svg><span class="post-info__meta-counter">77,8k</span>
                    </span>
                                        <span class="post-info__meta-item">
                      <svg class="post-info__meta-icon icon-svg_bookmark-mini" width="8" height="13"><use xlink:href="https://habr.com/images/1566547990/common-svg-sprite.svg#book" /></svg><span class="post-info__meta-counter">125</span>
                    </span>
                                        <a href="https://habr.com/ru/post/464603/#comments" class="post-info__meta-item" rel="nofollow">
                                            <svg class="post-info__meta-icon icon-svg_comments" width="14" height="13"><use xlink:href="https://habr.com/images/1566547990/common-svg-sprite.svg#comment" /></svg><span class="post-info__meta-counter">314</span>
                                        </a>
                                    </div>
                                </li>
                                <li class="content-list__item content-list__item_devided post-info">
                                    <a href="https://habr.com/ru/post/464595/" class="post-info__title post-info__title_large" onclick="if (typeof ga === 'function') { ga('send', 'event', 'tm_block', 'most_read', 'today'); }">Алкоголь и математик(а)</a>
                                    <div class="post-info__meta">
                    <span class="post-info__meta-item">
                      <svg class="post-info__meta-icon icon-svg_rating" width="11" height="13"><use xlink:href="https://habr.com/images/1566547990/common-svg-sprite.svg#arrow-bold" /></svg><span class="post-info__meta-counter">&plus;39</span>
                    </span>
                                        <span class="post-info__meta-item">
                      <svg class="post-info__meta-icon icon-svg_views" width="16" height="9"><use xlink:href="https://habr.com/images/1566547990/common-svg-sprite.svg#eye" /></svg><span class="post-info__meta-counter">45k</span>
                    </span>
                                        <span class="post-info__meta-item">
                      <svg class="post-info__meta-icon icon-svg_bookmark-mini" width="8" height="13"><use xlink:href="https://habr.com/images/1566547990/common-svg-sprite.svg#book" /></svg><span class="post-info__meta-counter">104</span>
                    </span>
                                        <a href="https://habr.com/ru/post/464595/#comments" class="post-info__meta-item" rel="nofollow">
                                            <svg class="post-info__meta-icon icon-svg_comments" width="14" height="13"><use xlink:href="https://habr.com/images/1566547990/common-svg-sprite.svg#comment" /></svg><span class="post-info__meta-counter">302</span>
                                        </a>
                                    </div>
                                </li>
                                <li class="content-list__item content-list__item_devided post-info">
                                    <a href="https://habr.com/ru/post/464599/" class="post-info__title post-info__title_large" onclick="if (typeof ga === 'function') { ga('send', 'event', 'tm_block', 'most_read', 'today'); }">Когда история ненастоящая: винзавод «Коктебель», фейковые вина и уроки маркетинга</a>
                                    <div class="post-info__meta">
                    <span class="post-info__meta-item">
                      <svg class="post-info__meta-icon icon-svg_rating" width="11" height="13"><use xlink:href="https://habr.com/images/1566547990/common-svg-sprite.svg#arrow-bold" /></svg><span class="post-info__meta-counter">&plus;96</span>
                    </span>
                                        <span class="post-info__meta-item">
                      <svg class="post-info__meta-icon icon-svg_views" width="16" height="9"><use xlink:href="https://habr.com/images/1566547990/common-svg-sprite.svg#eye" /></svg><span class="post-info__meta-counter">35,1k</span>
                    </span>
                                        <span class="post-info__meta-item">
                      <svg class="post-info__meta-icon icon-svg_bookmark-mini" width="8" height="13"><use xlink:href="https://habr.com/images/1566547990/common-svg-sprite.svg#book" /></svg><span class="post-info__meta-counter">62</span>
                    </span>
                                        <a href="https://habr.com/ru/post/464599/#comments" class="post-info__meta-item" rel="nofollow">
                                            <svg class="post-info__meta-icon icon-svg_comments" width="14" height="13"><use xlink:href="https://habr.com/images/1566547990/common-svg-sprite.svg#comment" /></svg><span class="post-info__meta-counter">147</span>
                                        </a>
                                    </div>
                                </li>
                                <li class="content-list__item content-list__item_devided post-info">
                                    <a href="https://habr.com/ru/post/464591/" class="post-info__title post-info__title_large" onclick="if (typeof ga === 'function') { ga('send', 'event', 'tm_block', 'most_read', 'today'); }">Химия жареной курочки. Детальный разбор</a>
                                    <div class="post-info__meta">
                    <span class="post-info__meta-item">
                      <svg class="post-info__meta-icon icon-svg_rating" width="11" height="13"><use xlink:href="https://habr.com/images/1566547990/common-svg-sprite.svg#arrow-bold" /></svg><span class="post-info__meta-counter">&plus;43</span>
                    </span>
                                        <span class="post-info__meta-item">
                      <svg class="post-info__meta-icon icon-svg_views" width="16" height="9"><use xlink:href="https://habr.com/images/1566547990/common-svg-sprite.svg#eye" /></svg><span class="post-info__meta-counter">24,1k</span>
                    </span>
                                        <span class="post-info__meta-item">
                      <svg class="post-info__meta-icon icon-svg_bookmark-mini" width="8" height="13"><use xlink:href="https://habr.com/images/1566547990/common-svg-sprite.svg#book" /></svg><span class="post-info__meta-counter">79</span>
                    </span>
                                        <a href="https://habr.com/ru/post/464591/#comments" class="post-info__meta-item" rel="nofollow">
                                            <svg class="post-info__meta-icon icon-svg_comments" width="14" height="13"><use xlink:href="https://habr.com/images/1566547990/common-svg-sprite.svg#comment" /></svg><span class="post-info__meta-counter">29</span>
                                        </a>
                                    </div>
                                </li>
                                <li class="content-list__item content-list__item_devided post-info">
                                    <a href="https://habr.com/ru/post/464457/" class="post-info__title post-info__title_large" onclick="if (typeof ga === 'function') { ga('send', 'event', 'tm_block', 'most_read', 'today'); }">22 сайта для программиста, которые помогут заговорить на английском</a>
                                    <div class="post-info__meta">
                    <span class="post-info__meta-item">
                      <svg class="post-info__meta-icon icon-svg_rating" width="11" height="13"><use xlink:href="https://habr.com/images/1566547990/common-svg-sprite.svg#arrow-bold" /></svg><span class="post-info__meta-counter">&plus;30</span>
                    </span>
                                        <span class="post-info__meta-item">
                      <svg class="post-info__meta-icon icon-svg_views" width="16" height="9"><use xlink:href="https://habr.com/images/1566547990/common-svg-sprite.svg#eye" /></svg><span class="post-info__meta-counter">36,1k</span>
                    </span>
                                        <span class="post-info__meta-item">
                      <svg class="post-info__meta-icon icon-svg_bookmark-mini" width="8" height="13"><use xlink:href="https://habr.com/images/1566547990/common-svg-sprite.svg#book" /></svg><span class="post-info__meta-counter">836</span>
                    </span>
                                        <a href="https://habr.com/ru/post/464457/#comments" class="post-info__meta-item" rel="nofollow">
                                            <svg class="post-info__meta-icon icon-svg_comments" width="14" height="13"><use xlink:href="https://habr.com/images/1566547990/common-svg-sprite.svg#comment" /></svg><span class="post-info__meta-counter">12</span>
                                        </a>
                                    </div>
                                </li>
                            </ul>
                        </div>
                        <div class="tabs__content tabs__content_reading" id="broadcast_posts_week">
                            <ul class="content-list content-list_most-read">
                                <li class="content-list__item content-list__item_devided post-info">
                                    <a href="https://habr.com/ru/post/463799/" class="post-info__title post-info__title_large" onclick="if (typeof ga === 'function') { ga('send', 'event', 'tm_block', 'most_read', 'week'); }">Что мне не нравится в Windows 10</a>
                                    <div class="post-info__meta">
                    <span class="post-info__meta-item">
                      <svg class="post-info__meta-icon icon-svg_rating" width="11" height="13"><use xlink:href="https://habr.com/images/1566547990/common-svg-sprite.svg#arrow-bold" /></svg><span class="post-info__meta-counter">&plus;62</span>
                    </span>
                                        <span class="post-info__meta-item">
                      <svg class="post-info__meta-icon icon-svg_views" width="16" height="9"><use xlink:href="https://habr.com/images/1566547990/common-svg-sprite.svg#eye" /></svg><span class="post-info__meta-counter">64,9k</span>
                    </span>
                                        <span class="post-info__meta-item">
                      <svg class="post-info__meta-icon icon-svg_bookmark-mini" width="8" height="13"><use xlink:href="https://habr.com/images/1566547990/common-svg-sprite.svg#book" /></svg><span class="post-info__meta-counter">121</span>
                    </span>
                                        <a href="https://habr.com/ru/post/463799/#comments" class="post-info__meta-item" rel="nofollow">
                                            <svg class="post-info__meta-icon icon-svg_comments" width="14" height="13"><use xlink:href="https://habr.com/images/1566547990/common-svg-sprite.svg#comment" /></svg><span class="post-info__meta-counter">726</span>
                                        </a>
                                    </div>
                                </li>
                                <li class="content-list__item content-list__item_devided post-info">
                                    <a href="https://habr.com/ru/company/mosigra/blog/463773/" class="post-info__title post-info__title_large" onclick="if (typeof ga === 'function') { ga('send', 'event', 'tm_block', 'most_read', 'week'); }">Как устроена защита детей от информации — и феерическая история про то, откуда она сначала взялась (18+)</a>
                                    <div class="post-info__meta">
                    <span class="post-info__meta-item">
                      <svg class="post-info__meta-icon icon-svg_rating" width="11" height="13"><use xlink:href="https://habr.com/images/1566547990/common-svg-sprite.svg#arrow-bold" /></svg><span class="post-info__meta-counter">&plus;160</span>
                    </span>
                                        <span class="post-info__meta-item">
                      <svg class="post-info__meta-icon icon-svg_views" width="16" height="9"><use xlink:href="https://habr.com/images/1566547990/common-svg-sprite.svg#eye" /></svg><span class="post-info__meta-counter">58,4k</span>
                    </span>
                                        <span class="post-info__meta-item">
                      <svg class="post-info__meta-icon icon-svg_bookmark-mini" width="8" height="13"><use xlink:href="https://habr.com/images/1566547990/common-svg-sprite.svg#book" /></svg><span class="post-info__meta-counter">163</span>
                    </span>
                                        <a href="https://habr.com/ru/company/mosigra/blog/463773/#comments" class="post-info__meta-item" rel="nofollow">
                                            <svg class="post-info__meta-icon icon-svg_comments" width="14" height="13"><use xlink:href="https://habr.com/images/1566547990/common-svg-sprite.svg#comment" /></svg><span class="post-info__meta-counter">642</span>
                                        </a>
                                    </div>
                                </li>
                                <li class="content-list__item content-list__item_devided post-info">
                                    <a href="https://habr.com/ru/post/463957/" class="post-info__title post-info__title_large" onclick="if (typeof ga === 'function') { ga('send', 'event', 'tm_block', 'most_read', 'week'); }">Как развернуть односвязный список на собеседовании</a>
                                    <div class="post-info__meta">
                    <span class="post-info__meta-item">
                      <svg class="post-info__meta-icon icon-svg_rating" width="11" height="13"><use xlink:href="https://habr.com/images/1566547990/common-svg-sprite.svg#arrow-bold" /></svg><span class="post-info__meta-counter">&plus;161</span>
                    </span>
                                        <span class="post-info__meta-item">
                      <svg class="post-info__meta-icon icon-svg_views" width="16" height="9"><use xlink:href="https://habr.com/images/1566547990/common-svg-sprite.svg#eye" /></svg><span class="post-info__meta-counter">49,8k</span>
                    </span>
                                        <span class="post-info__meta-item">
                      <svg class="post-info__meta-icon icon-svg_bookmark-mini" width="8" height="13"><use xlink:href="https://habr.com/images/1566547990/common-svg-sprite.svg#book" /></svg><span class="post-info__meta-counter">164</span>
                    </span>
                                        <a href="https://habr.com/ru/post/463957/#comments" class="post-info__meta-item" rel="nofollow">
                                            <svg class="post-info__meta-icon icon-svg_comments" width="14" height="13"><use xlink:href="https://habr.com/images/1566547990/common-svg-sprite.svg#comment" /></svg><span class="post-info__meta-counter">383</span>
                                        </a>
                                    </div>
                                </li>
                                <li class="content-list__item content-list__item_devided post-info">
                                    <a href="https://habr.com/ru/post/464603/" class="post-info__title post-info__title_large" onclick="if (typeof ga === 'function') { ga('send', 'event', 'tm_block', 'most_read', 'week'); }">Алкоголизм последней стадии</a>
                                    <div class="post-info__meta">
                    <span class="post-info__meta-item">
                      <svg class="post-info__meta-icon icon-svg_rating" width="11" height="13"><use xlink:href="https://habr.com/images/1566547990/common-svg-sprite.svg#arrow-bold" /></svg><span class="post-info__meta-counter">&plus;125</span>
                    </span>
                                        <span class="post-info__meta-item">
                      <svg class="post-info__meta-icon icon-svg_views" width="16" height="9"><use xlink:href="https://habr.com/images/1566547990/common-svg-sprite.svg#eye" /></svg><span class="post-info__meta-counter">77,8k</span>
                    </span>
                                        <span class="post-info__meta-item">
                      <svg class="post-info__meta-icon icon-svg_bookmark-mini" width="8" height="13"><use xlink:href="https://habr.com/images/1566547990/common-svg-sprite.svg#book" /></svg><span class="post-info__meta-counter">125</span>
                    </span>
                                        <a href="https://habr.com/ru/post/464603/#comments" class="post-info__meta-item" rel="nofollow">
                                            <svg class="post-info__meta-icon icon-svg_comments" width="14" height="13"><use xlink:href="https://habr.com/images/1566547990/common-svg-sprite.svg#comment" /></svg><span class="post-info__meta-counter">314</span>
                                        </a>
                                    </div>
                                </li>
                                <li class="content-list__item content-list__item_devided post-info">
                                    <a href="https://habr.com/ru/post/464055/" class="post-info__title post-info__title_large" onclick="if (typeof ga === 'function') { ga('send', 'event', 'tm_block', 'most_read', 'week'); }">Изучаем данные, собранные Xiaomi Mi Band за год</a>
                                    <div class="post-info__meta">
                    <span class="post-info__meta-item">
                      <svg class="post-info__meta-icon icon-svg_rating" width="11" height="13"><use xlink:href="https://habr.com/images/1566547990/common-svg-sprite.svg#arrow-bold" /></svg><span class="post-info__meta-counter">&plus;42</span>
                    </span>
                                        <span class="post-info__meta-item">
                      <svg class="post-info__meta-icon icon-svg_views" width="16" height="9"><use xlink:href="https://habr.com/images/1566547990/common-svg-sprite.svg#eye" /></svg><span class="post-info__meta-counter">48k</span>
                    </span>
                                        <span class="post-info__meta-item">
                      <svg class="post-info__meta-icon icon-svg_bookmark-mini" width="8" height="13"><use xlink:href="https://habr.com/images/1566547990/common-svg-sprite.svg#book" /></svg><span class="post-info__meta-counter">128</span>
                    </span>
                                        <a href="https://habr.com/ru/post/464055/#comments" class="post-info__meta-item" rel="nofollow">
                                            <svg class="post-info__meta-icon icon-svg_comments" width="14" height="13"><use xlink:href="https://habr.com/images/1566547990/common-svg-sprite.svg#comment" /></svg><span class="post-info__meta-counter">74</span>
                                        </a>
                                    </div>
                                </li>
                            </ul>
                        </div>
                        <div class="tabs__content tabs__content_reading" id="broadcast_posts_month">
                            <ul class="content-list content-list_most-read">
                                <li class="content-list__item content-list__item_devided post-info">
                                    <a href="https://habr.com/ru/post/460901/" class="post-info__title post-info__title_large" onclick="if (typeof ga === 'function') { ga('send', 'event', 'tm_block', 'most_read', 'month'); }">Почему Senior Developer'ы не могут устроиться на работу</a>
                                    <div class="post-info__meta">
                    <span class="post-info__meta-item">
                      <svg class="post-info__meta-icon icon-svg_rating" width="11" height="13"><use xlink:href="https://habr.com/images/1566547990/common-svg-sprite.svg#arrow-bold" /></svg><span class="post-info__meta-counter">&plus;137</span>
                    </span>
                                        <span class="post-info__meta-item">
                      <svg class="post-info__meta-icon icon-svg_views" width="16" height="9"><use xlink:href="https://habr.com/images/1566547990/common-svg-sprite.svg#eye" /></svg><span class="post-info__meta-counter">115k</span>
                    </span>
                                        <span class="post-info__meta-item">
                      <svg class="post-info__meta-icon icon-svg_bookmark-mini" width="8" height="13"><use xlink:href="https://habr.com/images/1566547990/common-svg-sprite.svg#book" /></svg><span class="post-info__meta-counter">320</span>
                    </span>
                                        <a href="https://habr.com/ru/post/460901/#comments" class="post-info__meta-item" rel="nofollow">
                                            <svg class="post-info__meta-icon icon-svg_comments" width="14" height="13"><use xlink:href="https://habr.com/images/1566547990/common-svg-sprite.svg#comment" /></svg><span class="post-info__meta-counter">898</span>
                                        </a>
                                    </div>
                                </li>
                                <li class="content-list__item content-list__item_devided post-info">
                                    <a href="https://habr.com/ru/company/medium-isp/blog/461979/" class="post-info__title post-info__title_large" onclick="if (typeof ga === 'function') { ga('send', 'event', 'tm_block', 'most_read', 'month'); }">Дорогая, мы убиваем Интернет</a>
                                    <div class="post-info__meta">
                    <span class="post-info__meta-item">
                      <svg class="post-info__meta-icon icon-svg_rating" width="11" height="13"><use xlink:href="https://habr.com/images/1566547990/common-svg-sprite.svg#arrow-bold" /></svg><span class="post-info__meta-counter">&plus;307</span>
                    </span>
                                        <span class="post-info__meta-item">
                      <svg class="post-info__meta-icon icon-svg_views" width="16" height="9"><use xlink:href="https://habr.com/images/1566547990/common-svg-sprite.svg#eye" /></svg><span class="post-info__meta-counter">114k</span>
                    </span>
                                        <span class="post-info__meta-item">
                      <svg class="post-info__meta-icon icon-svg_bookmark-mini" width="8" height="13"><use xlink:href="https://habr.com/images/1566547990/common-svg-sprite.svg#book" /></svg><span class="post-info__meta-counter">619</span>
                    </span>
                                        <a href="https://habr.com/ru/company/medium-isp/blog/461979/#comments" class="post-info__meta-item" rel="nofollow">
                                            <svg class="post-info__meta-icon icon-svg_comments" width="14" height="13"><use xlink:href="https://habr.com/images/1566547990/common-svg-sprite.svg#comment" /></svg><span class="post-info__meta-counter">932</span>
                                        </a>
                                    </div>
                                </li>
                                <li class="content-list__item content-list__item_devided post-info">
                                    <a href="https://habr.com/ru/post/462969/" class="post-info__title post-info__title_large" onclick="if (typeof ga === 'function') { ga('send', 'event', 'tm_block', 'most_read', 'month'); }">Как власти Казахстана пытаются прикрыть свой провал с внедрением сертификата</a>
                                    <div class="post-info__meta">
                    <span class="post-info__meta-item">
                      <svg class="post-info__meta-icon icon-svg_rating" width="11" height="13"><use xlink:href="https://habr.com/images/1566547990/common-svg-sprite.svg#arrow-bold" /></svg><span class="post-info__meta-counter">&plus;105</span>
                    </span>
                                        <span class="post-info__meta-item">
                      <svg class="post-info__meta-icon icon-svg_views" width="16" height="9"><use xlink:href="https://habr.com/images/1566547990/common-svg-sprite.svg#eye" /></svg><span class="post-info__meta-counter">107k</span>
                    </span>
                                        <span class="post-info__meta-item">
                      <svg class="post-info__meta-icon icon-svg_bookmark-mini" width="8" height="13"><use xlink:href="https://habr.com/images/1566547990/common-svg-sprite.svg#book" /></svg><span class="post-info__meta-counter">53</span>
                    </span>
                                        <a href="https://habr.com/ru/post/462969/#comments" class="post-info__meta-item" rel="nofollow">
                                            <svg class="post-info__meta-icon icon-svg_comments" width="14" height="13"><use xlink:href="https://habr.com/images/1566547990/common-svg-sprite.svg#comment" /></svg><span class="post-info__meta-counter">77</span>
                                        </a>
                                    </div>
                                </li>
                                <li class="content-list__item content-list__item_devided post-info">
                                    <a href="https://habr.com/ru/post/461745/" class="post-info__title post-info__title_large" onclick="if (typeof ga === 'function') { ga('send', 'event', 'tm_block', 'most_read', 'month'); }">Цены российского черного рынка на пробив персональных данных (плюс ответ на ответ Тинькофф Банка)</a>
                                    <div class="post-info__meta">
                    <span class="post-info__meta-item">
                      <svg class="post-info__meta-icon icon-svg_rating" width="11" height="13"><use xlink:href="https://habr.com/images/1566547990/common-svg-sprite.svg#arrow-bold" /></svg><span class="post-info__meta-counter">&plus;250</span>
                    </span>
                                        <span class="post-info__meta-item">
                      <svg class="post-info__meta-icon icon-svg_views" width="16" height="9"><use xlink:href="https://habr.com/images/1566547990/common-svg-sprite.svg#eye" /></svg><span class="post-info__meta-counter">106k</span>
                    </span>
                                        <span class="post-info__meta-item">
                      <svg class="post-info__meta-icon icon-svg_bookmark-mini" width="8" height="13"><use xlink:href="https://habr.com/images/1566547990/common-svg-sprite.svg#book" /></svg><span class="post-info__meta-counter">236</span>
                    </span>
                                        <a href="https://habr.com/ru/post/461745/#comments" class="post-info__meta-item" rel="nofollow">
                                            <svg class="post-info__meta-icon icon-svg_comments" width="14" height="13"><use xlink:href="https://habr.com/images/1566547990/common-svg-sprite.svg#comment" /></svg><span class="post-info__meta-counter">318</span>
                                        </a>
                                    </div>
                                </li>
                                <li class="content-list__item content-list__item_devided post-info">
                                    <a href="https://habr.com/ru/post/463165/" class="post-info__title post-info__title_large" onclick="if (typeof ga === 'function') { ga('send', 'event', 'tm_block', 'most_read', 'month'); }">Telegram наносит ответный удар DPI и блокировкам — Fake TLS</a>
                                    <div class="post-info__meta">
                    <span class="post-info__meta-item">
                      <svg class="post-info__meta-icon icon-svg_rating" width="11" height="13"><use xlink:href="https://habr.com/images/1566547990/common-svg-sprite.svg#arrow-bold" /></svg><span class="post-info__meta-counter">&plus;153</span>
                    </span>
                                        <span class="post-info__meta-item">
                      <svg class="post-info__meta-icon icon-svg_views" width="16" height="9"><use xlink:href="https://habr.com/images/1566547990/common-svg-sprite.svg#eye" /></svg><span class="post-info__meta-counter">101k</span>
                    </span>
                                        <span class="post-info__meta-item">
                      <svg class="post-info__meta-icon icon-svg_bookmark-mini" width="8" height="13"><use xlink:href="https://habr.com/images/1566547990/common-svg-sprite.svg#book" /></svg><span class="post-info__meta-counter">210</span>
                    </span>
                                        <a href="https://habr.com/ru/post/463165/#comments" class="post-info__meta-item" rel="nofollow">
                                            <svg class="post-info__meta-icon icon-svg_comments" width="14" height="13"><use xlink:href="https://habr.com/images/1566547990/common-svg-sprite.svg#comment" /></svg><span class="post-info__meta-counter">198</span>
                                        </a>
                                    </div>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>

            </div>

            <div class="sidebar_right">
                <div class="default-block">
                    <div class="default-block__header">
                        <h2 class="default-block__header-title">Рекомендуем</h2>
                        <a href="https://tmtm.ru/megapost" class="default-block__header-link" rel="nofollow" onclick="if (typeof ga === 'function') { ga('send', 'event', 'tm_block', 'megapost_promo', 'order'); }">Разместить</a>
                    </div>
                    <div class="default-block__content">
                        <ul class="megapost-teasers megapost-teasers_sidebar">
                            <li class="megapost-teasers__item megapost-teasers__item_sidebar teaser">
                                <a href="https://u.tmtm.ru/ZXdfX" target="_blank" class="teaser__image" onclick="if (typeof ga === 'function') { ga('send', 'event', 'tm_block', 'megapost_promo', 'article_pic'); }" rel="nofollow">
                                    <img src="https://habrastorage.org/getpro/tmtm/pictures/720/0e9/0e7/7200e90e7e888aa61b59c15d50c1a70f.jpg" class="teaser__image-pic"/>
                                    <div class="megapost-teasers__label">
                                        Интересно
                                    </div>
                                </a>
                                <a href="https://u.tmtm.ru/ZXdfX" target="_blank" class="teaser__body" onclick="if (typeof ga === 'function') { ga('send', 'event', 'tm_block', 'megapost_promo', 'article_title'); }" rel="nofollow">
                                    <h3 class="teaser__body-title">Как политика 19 века повлияла на расположение дата-центров сегодня</h3>
                                </a>
                            </li>
                            <li class="megapost-teasers__item megapost-teasers__item_sidebar teaser">
                                <a href="https://u.tmtm.ru/9evMT" target="_blank" class="teaser__image" onclick="if (typeof ga === 'function') { ga('send', 'event', 'tm_block', 'megapost_promo', 'article_pic'); }" rel="nofollow">
                                    <img src="https://habrastorage.org/getpro/tmtm/pictures/12b/10f/aca/12b10facabfa801bcb6ca117e841694e.jpg" class="teaser__image-pic"/>
                                    <div class="megapost-teasers__label">
                                        Интересно
                                    </div>
                                </a>
                                <a href="https://u.tmtm.ru/9evMT" target="_blank" class="teaser__body" onclick="if (typeof ga === 'function') { ga('send', 'event', 'tm_block', 'megapost_promo', 'article_title'); }" rel="nofollow">
                                    <h3 class="teaser__body-title">Увлекательная история: как сайт VPN-сервиса дважды вышел из-под незаконной блокировки</h3>
                                </a>
                            </li>
                        </ul>
                    </div>
                </div>

            </div>
        </div>

    </div>
</div>

<div class="layout__row layout__row_footer-links">
    <div class="layout__cell">
        <div class="footer-grid footer-grid_menu">
            <div class="footer-grid__item footer-block">
                <h3 class="footer-block__title">
                    Ваш аккаунт
                </h3>
                <div class="footer-block__content">
                    <ul class="footer-menu">
                        <li class="footer-menu__item">
                            <a href="https://habr.com/ru/auth/login/" class="footer-menu__item-link">Войти</a>
                        </li>
                        <li class="footer-menu__item">
                            <a href="https://habr.com/ru/auth/register/" class="footer-menu__item-link">Регистрация</a>
                        </li>
                    </ul>
                </div>
            </div>

            <div class="footer-grid__item footer-block">
                <h3 class="footer-block__title">Разделы</h3>
                <div class="footer-block__content">
                    <ul class="footer-menu">
                        <li class="footer-menu__item">
                            <a href="https://habr.com/ru/posts/top/" class="footer-menu__item-link">Публикации</a>
                        </li>
                        <li class="footer-menu__item">
                            <a href="https://habr.com/ru/news/" class="footer-menu__item-link">Новости</a>
                        </li>
                        <li class="footer-menu__item">
                            <a href="https://habr.com/ru/hubs/" class="footer-menu__item-link">Хабы</a>
                        </li>
                        <li class="footer-menu__item">
                            <a href="https://habr.com/ru/companies/" class="footer-menu__item-link">Компании</a>
                        </li>
                        <li class="footer-menu__item">
                            <a href="https://habr.com/ru/users/" class="footer-menu__item-link">Пользователи</a>
                        </li>
                        <li class="footer-menu__item">
                            <a href="https://habr.com/ru/sandbox/" class="footer-menu__item-link">Песочница</a>
                        </li>
                    </ul>
                </div>
            </div>

            <div class="footer-grid__item footer-block">
                <h3 class="footer-block__title">Информация</h3>
                <div class="footer-block__content">
                    <ul class="footer-menu">
                        <li class="footer-menu__item">
                            <a href="https://habr.com/ru/info/help/rules/" class="footer-menu__item-link">Правила</a>
                        </li>
                        <li class="footer-menu__item">
                            <a href="https://habr.com/ru/info/help/" class="footer-menu__item-link">Помощь</a>
                        </li>
                        <li class="footer-menu__item">
                            <a href="https://habr.com/ru/info/topics/madskillz/" class="footer-menu__item-link">Документация</a>
                        </li>
                        <li class="footer-menu__item">
                            <a href="https://account.habr.com/info/agreement/?hl=ru_RU" class="footer-menu__item-link">Соглашение</a>
                        </li>
                        <li class="footer-menu__item">
                            <a href="https://account.habr.com/info/confidential/?hl=ru_RU" class="footer-menu__item-link">Конфиденциальность</a>
                        </li>
                    </ul>
                </div>
            </div>

            <div class="footer-grid__item footer-block">
                <h3 class="footer-block__title">Услуги</h3>
                <div class="footer-block__content">
                    <ul class="footer-menu">
                        <li class="footer-menu__item">
                            <a href="https://tmtm.ru/services/advertising/" target="_blank" class="footer-menu__item-link">Реклама</a>
                        </li>
                        <li class="footer-menu__item">
                            <a href="https://tmtm.ru/services/corpblog/" target="_blank" class="footer-menu__item-link">Тарифы</a>
                        </li>
                        <li class="footer-menu__item">
                            <a href="https://tmtm.ru/services/content/" target="_blank" class="footer-menu__item-link">Контент</a>
                        </li>
                        <li class="footer-menu__item">
                            <a href="https://tmtm.ru/workshops/" target="_blank" class="footer-menu__item-link">Семинары</a>
                        </li>
                    </ul>
                </div>
            </div>
        </div>

        <div class="footer-misprints">
            Если нашли опечатку в&nbsp;посте, выделите ее&nbsp;и&nbsp;нажмите Ctrl+Enter, чтобы сообщить автору.
        </div>

    </div>
</div>

<div class="layout__row layout__row_footer">
    <div class="layout__cell">
        <div class="footer-grid footer">
            <div class="footer-grid__item footer-grid__item_copyright">
                <span class="footer__copyright">&copy; 2006 &ndash; 2019 «<a href="https://tmtm.ru/" class="footer__link">TM</a>»</span>
            </div>
            <div class="footer-grid__item footer-grid__item_link footer-grid__item_lang">
                <svg class="icon-svg icon-svg_lang-footer" width="16" height="16">
                    <use xlink:href="https://habr.com/images/1566547990/common-svg-sprite.svg#globus" />
                </svg>
                <a href="#" class="footer__link js-show_lang_settings">Настройка языка</a>
            </div>
            <div class="footer-grid__item footer-grid__item_link">
                <a href="https://habr.com/ru/about/" class="footer__link">О сайте</a>
            </div>
            <div class="footer-grid__item footer-grid__item_link">
                <a href="https://habr.com/ru/feedback/" class="footer__link">Служба поддержки</a>
            </div>
            <div class="footer-grid__item footer-grid__item_link">
                <a href="https://m.habr.com/ru?mobile=yes" class="footer__link">Мобильная версия</a>
            </div>

            <div class="footer-grid__item footer-grid__item_social">
                <ul class="social-icons">
                    <li class="social-icons__item">
                        <a href="https://twitter.com/habr_com"
                           class="social-icons__item-link social-icons__item-link_normal social-icons__item-link_twitter"
                           target="_blank"
                           onclick="if (typeof ga === 'function') { ga('send', 'event', 'footer', 'Social_icons', 'twitter'); }"
                        >
                            <svg class="icon-svg" aria-hidden="true" aria-labelledby="title" version="1.1" role="img" width="24" height="24" viewBox="0 0 24 24"><path d="M17.414 8.642c-.398.177-.826.296-1.276.35.459-.275.811-.71.977-1.229-.43.254-.905.439-1.41.539-.405-.432-.982-.702-1.621-.702-1.227 0-2.222.994-2.222 2.222 0 .174.019.344.058.506-1.846-.093-3.484-.978-4.579-2.322-.191.328-.301.71-.301 1.117 0 .77.392 1.45.988 1.849-.363-.011-.706-.111-1.006-.278v.028c0 1.077.766 1.974 1.782 2.178-.187.051-.383.078-.586.078-.143 0-.282-.014-.418-.04.282.882 1.103 1.525 2.075 1.542-.76.596-1.718.951-2.759.951-.179 0-.356-.01-.53-.031.983.63 2.15.998 3.406.998 4.086 0 6.321-3.386 6.321-6.321l-.006-.287c.433-.314.81-.705 1.107-1.15z"/></svg>
                        </a>
                    </li>
                    <li class="social-icons__item">
                        <a href="https://www.facebook.com/habrahabr.ru"
                           class="social-icons__item-link social-icons__item-link_normal social-icons__item-link_facebook"
                           target="_blank"
                           onclick="if (typeof ga === 'function') { ga('send', 'event', 'footer', 'Social_icons', 'facebook'); }"
                        >
                            <svg class="icon-svg" aria-hidden="true" aria-labelledby="title" version="1.1" role="img" width="24" height="24" viewBox="0 0 24 24"><path d="M14.889 8.608h-1.65c-.195 0-.413.257-.413.6v1.192h2.063v1.698h-2.063v5.102h-1.948v-5.102h-1.766v-1.698h1.766v-1c0-1.434.995-2.6 2.361-2.6h1.65v1.808z"/></svg>
                        </a>
                    </li>
                    <li class="social-icons__item">
                        <a href="https://vk.com/habr"
                           class="social-icons__item-link social-icons__item-link_normal social-icons__item-link_vkontakte"
                           target="_blank"
                           onclick="if (typeof ga === 'function') { ga('send', 'event', 'footer', 'Social_icons', 'vkontakte'); }"
                        >
                            <svg class="icon-svg" aria-hidden="true" aria-labelledby="title" version="1.1" role="img" width="24" height="24" viewBox="0 0 24 24"><path d="M16.066 11.93s1.62-2.286 1.782-3.037c.054-.268-.064-.418-.343-.418h-1.406c-.322 0-.44.139-.537.343 0 0-.76 1.619-1.685 2.64-.297.33-.448.429-.612.429-.132 0-.193-.11-.193-.408v-2.607c0-.365-.043-.472-.343-.472h-2.254c-.172 0-.279.1-.279.236 0 .343.526.421.526 1.352v1.921c0 .386-.022.537-.204.537-.483 0-1.631-1.663-2.274-3.552-.129-.386-.268-.494-.633-.494h-1.406c-.204 0-.354.139-.354.343 0 .375.44 2.114 2.167 4.442 1.159 1.566 2.683 2.414 4.056 2.414.838 0 1.041-.139 1.041-.494v-1.202c0-.301.118-.429.29-.429.193 0 .534.062 1.33.848.945.901 1.01 1.276 1.525 1.276h1.578c.161 0 .311-.075.311-.343 0-.354-.462-.987-1.17-1.738-.29-.386-.762-.805-.912-.998-.215-.226-.151-.354-.001-.59z"/></svg>
                        </a>
                    </li>
                    <li class="social-icons__item">
                        <a href="https://telegram.me/habr_com"
                           class="social-icons__item-link social-icons__item-link_normal social-icons__item-link_telegram"
                           target="_blank"
                           onclick="if (typeof ga === 'function') { ga('send', 'event', 'footer', 'Social_icons', 'telegram'); }"
                        >
                            <svg class="icon-svg" aria-hidden="true" aria-labelledby="title" version="1.1" role="img" width="24" height="24" viewBox="0 0 24 24"><path d="M17.17 7.621l-10.498 3.699c-.169.059-.206.205-.006.286l2.257.904 1.338.536 6.531-4.796s.189.057.125.126l-4.68 5.062-.27.299.356.192 2.962 1.594c.173.093.397.016.447-.199.058-.254 1.691-7.29 1.728-7.447.047-.204-.087-.328-.291-.256zm-6.922 8.637c0 .147.082.188.197.084l1.694-1.522-1.891-.978v2.416z"/></svg>
                        </a>
                    </li>
                    <li class="social-icons__item">
                        <a href="https://www.youtube.com/channel/UCd_sTwKqVrweTt4oAKY5y4w"
                           class="social-icons__item-link social-icons__item-link_normal social-icons__item-link_youtube"
                           target="_blank"
                           onclick="if (typeof ga === 'function') { ga('send', 'event', 'footer', 'Social_icons', 'youtube'); }"
                        >
                            <svg class="icon-svg" aria-hidden="true" aria-labelledby="title" version="1.1" role="img" width="32" height="32" viewBox="0 0 32 32"><path d="M3.2 0h25.6c1.767 0 3.2 1.433 3.2 3.2v25.6c0 1.767-1.433 3.2-3.2 3.2h-25.6c-1.767 0-3.2-1.433-3.2-3.2v-25.6c0-1.767 1.433-3.2 3.2-3.2zm18.133 16l-10.667-5.333v10.667l10.667-5.333z"/></svg>

                        </a>
                    </li>
                    <li class="social-icons__item">
                        <a href="https://zen.yandex.ru/habr"
                           class="social-icons__item-link social-icons__item-link_normal social-icons__item-link_zen"
                           target="_blank"
                           onclick="if (typeof ga === 'function') { ga('send', 'event', 'footer', 'Social_icons', 'zen'); }"
                        >
                            <svg xmlns="http://www.w3.org/2000/svg" class="icon-svg" width="32" height="32" viewBox="0 0 32 32"><path fill="#f00" d="M3.2 0h25.6c1.767 0 3.2 1.433 3.2 3.2v25.6c0 1.767-1.433 3.2-3.2 3.2h-25.6c-1.767 0-3.2-1.433-3.2-3.2v-25.6c0-1.767 1.433-3.2 3.2-3.2z"/><path fill="#d00000" d="M13.661 32h-6.157l12.409-9.537 2.44 2.811-8.693 6.726zm3.782-32h5.883l-10.515 8.777-2.44-2.811 7.072-5.966z"/><path fill="#f8b3b2" d="M10.4 17.879l9.474-7.314 2.441 2.812-9.475 7.314z"/><path fill="#fff" d="M10.417 5.954l11.909 3.897v3.543l-11.909-3.897v-3.543zm0 11.909l11.909 3.897v3.543l-11.909-3.897v-3.543z"/></svg>

                        </a>
                    </li>
                </ul>
            </div>
        </div>

    </div>
</div>

<a href="#" class="layout__elevator hidden" id="scroll_to_top" title="Наверх"  onclick="if (typeof ga === 'function') { ga('send', 'event', 'navigation_button', 'down'); }">
    <svg class="icon-svg icon-svg_scroll-up" width="32" height="32" viewBox="0 0 32 32" aria-hidden="true" version="1.1" role="img"><path d="M16 0C7.164 0 0 7.164 0 16s7.164 16 16 16 16-7.164 16-16S24.836 0 16 0zm8.412 19.523c-.517.512-1.355.512-1.872 0L16 13.516l-6.54 6.01c-.518.51-1.356.51-1.873 0-.516-.513-.517-1.343 0-1.855l7.476-7.326c.517-.512 1.356-.512 1.873 0l7.476 7.327c.516.513.516 1.342 0 1.854z"/></svg>
</a>
</div>

<div class="overlay hidden" id="js-lang_settings">
    <div class="popup">
        <div class="popup__head popup__head_lang-settings">
            <span class="popup__head-title js-popup_title" data-section="1">Настройка языка</span>
            <button type="button" class="btn btn_small btn_popup-close js-hide_lang_settings">
                <svg class="icon-svg" width="12" height="12">
                    <use xlink:href="https://habr.com/images/1566547990/common-svg-sprite.svg#close" />
                </svg>
            </button>
        </div>
        <div class="popup__body">
            <form action="/json/settings/i18n/" method="post" class="form form_lang-settings" id="lang-settings-form">
                <fieldset class="form__fieldset form__fieldset_thin" data-section="2">
                    <legend class="form__legend form__legend_lang-settings js-popup_hl_legend">Интерфейс</legend>
                    <div class="form-field form-field_lang-settings">
                        <label class="radio radio_custom ">
                            <input type="radio" name="hl" id="hl_langs_ru" class="radio__input js-hl_langs" value="ru" checked>
                            <span class="radio__label radio__label_another">Русский</span>
                        </label>
                    </div>
                    <div class="form-field form-field_lang-settings">
                        <label class="radio radio_custom ">
                            <input type="radio" name="hl" id="hl_langs_en" class="radio__input js-hl_langs" value="en" >
                            <span class="radio__label radio__label_another">English</span>
                        </label>
                    </div>
                </fieldset>

                <fieldset class="form__fieldset form__fieldset_thin">
                    <legend class="form__legend form__legend_lang-settings js-popup_fl_legend" data-section="3">Язык публикаций</legend>
                    <div class="form-field form-field_lang-settings">
                        <label class="checkbox checkbox_custom">
                            <input type="checkbox" name="fl[]" id="fl_langs_ru" class="checkbox__input js-fl_langs" value="ru" checked>
                            <span class="checkbox__label checkbox__label_another js-popup_feed_ru">Русский</span>
                        </label>
                    </div>
                    <div class="form-field form-field_lang-settings">
                        <label class="checkbox checkbox_custom">
                            <input type="checkbox" name="fl[]" id="fl_langs_en" class="checkbox__input js-fl_langs" value="en" >
                            <span class="checkbox__label checkbox__label_another js-popup_feed_en">Английский</span>
                        </label>
                    </div>
                </fieldset>

                <div class="form__footer form__footer_lang-settings">
                    <button type="submit" class="btn btn_blue btn_huge btn_full-width js-popup_save_btn">Сохранить настройки</button>
                </div>
            </form>
        </div>
    </div>
</div>



<script type="text/javascript">
    // global vars
    var g_base_url = 'habr.com/ru';
    var g_base_fullurl = 'https://habr.com/ru/';
    var g_tmid_fullurl = 'https://account.habr.com/';
    var g_is_guest = false;
    var g_show_xpanel = false;
    var g_is_enableShortcuts = '1';
    var g_current_hl = 'ru';
    var g_current_fl = 'ru';

</script>

<script src="https://dr.habracdn.net/habrcom/javascripts/1566547990/vendors.bundle.js"></script>
<script src="https://dr.habracdn.net/habrcom/javascripts/1566547990/main.bundle.js"></script>
<script type="text/x-mathjax-config">
  MathJax.Hub.Config({
    showProcessingMessages: false,
    showMathMenu: true,
    tex2jax: {
      inlineMath: [['$inline$','$inline$']],
      displayMath: [['$$display$$','$$display$$']],
      processEscapes: true
    },
    MathMenu: {
      showRenderer: true,
      showContext:  true
    }
  });

  MathJax.Extension.Img2jax = {
    PreProcess: function (element) {
      var hasMath = false;
      var images = element.querySelectorAll('[data-tex]');
      for (var i = images.length - 1; i >= 0; i--) {
        var img = images[i];
        var tex = img.alt.replace(/(\r\n|\n|\r)/gm, " ");
        if (tex && tex[0] === '$'){
          var script = document.createElement("script"); script.type = "math/tex";
          hasMath = true;
          if (img.getAttribute('data-tex') == "display"){script.type += ";mode=display"}
          MathJax.HTML.setScript(script, tex.substring(1,tex.length-1));
          img.parentNode.replaceChild(script,img);
        }
      }
    }
  };

  MathJax.Hub.Register.PreProcessor(["PreProcess", MathJax.Extension.Img2jax]);
</script>

<script type="text/javascript"
        src="https://cdnjs.cloudflare.com/ajax/libs/mathjax/2.7.1/MathJax.js?config=TeX-AMS_SVG-full&locale=ru">
</script>



<script src="https://dr.habracdn.net/habrcom/javascripts/1566547990/check-login.js"></script>



<!-- Yandex.Metrika counter -->
<script type="text/javascript">
    (function (d, w, c) {
        (w[c] = w[c] || []).push(function() {
            try {
                if (typeof (_yaparams) != 'undefined') {
                    w.yaCounter24049213 = new Ya.Metrika({id:24049213,
                        webvisor:true,
                        clickmap:true,
                        trackLinks:true,
                        accurateTrackBounce:true,
                        params:_yaparams});
                } else {
                    w.yaCounter24049213 = new Ya.Metrika({id:24049213,
                        webvisor:true,
                        clickmap:true,
                        trackLinks:true,
                        accurateTrackBounce:true});
                }

            } catch(e) { }
        });

        var n = d.getElementsByTagName("script")[0],
            s = d.createElement("script"),
            f = function () { n.parentNode.insertBefore(s, n); };
        s.type = "text/javascript";
        s.async = true;
        s.src = (d.location.protocol == "https:" ? "https:" : "http:") + "//mc.yandex.ru/metrika/watch.js";

        if (w.opera == "[object Opera]") {
            d.addEventListener("DOMContentLoaded", f, false);
        } else { f(); }
    })(document, window, "yandex_metrika_callbacks");
</script>
<noscript><div><img src="//mc.yandex.ru/watch/24049213" style="position:absolute; left:-9999px;" alt="" /></div></noscript>
<!-- /Yandex.Metrika counter -->

<!-- Facebook Pixel Code -->
<script>
    !function(f,b,e,v,n,t,s)
    {if(f.fbq)return;n=f.fbq=function(){n.callMethod?
        n.callMethod.apply(n,arguments):n.queue.push(arguments)};
        if(!f._fbq)f._fbq=n;n.push=n;n.loaded=!0;n.version='2.0';
        n.queue=[];t=b.createElement(e);t.async=!0;
        t.src=v;s=b.getElementsByTagName(e)[0];
        s.parentNode.insertBefore(t,s)}(window,document,'script',
        'https://connect.facebook.net/en_US/fbevents.js');

    fbq('init', '317458588730613');
    fbq('track', 'PageView');
</script>
<noscript>
    <img height="1" width="1" src="https://www.facebook.com/tr?id=317458588730613&ev=PageView&noscript=1"/>
</noscript>
<!-- End Facebook Pixel Code -->
</body>
</html>