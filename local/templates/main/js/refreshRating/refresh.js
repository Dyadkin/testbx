$(document).ready(function () {

    $(".R_up").on("click", function () {
        let raise = 1;
        let id = $(this).attr("id");

        ajax(raise, id);
    });

    $(".R_down").on("click", function () {
        let raise = 0;
        let id = $(this).attr("id");

        ajax(raise, id);
    });
});

function ajax(raise, id) {
    $.ajax({
        url: '/local/raiting/refreshRaiting/class.php',
        type: 'POST',
        cache: false,
        data: {raise: raise, id: id,},
        dataType: 'html',
        success: function (data) {
            var rating = data;
            console.log(data);
            $(".rating").html(rating);
        }
    });
}

