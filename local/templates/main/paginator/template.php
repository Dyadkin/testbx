<div class="page__footer">
    <ul class="arrows-pagination">
        <li class="arrows-pagination__item">
            <span class="arrows-pagination__item-link">&larr;&nbsp;сюда</span>
        </li>
        <li class="arrows-pagination__item">
            <a class="arrows-pagination__item-link arrows-pagination__item-link_next"
               id="next_page" href="/ru/hub/php/page2/" rel=""><span>туда</span>&nbsp;&rarr;</a>
        </li>
    </ul>

    <ul class="toggle-menu toggle-menu_pagination" id="nav-pagess">

        <li class="toggle-menu__item toggle-menu__item_pagination">
            <span class="toggle-menu__item-link toggle-menu__item-link_pagination toggle-menu__item-link_active">1</span>
        </li>

        <li class="toggle-menu__item toggle-menu__item_pagination">
            <a href="/ru/hub/php/page2/" class="toggle-menu__item-link toggle-menu__item-link_pagination" >2</a>
        </li>

        <li class="toggle-menu__item toggle-menu__item_pagination">
            <a title="Последняя страница" class="toggle-menu__item-link toggle-menu__item-link_pagination toggle-menu__item-link_bordered" href="/ru/hub/php/page150/" rel="">
                <span class="icon-svg icon-svg_arrow-last"><svg class="icon-svg" width="28" height="24" viewBox="0 0 28 24"><path d="M8.452 5.455l2.93 3.192c.89.969 1.335 1.361 2.225 1.909h-13.608v2.773h13.634c-.838.5-1.492 1.102-2.252 1.913l-2.93 3.192 2.276 1.964 7.588-8.452-7.588-8.452-2.276 1.961zM24.297 0h3.087v23.891h-3.087v-23.891z"/></svg></span>
            </a>
        </li>
    </ul>
</div>
