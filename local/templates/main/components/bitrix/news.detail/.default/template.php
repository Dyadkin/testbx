<?php $arResult['TIMESTAMP_X'] = ConvertDateTime($arResult["TIMESTAMP_X"], "  d F Y  HH:MI"); ?>
<div class="company_post">
    <article class="post post_full" id="post_460285" lang="ru">
        <div class="post__wrapper">
            <header class="post__meta">
                <a href="https://habr.com/ru/users/rjhdby/" class="post__user-info user-info" title="Автор публикации">
                    <img src="//habrastorage.org/getpro/habr/avatars/330/b5e/fbd/330b5efbd4f629e93730d497b8d865e1.png" width="24" height="24" class="user-info__image-pic user-info__image-pic_small">
                    <span class="user-info__nickname user-info__nickname_small">НИК АВТОРА</span>
                </a>

                <span class="post__time" data-time_published="2019-07-23T07:54Z"><?=$arResult['TIMESTAMP_X']?></span>


            </header>

            <h1 class="post__title post__title_full">
                <span class="post__title-text"><?=$arResult['NAME']?></span>
            </h1>


            <!--Вкорячить из хайлоад блока теги -->

            <ul class="post__marks inline-list"></ul>

            <div class="post__body post__body_full">
                <div class="post__text post__text-html js-mediator-article">
                    <?=$arResult['DETAIL_TEXT']?>
                </div>
            </div>
        </div>
    </article>
    <form action="/json/favorites/" method="post" class="form form_bordered form_favorites-tags hidden" id="edit_tags_form">
        <input type="hidden" name="action" value="add" />
        <input type="hidden" name="ti" value="0" />
        <input type="hidden" name="tt" value="0" />

        <button type="button" class="btn form__close-btn" onclick="closeForm(this)" title="Закрыть"><svg class="icon-svg icon-svg_navbar-close-search" width="31" height="32" viewBox="0 0 31 32" aria-hidden="true" version="1.1" role="img"><path d="M26.67 0L15.217 11.448 3.77 0 0 3.77l11.447 11.45L0 26.666l3.77 3.77L15.218 18.99l11.45 11.448 3.772-3.77-11.448-11.45L30.44 3.772z"/></svg>
        </button>

        <fieldset class="form__fieldset">
            <legend class="form__legend">Пометьте публикацию своими метками</legend>
            <input type="text" name="tags_string" class="form__text-field" />
            <span class="form__desc">Метки лучше разделять запятой. Например: <i>программирование, алгоритмы</i></span>
        </fieldset>

        <div class="form__footer">
            <button type="submit" class="btn btn_x-large btn_outline_blue" disabled>Сохранить</button>
        </div>
    </form>

    <div class="post-additionals post-additionals_company">
        <ul class="post-stats post-stats_post js-user_" data-post-type="publish_corp" id="infopanel_post_460285">
            <li class="post-stats__item post-stats__item_voting-wjt">
                <div class="voting-wjt voting-wjt_post js-post-vote" data-id="460285" data-type="2">
                    <button type="button" class="btn voting-wjt__button " data-action="plus" onclick="posts_vote(this);" title="Голосовать могут только зарегистрированные пользователи " disabled><svg class="icon-svg_arrow-up" width="10" height="16"><use xlink:href="https://habr.com/images/1565957267/common-svg-sprite.svg#vote-arrow" /></svg></button>

                    <span class="voting-wjt__counter voting-wjt__counter_positive  js-score" title="Общий рейтинг 62: &uarr;61 и &darr;1">+60</span>

                    <button type="button" class="btn voting-wjt__button " data-action="minus" onclick="posts_vote(this);" title="Голосовать могут только зарегистрированные пользователи " disabled><svg class="icon-svg_arrow-down" width="10" height="16"><use xlink:href="https://habr.com/images/1565957267/common-svg-sprite.svg#vote-arrow" /></svg></button>
                </div>
            </li>
            <li class="post-stats__item post-stats__item_bookmark">
                <button type="button" class="btn bookmark-btn bookmark-btn_post " data-post-type="publish_corp" data-type="2" data-id="460285" data-action="add" title="Только зарегистрированные пользователи могут добавлять публикации в закладки" onclick="posts_add_to_favorite(this);" disabled>
                    <span class="btn_inner"><svg class="icon-svg_bookmark" width="10" height="16"><use xlink:href="https://habr.com/images/1565957267/common-svg-sprite.svg#book" /></svg><span class="bookmark__counter js-favs_count" title="Количество пользователей, добавивших публикацию в закладки">88</span></span>
                </button>
            </li>

            <li class="post-stats__item post-stats__item_views">
                <div class="post-stats__views" title="Количество просмотров">
                    <svg class="icon-svg_views-count" width="21" height="12"><use xlink:href="https://habr.com/images/1565957267/common-svg-sprite.svg#eye" /></svg><span class="post-stats__views-count">14,2k</span>
                </div>
            </li>

            <li class="post-stats__item post-stats__item_comments">
                <a href="https://habr.com/ru/company/funcorp/blog/460285/#comments" class="post-stats__comments-link" rel="nofollow">
                    <svg class="icon-svg_post-comments" width="16" height="16"><use xlink:href="https://habr.com/images/1565957267/common-svg-sprite.svg#comment" /></svg><span class="post-stats__comments-count" title="Читать комментарии">25</span>
                </a>
            </li>





        </ul>

        <form action="/json/complaints/add/" class="form form_bordered form_abuse hidden" method="post" id="abuse_form">
            <input type="hidden" name="tt" value="2" />
            <input type="hidden" name="ti" value="460285" />

            <button type="button" class="btn form__close-btn" onclick="closeForm(this)"><svg class="icon-svg icon-svg_navbar-close-search" width="31" height="32" viewBox="0 0 31 32" aria-hidden="true" version="1.1" role="img"><path d="M26.67 0L15.217 11.448 3.77 0 0 3.77l11.447 11.45L0 26.666l3.77 3.77L15.218 18.99l11.45 11.448 3.772-3.77-11.448-11.45L30.44 3.772z"/></svg>
            </button>

            <fieldset class="form__fieldset">
                <legend class="form__legend">Нарушение</legend>
                <input type="text" name="text" class="form__text-field" />
                <span class="form__desc">Опишите суть нарушения</span>
            </fieldset>

            <div class="form__footer">
                <button type="submit" class="btn btn_x-large btn_outline_blue" disabled>Отправить</button>
            </div>
        </form>


        <form action="/conversations/rjhdby/" method="GET" class="form form_bordered form_admin-causes hidden" id="admin_causes">

            <input type="hidden" name="moderation" value="460285">

            <button type="button" class="btn form__close-btn" onclick="closeForm(this)"><svg class="icon-svg icon-svg_navbar-close-search" width="31" height="32" viewBox="0 0 31 32" aria-hidden="true" version="1.1" role="img"><path d="M26.67 0L15.217 11.448 3.77 0 0 3.77l11.447 11.45L0 26.666l3.77 3.77L15.218 18.99l11.45 11.448 3.772-3.77-11.448-11.45L30.44 3.772z"/></svg>
            </button>

            <fieldset class="form__fieldset form__fieldset_admin-causes">
                <legend class="form__legend">Выберите рекомендации для отправки автору:</legend>

                <div class="checkbox-group_columns">
                    <label class="checkbox checkbox_custom checkbox_recommendation-form">
                        <input type="checkbox" class="form__input form__input_checkbox" name="id[]" value="1"/>
                        <span class="checkbox__label">Указан только блог</span>
                    </label>

                    <label class="checkbox checkbox_custom checkbox_recommendation-form">
                        <input type="checkbox" class="form__input form__input_checkbox" name="id[]" value="2"/>
                        <span class="checkbox__label">Орфографические ошибки</span>
                    </label>

                    <label class="checkbox checkbox_custom checkbox_recommendation-form">
                        <input type="checkbox" class="form__input form__input_checkbox" name="id[]" value="3"/>
                        <span class="checkbox__label">Пунктуационные ошибки</span>
                    </label>

                    <label class="checkbox checkbox_custom checkbox_recommendation-form">
                        <input type="checkbox" class="form__input form__input_checkbox" name="id[]" value="4"/>
                        <span class="checkbox__label">Отступы</span>
                    </label>

                    <label class="checkbox checkbox_custom checkbox_recommendation-form">
                        <input type="checkbox" class="form__input form__input_checkbox" name="id[]" value="5"/>
                        <span class="checkbox__label">Текст-простыня</span>
                    </label>

                    <label class="checkbox checkbox_custom checkbox_recommendation-form">
                        <input type="checkbox" class="form__input form__input_checkbox" name="id[]" value="6"/>
                        <span class="checkbox__label">Короткие предложения</span>
                    </label>
                    <label class="checkbox checkbox_custom checkbox_recommendation-form">
                        <input type="checkbox" class="form__input form__input_checkbox" name="id[]" value="7"/>
                        <span class="checkbox__label">Смайлики</span>
                    </label>
                    <label class="checkbox checkbox_custom checkbox_recommendation-form">
                        <input type="checkbox" class="form__input form__input_checkbox" name="id[]" value="8"/>
                        <span class="checkbox__label">Много форматирования</span>
                    </label>
                    <label class="checkbox checkbox_custom checkbox_recommendation-form">
                        <input type="checkbox" class="form__input form__input_checkbox" name="id[]" value="9"/>
                        <span class="checkbox__label">Картинки</span>
                    </label>
                    <label class="checkbox checkbox_custom checkbox_recommendation-form">
                        <input type="checkbox" class="form__input form__input_checkbox" name="id[]" value="10"/>
                        <span class="checkbox__label">Ссылки</span>
                    </label>
                    <label class="checkbox checkbox_custom checkbox_recommendation-form">
                        <input type="checkbox" class="form__input form__input_checkbox" name="id[]" value="11"/>
                        <span class="checkbox__label">Оформление кода</span>
                    </label>
                    <label class="checkbox checkbox_custom checkbox_recommendation-form">
                        <input type="checkbox" class="form__input form__input_checkbox" name="id[]" value="12"/>
                        <span class="checkbox__label">Рекламный характер</span>
                    </label>
                </div>
            </fieldset>

            <div class="form__footer">
                <button type="submit" class="btn btn_x-large btn_outline_blue" disabled>Отправить</button>
            </div>
        </form>


        <div class="company-info company-info_post-additional">
            <div class="company-info__about">
                <div class="media-obj media-obj_company">
                    <a href="/company/funcorp/" class="media-obj__image page-header__image">
                        <img src="//habrastorage.org/getpro/habr/company/b82/d35/e95/b82d35e95cc35c80a34a7dad4d08f5c7.jpg" width="48" height="48" class="company-info__image-pic"/>
                    </a>

                    <div class="media-obj__body media-obj__body_page-header media-obj__body_page-header_branding">

                        <div class="page-header__info">
                            <div class="page-header__info-title">FunCorp</div>
                            <sup class="page-header__stats-value page-header__stats-value_branding" title="Рейтинг компании">280,70</sup>
                            <div class="page-header__info-desc">
                                Разработка развлекательных сервисов
                            </div>
                        </div>
                    </div>
                </div>


            </div>
            <div class="company-info__author">
                <div class="user-info">
                    <div class="user-info__stats">
                        <div class="media-obj media-obj_user-info">
                            <a href="https://habr.com/ru/users/rjhdby/" class="media-obj__image" onclick="if (typeof ga === 'function') { ga('send', 'event', 'author_info_top', 'profile', 'rjhdby');}">
                                <img src="//habrastorage.org/getpro/habr/avatars/330/b5e/fbd/330b5efbd4f629e93730d497b8d865e1.png" width="48" height="48" class="media-obj__image-pic media-obj__image-pic_user"/>
                            </a>

                            <div class="media-obj__body media-obj__body_user-info">
                                <a href="https://habr.com/ru/info/help/karma/" class="user-info__stats-item stacked-counter" title="107 голосов">
                                    <div class="stacked-counter__value stacked-counter__value_green ">90,2</div>
                                    <div class="stacked-counter__label">Карма</div>
                                </a>

                                <a href="https://habr.com/ru/info/help/karma/#rating" class="user-info__stats-item stacked-counter stacked-counter_rating" title="Рейтинг пользователя">
                                    <div class="stacked-counter__value stacked-counter__value_magenta">128,0</div>
                                    <div class="stacked-counter__label">Рейтинг</div>
                                </a>

                                <a href="https://habr.com/ru/users/rjhdby/subscription/followers/" class="user-info__stats-item stacked-counter stacked-counter_subscribers" title="Количество подписчиков">
                                    <div class="stacked-counter__value stacked-counter__value_blue">27</div>
                                    <div class="stacked-counter__label">Подписчики</div>
                                </a>

                                <a href="https://habr.com/ru/users/rjhdby/subscription/follow/" class="user-info__stats-item stacked-counter stacked-counter_subscribers" title="Количество подписок">
                                    <div class="stacked-counter__value stacked-counter__value_blue">1</div>
                                    <div class="stacked-counter__label">Подписки</div>
                                </a>
                            </div>
                        </div>

                    </div>


                    <div class="user-info__about">
                        <div class="user-info__links">
                            <a href="https://habr.com/ru/users/rjhdby/" class="user-info__fullname" onclick="if (typeof ga === 'function') { ga('send', 'event', 'author_info_top', 'profile', 'rjhdby');}">Громов Андрей</a>&nbsp;<a href="/users/rjhdby/" class="user-info__nickname user-info__nickname_doggy" onclick="if (typeof ga === 'function') { ga('send', 'event', 'author_info_top', 'profile', 'rjhdby');}">rjhdby</a>

                        </div>

                        <div class="user-info__specialization">Ни то ни сё</div>
                    </div>

                </div>

            </div>

        </div>
        <div class="post-share">
            <span class="post-share__title">Поделиться публикацией</span>
            <ul class="post-share__buttons social-icons">
                <li class="social-icons__item social-icons__item_post">
                    <a href="https://www.facebook.com/sharer/sharer.php?u=https://habr.com/ru/company/funcorp/blog/460285/"
                       class="social-icons__item-link social-icons__item-link_normal social-icons__item-link_facebook"
                       title="Опубликовать ссылку в Facebook"
                       onclick="window.open(this.href, 'Опубликовать ссылку в Facebook', 'width=640,height=436,toolbar=0,status=0'); return false"
                    >
                        <svg class="icon-svg" aria-hidden="true" aria-labelledby="title" version="1.1" role="img" width="24" height="24" viewBox="0 0 24 24"><path d="M14.889 8.608h-1.65c-.195 0-.413.257-.413.6v1.192h2.063v1.698h-2.063v5.102h-1.948v-5.102h-1.766v-1.698h1.766v-1c0-1.434.995-2.6 2.361-2.6h1.65v1.808z"/></svg>
                    </a>
                </li>
                <li class="social-icons__item social-icons__item_post">
                    <a href="https://twitter.com/intent/tweet?text=%D0%9F%D1%80%D0%B5%D0%BF%D0%B0%D1%80%D0%B8%D1%80%D1%83%D0%B5%D0%BC+PHP.+%D0%9A%D0%B0%D0%BA+%D1%83%D1%81%D1%82%D1%80%D0%BE%D0%B5%D0%BD%D1%8B+while%2C+foreach%2C+array_walk+%D0%B8+%D0%BD%D0%B5%D0%BA%D0%BE%D1%82%D0%BE%D1%80%D1%8B%D0%B5+%D0%B4%D1%80%D1%83%D0%B3%D0%B8%D0%B5+%D1%81%D1%82%D1%80%D0%B0%D1%88%D0%BD%D1%8B%D0%B5+%D1%81%D0%BB%D0%BE%D0%B2%D0%B0+https://habr.com/p/460285/+via+%40habr_com"
                       class="social-icons__item-link social-icons__item-link_normal social-icons__item-link_twitter"
                       title="Опубликовать ссылку в Twitter"
                       onclick="window.open(this.href, 'Опубликовать ссылку в Twitter', 'width=800,height=300,resizable=yes,toolbar=0,status=0'); return false"
                    >
                        <svg class="icon-svg" aria-hidden="true" aria-labelledby="title" version="1.1" role="img" width="24" height="24" viewBox="0 0 24 24"><path d="M17.414 8.642c-.398.177-.826.296-1.276.35.459-.275.811-.71.977-1.229-.43.254-.905.439-1.41.539-.405-.432-.982-.702-1.621-.702-1.227 0-2.222.994-2.222 2.222 0 .174.019.344.058.506-1.846-.093-3.484-.978-4.579-2.322-.191.328-.301.71-.301 1.117 0 .77.392 1.45.988 1.849-.363-.011-.706-.111-1.006-.278v.028c0 1.077.766 1.974 1.782 2.178-.187.051-.383.078-.586.078-.143 0-.282-.014-.418-.04.282.882 1.103 1.525 2.075 1.542-.76.596-1.718.951-2.759.951-.179 0-.356-.01-.53-.031.983.63 2.15.998 3.406.998 4.086 0 6.321-3.386 6.321-6.321l-.006-.287c.433-.314.81-.705 1.107-1.15z"/></svg>
                    </a>
                </li>
                <li class="social-icons__item social-icons__item_post">
                    <a href="https://vk.com/share.php?url=https://habr.com/ru/company/funcorp/blog/460285/"
                       class="social-icons__item-link social-icons__item-link_normal social-icons__item-link_vkontakte"
                       title="Опубликовать ссылку во ВКонтакте"
                       onclick="window.open(this.href, 'Опубликовать ссылку во ВКонтакте', 'width=800,height=300,toolbar=0,status=0'); return false"
                    >
                        <svg class="icon-svg" aria-hidden="true" aria-labelledby="title" version="1.1" role="img" width="24" height="24" viewBox="0 0 24 24"><path d="M16.066 11.93s1.62-2.286 1.782-3.037c.054-.268-.064-.418-.343-.418h-1.406c-.322 0-.44.139-.537.343 0 0-.76 1.619-1.685 2.64-.297.33-.448.429-.612.429-.132 0-.193-.11-.193-.408v-2.607c0-.365-.043-.472-.343-.472h-2.254c-.172 0-.279.1-.279.236 0 .343.526.421.526 1.352v1.921c0 .386-.022.537-.204.537-.483 0-1.631-1.663-2.274-3.552-.129-.386-.268-.494-.633-.494h-1.406c-.204 0-.354.139-.354.343 0 .375.44 2.114 2.167 4.442 1.159 1.566 2.683 2.414 4.056 2.414.838 0 1.041-.139 1.041-.494v-1.202c0-.301.118-.429.29-.429.193 0 .534.062 1.33.848.945.901 1.01 1.276 1.525 1.276h1.578c.161 0 .311-.075.311-.343 0-.354-.462-.987-1.17-1.738-.29-.386-.762-.805-.912-.998-.215-.226-.151-.354-.001-.59z"/></svg>
                    </a>
                </li>
                <li class="social-icons__item social-icons__item_post">
                    <a href="https://t.me/share/url?url=https://habr.com/ru/company/funcorp/blog/460285/&title=Препарируем PHP. Как устроены while, foreach, array_walk и некоторые другие страшные слова"
                       class="social-icons__item-link social-icons__item-link_normal social-icons__item-link_telegram"
                       title="Поделиться ссылкой в Telegram"
                       onclick="window.open(this.href, 'Поделиться ссылкой в Telegram', 'width=800,height=300,toolbar=0,status=0'); return false"
                    >
                        <svg class="icon-svg" aria-hidden="true" aria-labelledby="title" version="1.1" role="img" width="24" height="24" viewBox="0 0 24 24"><path d="M17.17 7.621l-10.498 3.699c-.169.059-.206.205-.006.286l2.257.904 1.338.536 6.531-4.796s.189.057.125.126l-4.68 5.062-.27.299.356.192 2.962 1.594c.173.093.397.016.447-.199.058-.254 1.691-7.29 1.728-7.447.047-.204-.087-.328-.291-.256zm-6.922 8.637c0 .147.082.188.197.084l1.694-1.522-1.891-.978v2.416z"/></svg>
                    </a>
                </li>
                <li class="social-icons__item social-icons__item_post">
                    <a href="https://getpocket.com/edit?url=https://habr.com/ru/company/funcorp/blog/460285/&title=Препарируем PHP. Как устроены while, foreach, array_walk и некоторые другие страшные слова"
                       class="social-icons__item-link social-icons__item-link_normal social-icons__item-link_pocket"
                       title="Добавить ссылку в Pocket"
                       target="_blank"
                    >
                    </a>
                </li>
            </ul>
        </div>


    </div>



    <div class="default-block default-block_content">
        <div class="default-block__header default-block__header_large">
            <h2 class="default-block__header-title default-block__header-title_large">Похожие публикации</h2>
        </div>
        <div class="default-block__content">

            <ul class="content-list">
                <li class="content-list__item content-list__item_devided post-info">
                    <span class="post-info__date">26 октября 2018 в 14:40</span>
                    <h3 class="post-info__title post-info__title_large">
                        <a href="https://habr.com/ru/post/427855/" class="post-info__title post-info__title_large" onclick="if (typeof ga === 'function') { ga('send', 'event', 'tm_block', 'similar_posts', 'common', '1'); }">
                            MOSDROID митап в FunCorp
                        </a>
                    </h3>

                    <div class="post-info__meta">
                <span class="post-info__meta-item">
                  <svg class="post-info__meta-icon icon-svg_rating" width="11" height="13"><use xlink:href="https://habr.com/images/1565957267/common-svg-sprite.svg#arrow-bold" /></svg>
                      <span class="post-info__meta-counter">+21</span>
                </span>
                        <span class="post-info__meta-item">
                  <svg class="post-info__meta-icon icon-svg_views" width="16" height="9"><use xlink:href="https://habr.com/images/1565957267/common-svg-sprite.svg#eye" /></svg><span class="post-info__meta-counter">1,7k</span>
                </span>
                        <span class="post-info__meta-item">
                  <svg class="post-info__meta-icon icon-svg_bookmark-mini" width="8" height="13"><use xlink:href="https://habr.com/images/1565957267/common-svg-sprite.svg#book" /></svg><span class="post-info__meta-counter">7</span>
                </span>
                        <a href="https://habr.com/ru/post/427855#comments" class="post-info__meta-item" rel="nofollow">
                            <svg class="post-info__meta-icon icon-svg_comments" width="14" height="13"><use xlink:href="https://habr.com/images/1565957267/common-svg-sprite.svg#comment" /></svg><span class="post-info__meta-counter">0</span>
                        </a>
                    </div>
                </li>
                <li class="content-list__item content-list__item_devided post-info">
                    <span class="post-info__date">31 августа 2018 в 11:40</span>
                    <h3 class="post-info__title post-info__title_large">
                        <a href="https://habr.com/ru/post/421883/" class="post-info__title post-info__title_large" onclick="if (typeof ga === 'function') { ga('send', 'event', 'tm_block', 'similar_posts', 'common', '2'); }">
                            Видео докладов с CocoaHeads @ FunCorp митапа
                        </a>
                    </h3>

                    <div class="post-info__meta">
                <span class="post-info__meta-item">
                  <svg class="post-info__meta-icon icon-svg_rating" width="11" height="13"><use xlink:href="https://habr.com/images/1565957267/common-svg-sprite.svg#arrow-bold" /></svg>
                      <span class="post-info__meta-counter">+18</span>
                </span>
                        <span class="post-info__meta-item">
                  <svg class="post-info__meta-icon icon-svg_views" width="16" height="9"><use xlink:href="https://habr.com/images/1565957267/common-svg-sprite.svg#eye" /></svg><span class="post-info__meta-counter">3,3k</span>
                </span>
                        <span class="post-info__meta-item">
                  <svg class="post-info__meta-icon icon-svg_bookmark-mini" width="8" height="13"><use xlink:href="https://habr.com/images/1565957267/common-svg-sprite.svg#book" /></svg><span class="post-info__meta-counter">17</span>
                </span>
                        <a href="https://habr.com/ru/post/421883#comments" class="post-info__meta-item" rel="nofollow">
                            <svg class="post-info__meta-icon icon-svg_comments" width="14" height="13"><use xlink:href="https://habr.com/images/1565957267/common-svg-sprite.svg#comment" /></svg><span class="post-info__meta-counter">0</span>
                        </a>
                    </div>
                </li>
                <li class="content-list__item content-list__item_devided post-info">
                    <span class="post-info__date">26 июля 2018 в 11:21</span>
                    <h3 class="post-info__title post-info__title_large">
                        <a href="https://habr.com/ru/post/418283/" class="post-info__title post-info__title_large" onclick="if (typeof ga === 'function') { ga('send', 'event', 'tm_block', 'similar_posts', 'common', '3'); }">
                            CocoaHeads в FunCorp
                        </a>
                    </h3>

                    <div class="post-info__meta">
                <span class="post-info__meta-item">
                  <svg class="post-info__meta-icon icon-svg_rating" width="11" height="13"><use xlink:href="https://habr.com/images/1565957267/common-svg-sprite.svg#arrow-bold" /></svg>
                      <span class="post-info__meta-counter">+16</span>
                </span>
                        <span class="post-info__meta-item">
                  <svg class="post-info__meta-icon icon-svg_views" width="16" height="9"><use xlink:href="https://habr.com/images/1565957267/common-svg-sprite.svg#eye" /></svg><span class="post-info__meta-counter">1,2k</span>
                </span>
                        <span class="post-info__meta-item">
                  <svg class="post-info__meta-icon icon-svg_bookmark-mini" width="8" height="13"><use xlink:href="https://habr.com/images/1565957267/common-svg-sprite.svg#book" /></svg><span class="post-info__meta-counter">6</span>
                </span>
                        <a href="https://habr.com/ru/post/418283#comments" class="post-info__meta-item" rel="nofollow">
                            <svg class="post-info__meta-icon icon-svg_comments" width="14" height="13"><use xlink:href="https://habr.com/images/1565957267/common-svg-sprite.svg#comment" /></svg><span class="post-info__meta-counter">0</span>
                        </a>
                    </div>
                </li>
            </ul>

        </div>
    </div>



    <div class="promo-block promo-block_vacancies">
        <h3 class="promo-block__header promo-block__header_justify">
      <span class="promo-block__header-wrap">
        <a href="https://moikrug.ru/companies/funcorp/vacancies" target="_blank" data-utm="?utm_source=tm_habrahabr&utm_medium=tm_block&utm_content=block_name&utm_campaign=vacancies_company" class="promo-block__title-link">
          <img src="//habrastorage.org/getpro/habr/company/b82/d35/e95/b82d35e95cc35c80a34a7dad4d08f5c7.jpg" width="28" height="28" class="promo-block__title-img">
          Вакансии компании FunCorp
        </a>
      </span>
            <a href="https://moikrug.ru/" target="_blank" data-utm="?utm_source=tm_habrahabr&utm_medium=tm_block&utm_content=block_logo&utm_campaign=vacancies_company" class="icon-svg_logo-mk" rel="nofollow"><svg class="icon-svg" width="114" height="24" viewBox="0 0 114 24" aria-hidden="true" aria-labelledby="title" version="1.1" role="img"><path d="M9.383 7.685l-4.127-6.87h-5.258v17.614h4.931v-10.166l3.497 5.963h1.938l3.497-5.963v10.166h4.931v-17.614h-5.258zM27.726 5.47c-3.673 0-6.667 2.994-6.667 6.667s2.994 6.667 6.667 6.667c3.673 0 6.667-2.994 6.667-6.667.001-3.673-2.993-6.667-6.667-6.667zm0 8.908c-1.208 0-2.239-.982-2.239-2.239 0-1.208 1.032-2.239 2.239-2.239s2.239 1.032 2.239 2.239c.001 1.257-1.031 2.239-2.239 2.239zM40.835 11.735v-5.888h-4.68v12.582h4.68l4.152-5.888v5.888h4.68v-12.582h-4.68zM47.906 3.055l-1.359-2.617c-1.309.704-2.617.982-3.623.982s-2.341-.278-3.649-.982l-1.359 2.617c1.811.982 3.397 1.283 5.007 1.283s3.171-.303 4.983-1.283zM57.971 5.847v12.582h4.68v-12.582zM71.432 5.847h-4.931l-3.849 6.19 4.101 6.392h4.933l-4.353-6.392zM81.12 5.47c-1.283 0-2.667.806-3.246 2.013v-1.635h-4.68v17.713h4.68v-6.768c.578 1.208 1.962 2.013 3.246 2.013 3.673 0 5.662-2.994 5.662-6.667s-1.989-6.667-5.662-6.667zm-1.51 8.908c-.68 0-1.309-.327-1.736-.856v-2.792c.427-.503 1.057-.83 1.736-.83 1.208 0 2.239.982 2.239 2.239.001 1.207-1.031 2.239-2.239 2.239zM94.909 12.692l-2.666-6.845h-5.083l5.335 12.557-2.165 5.158h4.655l7.926-17.714h-5.082zM104.042 5.847v12.582h4.68v-8.304h5.561v-4.278z"/></svg></a>
        </h3>

        <div class="promo-block__content">
            <ul class="content-list content-list_promo" >
                <li class="content-list__item content-list__item_promo">
                    <a href="https://moikrug.ru/vacancies/1000052380" class="promo-item" data-utm="?utm_source=tm_habrahabr&utm_medium=tm_block&utm_content=vacancy&utm_campaign=vacancies_company">
                        <div class="promo-item__wrap">
                            <span class="promo-item__title promo-item__title_hovered">Аналитик</span>
                            <div class="promo-item__attrs">
                                <span class="promo-item__attrs-item">Москва</span>

                            </div>
                        </div>

                        <div class="promo-item__aside promo-item__aside_hovered">
                            <div class="promo-item__amount promo-item__amount_rur">
                                <span title="150000 rur">до 150 000</span>
                            </div>
                        </div>

                    </a>







                </li>
            </ul>
        </div>

        <div class="promo-block__footer">
            <a href="https://moikrug.ru/companies/funcorp/vacancies/" data-utm="?utm_source=tm_habrahabr&utm_medium=tm_block&utm_content=vacancies_all&utm_campaign=vacancies_company" class="promo-block__footer-link promo-block__footer-link_selected" target="_blank" rel="nofollow">Вакансии компании</a>
            <a href="https://moikrug.ru/info/employer/" data-utm="?utm_source=tm_habrahabr&utm_medium=tm_block&&utm_content=vacancy_add&utm_campaign=vacancies_company" class="promo-block__footer-link" target="_blank" rel="nofollow">Создать резюме</a>
        </div>
    </div>


    <div class="comments-section" id="comments">
        <header class="comments-section__head">
            <h2 class="comments-section__head-title">
                Комментарии
                <span class="comments-section__head-counter" id="comments_count">
          25
        </span>
            </h2>

        </header>



        <ul class="content-list content-list_comments" id="comments-list">
            <li class="content-list__item content-list__item_comment js-comment " rel="20422219">

                <span class="parent_id" data-parent_id="0"></span>
                <div class="comment" id="comment_20422219">
                    <span class="comment__folding-dotholder"></span>
                    <div class="comment__head   " rel="20422219">
                        <a href="https://habr.com/ru/users/E11E/" class="user-info user-info_inline"  data-user-login="E11E">
                            <svg class="default-image default-image_mini default-image_green" width="24" height="24"><use xlink:href="https://habr.com/images/1565957267/common-svg-sprite.svg#slug" /></svg>
                            <span class="user-info__nickname user-info__nickname_small user-info__nickname_comment">E11E</span>
                        </a>


                        <time class="comment__date-time comment__date-time_published">23 июля 2019 в 12:15</time>
                        <ul class="inline-list inline-list_comment-nav">
                            <li class="inline-list__item inline-list__item_comment-nav">
                                <a href="#comment_20422219" class="icon_comment-anchor" title="Ссылка на комментарий"><svg width="12" height="12"><use xlink:href="https://habr.com/images/1565957267/common-svg-sprite.svg#anchor" /></svg></a>
                            </li>

                            <li class="inline-list__item inline-list__item_comment-nav">
                                <a href="#" class="icon_comment-bookmark " onclick="return comments_add_to_favorite(this)" data-type="3" data-id="20422219" data-action="add" title="Только зарегистрированные пользователи могут добавлять комментарии в избранное" disabled><svg width="12" height="12"><use xlink:href="https://habr.com/images/1565957267/common-svg-sprite.svg#book" /></svg></a>
                            </li>


                            <li class="inline-list__item inline-list__item_comment-nav hidden js-comment_children">
                                <a href="#comment_20422219" class="icon_comment-arrow-down js-comment_parent back_to_children" data-id="20422219" data-parent_id="0" title="Вернуться к ответу"><svg width="12" height="12"><use xlink:href="https://habr.com/images/1565957267/common-svg-sprite.svg#rounded-arrow" /></svg></a>
                            </li>
                        </ul>

                        <div class="voting-wjt voting-wjt_comments js-comment-vote" data-id="20422219" data-post-target="460285" data-type="3">
                            <button type="button" class="btn voting-wjt__button voting-wjt__button_small " data-action="plus" onclick="comment_vote(this);" title="Голосовать могут только зарегистрированные пользователи" disabled><svg class="icon-svg_arrow-up" width="10" height="16"><use xlink:href="https://habr.com/images/1565957267/common-svg-sprite.svg#vote-arrow" /></svg></button>

                            <span class="voting-wjt__counter   js-score" title="Общий рейтинг 0: &uarr;0 и &darr;0">0</span>

                            <button type="button" class="btn voting-wjt__button voting-wjt__button_small " data-action="minus" onclick="comment_vote(this);" title="Голосовать могут только зарегистрированные пользователи" disabled><svg class="icon-svg_arrow-down" width="10" height="16"><use xlink:href="https://habr.com/images/1565957267/common-svg-sprite.svg#vote-arrow" /></svg></button>
                        </div>

                    </div>

                    <div class="comment__message ">Спасибо тебе добрый человек. По тестам всегда знал, что в PHP foreach for while самые быстрые и использовал их — но вот залезть под капот всё не было ни времени ни желания, а ты это сделал для нас всех, таких ленивых.</div>

                    <div class="comment__footer">
                    </div>

                    <div class="comment__reply-form js-form_placeholder"></div>
                </div>

                <ul class="content-list content-list_nested-comments content-list_nested-comments-0" id="reply_comments_20422219"></ul>
            </li>
            <li class="content-list__item content-list__item_comment js-comment " rel="20423349">

                <span class="parent_id" data-parent_id="0"></span>
                <div class="comment" id="comment_20423349">
                    <span class="comment__folding-dotholder"></span>
                    <div class="comment__head   " rel="20423349">
                        <a href="https://habr.com/ru/users/Programmer/" class="user-info user-info_inline"  data-user-login="Programmer">
                            <img src="//habrastorage.org/r/w48/getpro/habr/avatars/f71/8d4/123/f718d41238a7c754e0a12357954d9eaa.jpg" class="user-info__image-pic user-info__image-pic_small" width="24" height="24" />
                            <span class="user-info__nickname user-info__nickname_small user-info__nickname_comment">Programmer</span>
                        </a>


                        <time class="comment__date-time comment__date-time_published">23 июля 2019 в 14:42</time>
                        <ul class="inline-list inline-list_comment-nav">
                            <li class="inline-list__item inline-list__item_comment-nav">
                                <a href="#comment_20423349" class="icon_comment-anchor" title="Ссылка на комментарий"><svg width="12" height="12"><use xlink:href="https://habr.com/images/1565957267/common-svg-sprite.svg#anchor" /></svg></a>
                            </li>

                            <li class="inline-list__item inline-list__item_comment-nav">
                                <a href="#" class="icon_comment-bookmark " onclick="return comments_add_to_favorite(this)" data-type="3" data-id="20423349" data-action="add" title="Только зарегистрированные пользователи могут добавлять комментарии в избранное" disabled><svg width="12" height="12"><use xlink:href="https://habr.com/images/1565957267/common-svg-sprite.svg#book" /></svg></a>
                            </li>


                            <li class="inline-list__item inline-list__item_comment-nav hidden js-comment_children">
                                <a href="#comment_20423349" class="icon_comment-arrow-down js-comment_parent back_to_children" data-id="20423349" data-parent_id="0" title="Вернуться к ответу"><svg width="12" height="12"><use xlink:href="https://habr.com/images/1565957267/common-svg-sprite.svg#rounded-arrow" /></svg></a>
                            </li>
                        </ul>

                        <div class="voting-wjt voting-wjt_comments js-comment-vote" data-id="20423349" data-post-target="460285" data-type="3">
                            <button type="button" class="btn voting-wjt__button voting-wjt__button_small " data-action="plus" onclick="comment_vote(this);" title="Голосовать могут только зарегистрированные пользователи" disabled><svg class="icon-svg_arrow-up" width="10" height="16"><use xlink:href="https://habr.com/images/1565957267/common-svg-sprite.svg#vote-arrow" /></svg></button>

                            <span class="voting-wjt__counter   js-score" title="Общий рейтинг 0: &uarr;0 и &darr;0">0</span>

                            <button type="button" class="btn voting-wjt__button voting-wjt__button_small " data-action="minus" onclick="comment_vote(this);" title="Голосовать могут только зарегистрированные пользователи" disabled><svg class="icon-svg_arrow-down" width="10" height="16"><use xlink:href="https://habr.com/images/1565957267/common-svg-sprite.svg#vote-arrow" /></svg></button>
                        </div>

                    </div>

                    <div class="comment__message "><p>А что насчет лямбды объявленной со словом <em>static</em></p><br>
                        <pre><code class="php">array_walk($arr, static function($value, $key){ echo $key.$value;});</code></pre><br>
                        <p>По идее должно быть быстрее если вызовы горячие</p></div>

                    <div class="comment__footer">
                    </div>

                    <div class="comment__reply-form js-form_placeholder"></div>
                </div>

                <ul class="content-list content-list_nested-comments content-list_nested-comments-0" id="reply_comments_20423349">  <li class="content-list__item content-list__item_comment js-comment " rel="20423557">

                        <span class="parent_id" data-parent_id="20423349"></span>
                        <div class="comment" id="comment_20423557">
                            <span class="comment__folding-dotholder"></span>
                            <div class="comment__head comment__head_topic-author  " rel="20423557">
                                <a href="https://habr.com/ru/users/rjhdby/" class="user-info user-info_inline"  data-user-login="rjhdby">
                                    <img src="//habrastorage.org/r/w48/getpro/habr/avatars/330/b5e/fbd/330b5efbd4f629e93730d497b8d865e1.png" class="user-info__image-pic user-info__image-pic_small" width="24" height="24" />
                                    <span class="user-info__nickname user-info__nickname_small user-info__nickname_comment">rjhdby</span>
                                </a>


                                <time class="comment__date-time comment__date-time_published">23 июля 2019 в 15:16</time>
                                <ul class="inline-list inline-list_comment-nav">
                                    <li class="inline-list__item inline-list__item_comment-nav">
                                        <a href="#comment_20423557" class="icon_comment-anchor" title="Ссылка на комментарий"><svg width="12" height="12"><use xlink:href="https://habr.com/images/1565957267/common-svg-sprite.svg#anchor" /></svg></a>
                                    </li>

                                    <li class="inline-list__item inline-list__item_comment-nav">
                                        <a href="#" class="icon_comment-bookmark " onclick="return comments_add_to_favorite(this)" data-type="3" data-id="20423557" data-action="add" title="Только зарегистрированные пользователи могут добавлять комментарии в избранное" disabled><svg width="12" height="12"><use xlink:href="https://habr.com/images/1565957267/common-svg-sprite.svg#book" /></svg></a>
                                    </li>

                                    <li class="inline-list__item inline-list__item_comment-nav">
                                        <span class="icon_comment-branch js-comment_tree" data-id="20423557" data-parent_id="20423349" title="Показать ветку комментариев"><svg width="8" height="12"><use xlink:href="https://habr.com/images/1565957267/common-svg-sprite.svg#tree" /></svg></span>
                                    </li>

                                    <li class="inline-list__item inline-list__item_comment-nav">
                                        <a href="#comment_20423349" class="icon_comment-arrow-up js-comment_parent" data-id="20423557" data-parent_id="20423349" title="Показать предыдущий комментарий"><svg width="12" height="12"><use xlink:href="https://habr.com/images/1565957267/common-svg-sprite.svg#rounded-arrow" /></svg></a>
                                    </li>

                                    <li class="inline-list__item inline-list__item_comment-nav hidden js-comment_children">
                                        <a href="#comment_20423557" class="icon_comment-arrow-down js-comment_parent back_to_children" data-id="20423557" data-parent_id="20423349" title="Вернуться к ответу"><svg width="12" height="12"><use xlink:href="https://habr.com/images/1565957267/common-svg-sprite.svg#rounded-arrow" /></svg></a>
                                    </li>
                                </ul>

                                <div class="voting-wjt voting-wjt_comments js-comment-vote" data-id="20423557" data-post-target="460285" data-type="3">
                                    <button type="button" class="btn voting-wjt__button voting-wjt__button_small " data-action="plus" onclick="comment_vote(this);" title="Голосовать могут только зарегистрированные пользователи" disabled><svg class="icon-svg_arrow-up" width="10" height="16"><use xlink:href="https://habr.com/images/1565957267/common-svg-sprite.svg#vote-arrow" /></svg></button>

                                    <span class="voting-wjt__counter   js-score" title="Общий рейтинг 0: &uarr;0 и &darr;0">0</span>

                                    <button type="button" class="btn voting-wjt__button voting-wjt__button_small " data-action="minus" onclick="comment_vote(this);" title="Голосовать могут только зарегистрированные пользователи" disabled><svg class="icon-svg_arrow-down" width="10" height="16"><use xlink:href="https://habr.com/images/1565957267/common-svg-sprite.svg#vote-arrow" /></svg></button>
                                </div>

                            </div>

                            <div class="comment__message ">Добавил замер для статической функции. Разницы не заметно.</div>

                            <div class="comment__footer">
                            </div>

                            <div class="comment__reply-form js-form_placeholder"></div>
                        </div>

                        <ul class="content-list content-list_nested-comments content-list_nested-comments-1" id="reply_comments_20423557">  <li class="content-list__item content-list__item_comment js-comment " rel="20425393">

                                <span class="parent_id" data-parent_id="20423557"></span>
                                <div class="comment" id="comment_20425393">
                                    <span class="comment__folding-dotholder"></span>
                                    <div class="comment__head   " rel="20425393">
                                        <a href="https://habr.com/ru/users/VolCh/" class="user-info user-info_inline"  data-user-login="VolCh">
                                            <svg class="default-image default-image_mini default-image_lilac" width="24" height="24"><use xlink:href="https://habr.com/images/1565957267/common-svg-sprite.svg#slug" /></svg>
                                            <span class="user-info__nickname user-info__nickname_small user-info__nickname_comment">VolCh</span>
                                        </a>


                                        <time class="comment__date-time comment__date-time_published">23 июля 2019 в 21:54</time>
                                        <ul class="inline-list inline-list_comment-nav">
                                            <li class="inline-list__item inline-list__item_comment-nav">
                                                <a href="#comment_20425393" class="icon_comment-anchor" title="Ссылка на комментарий"><svg width="12" height="12"><use xlink:href="https://habr.com/images/1565957267/common-svg-sprite.svg#anchor" /></svg></a>
                                            </li>

                                            <li class="inline-list__item inline-list__item_comment-nav">
                                                <a href="#" class="icon_comment-bookmark " onclick="return comments_add_to_favorite(this)" data-type="3" data-id="20425393" data-action="add" title="Только зарегистрированные пользователи могут добавлять комментарии в избранное" disabled><svg width="12" height="12"><use xlink:href="https://habr.com/images/1565957267/common-svg-sprite.svg#book" /></svg></a>
                                            </li>

                                            <li class="inline-list__item inline-list__item_comment-nav">
                                                <span class="icon_comment-branch js-comment_tree" data-id="20425393" data-parent_id="20423557" title="Показать ветку комментариев"><svg width="8" height="12"><use xlink:href="https://habr.com/images/1565957267/common-svg-sprite.svg#tree" /></svg></span>
                                            </li>

                                            <li class="inline-list__item inline-list__item_comment-nav">
                                                <a href="#comment_20423557" class="icon_comment-arrow-up js-comment_parent" data-id="20425393" data-parent_id="20423557" title="Показать предыдущий комментарий"><svg width="12" height="12"><use xlink:href="https://habr.com/images/1565957267/common-svg-sprite.svg#rounded-arrow" /></svg></a>
                                            </li>

                                            <li class="inline-list__item inline-list__item_comment-nav hidden js-comment_children">
                                                <a href="#comment_20425393" class="icon_comment-arrow-down js-comment_parent back_to_children" data-id="20425393" data-parent_id="20423557" title="Вернуться к ответу"><svg width="12" height="12"><use xlink:href="https://habr.com/images/1565957267/common-svg-sprite.svg#rounded-arrow" /></svg></a>
                                            </li>
                                        </ul>

                                        <div class="voting-wjt voting-wjt_comments js-comment-vote" data-id="20425393" data-post-target="460285" data-type="3">
                                            <button type="button" class="btn voting-wjt__button voting-wjt__button_small " data-action="plus" onclick="comment_vote(this);" title="Голосовать могут только зарегистрированные пользователи" disabled><svg class="icon-svg_arrow-up" width="10" height="16"><use xlink:href="https://habr.com/images/1565957267/common-svg-sprite.svg#vote-arrow" /></svg></button>

                                            <span class="voting-wjt__counter voting-wjt__counter_positive  js-score" title="Общий рейтинг +1: &uarr;1 и &darr;0">+1</span>

                                            <button type="button" class="btn voting-wjt__button voting-wjt__button_small " data-action="minus" onclick="comment_vote(this);" title="Голосовать могут только зарегистрированные пользователи" disabled><svg class="icon-svg_arrow-down" width="10" height="16"><use xlink:href="https://habr.com/images/1565957267/common-svg-sprite.svg#vote-arrow" /></svg></button>
                                        </div>

                                    </div>

                                    <div class="comment__message "><p>Шторм советует добавить статик с пометкой "микрооптимизация". Я добавляю :) </p></div>

                                    <div class="comment__footer">
                                    </div>

                                    <div class="comment__reply-form js-form_placeholder"></div>
                                </div>

                                <ul class="content-list content-list_nested-comments content-list_nested-comments-2" id="reply_comments_20425393">  <li class="content-list__item content-list__item_comment js-comment " rel="20425417">

                                        <span class="parent_id" data-parent_id="20425393"></span>
                                        <div class="comment" id="comment_20425417">
                                            <span class="comment__folding-dotholder"></span>
                                            <div class="comment__head comment__head_topic-author  " rel="20425417">
                                                <a href="https://habr.com/ru/users/rjhdby/" class="user-info user-info_inline"  data-user-login="rjhdby">
                                                    <img src="//habrastorage.org/r/w48/getpro/habr/avatars/330/b5e/fbd/330b5efbd4f629e93730d497b8d865e1.png" class="user-info__image-pic user-info__image-pic_small" width="24" height="24" />
                                                    <span class="user-info__nickname user-info__nickname_small user-info__nickname_comment">rjhdby</span>
                                                </a>


                                                <time class="comment__date-time comment__date-time_published">23 июля 2019 в 22:06</time>
                                                <ul class="inline-list inline-list_comment-nav">
                                                    <li class="inline-list__item inline-list__item_comment-nav">
                                                        <a href="#comment_20425417" class="icon_comment-anchor" title="Ссылка на комментарий"><svg width="12" height="12"><use xlink:href="https://habr.com/images/1565957267/common-svg-sprite.svg#anchor" /></svg></a>
                                                    </li>

                                                    <li class="inline-list__item inline-list__item_comment-nav">
                                                        <a href="#" class="icon_comment-bookmark " onclick="return comments_add_to_favorite(this)" data-type="3" data-id="20425417" data-action="add" title="Только зарегистрированные пользователи могут добавлять комментарии в избранное" disabled><svg width="12" height="12"><use xlink:href="https://habr.com/images/1565957267/common-svg-sprite.svg#book" /></svg></a>
                                                    </li>

                                                    <li class="inline-list__item inline-list__item_comment-nav">
                                                        <span class="icon_comment-branch js-comment_tree" data-id="20425417" data-parent_id="20425393" title="Показать ветку комментариев"><svg width="8" height="12"><use xlink:href="https://habr.com/images/1565957267/common-svg-sprite.svg#tree" /></svg></span>
                                                    </li>

                                                    <li class="inline-list__item inline-list__item_comment-nav">
                                                        <a href="#comment_20425393" class="icon_comment-arrow-up js-comment_parent" data-id="20425417" data-parent_id="20425393" title="Показать предыдущий комментарий"><svg width="12" height="12"><use xlink:href="https://habr.com/images/1565957267/common-svg-sprite.svg#rounded-arrow" /></svg></a>
                                                    </li>

                                                    <li class="inline-list__item inline-list__item_comment-nav hidden js-comment_children">
                                                        <a href="#comment_20425417" class="icon_comment-arrow-down js-comment_parent back_to_children" data-id="20425417" data-parent_id="20425393" title="Вернуться к ответу"><svg width="12" height="12"><use xlink:href="https://habr.com/images/1565957267/common-svg-sprite.svg#rounded-arrow" /></svg></a>
                                                    </li>
                                                </ul>

                                                <div class="voting-wjt voting-wjt_comments js-comment-vote" data-id="20425417" data-post-target="460285" data-type="3">
                                                    <button type="button" class="btn voting-wjt__button voting-wjt__button_small " data-action="plus" onclick="comment_vote(this);" title="Голосовать могут только зарегистрированные пользователи" disabled><svg class="icon-svg_arrow-up" width="10" height="16"><use xlink:href="https://habr.com/images/1565957267/common-svg-sprite.svg#vote-arrow" /></svg></button>

                                                    <span class="voting-wjt__counter voting-wjt__counter_positive  js-score" title="Общий рейтинг +1: &uarr;1 и &darr;0">+1</span>

                                                    <button type="button" class="btn voting-wjt__button voting-wjt__button_small " data-action="minus" onclick="comment_vote(this);" title="Голосовать могут только зарегистрированные пользователи" disabled><svg class="icon-svg_arrow-down" width="10" height="16"><use xlink:href="https://habr.com/images/1565957267/common-svg-sprite.svg#vote-arrow" /></svg></button>
                                                </div>

                                            </div>

                                            <div class="comment__message ">Ну да, как тут не нажать ALT+ENTER, если подсвечено!?<br>
                                                Вообще забавно, как среда разработки сама формирует нужный ей стиль кода. Добавят завтра джетбрейновцы A/B эксперимет, который будет советовать, в качестве оптимизации, заменить while на if/goto и ведь многие поведутся и будут потом уверены, что это влияет на производительность.<br>
                                                <br>
                                                PS Я тоже добавляю :D</div>

                                            <div class="comment__footer">
                                            </div>

                                            <div class="comment__reply-form js-form_placeholder"></div>
                                        </div>

                                        <ul class="content-list content-list_nested-comments content-list_nested-comments-3" id="reply_comments_20425417">  <li class="content-list__item content-list__item_comment js-comment " rel="20425783">

                                                <span class="parent_id" data-parent_id="20425417"></span>
                                                <div class="comment" id="comment_20425783">
                                                    <span class="comment__folding-dotholder"></span>
                                                    <div class="comment__head   " rel="20425783">
                                                        <a href="https://habr.com/ru/users/t_kanstantsin/" class="user-info user-info_inline"  data-user-login="t_kanstantsin">
                                                            <svg class="default-image default-image_mini default-image_blue" width="24" height="24"><use xlink:href="https://habr.com/images/1565957267/common-svg-sprite.svg#slug" /></svg>
                                                            <span class="user-info__nickname user-info__nickname_small user-info__nickname_comment">t_kanstantsin</span>
                                                        </a>


                                                        <time class="comment__date-time comment__date-time_published">23 июля 2019 в 23:57</time>
                                                        <ul class="inline-list inline-list_comment-nav">
                                                            <li class="inline-list__item inline-list__item_comment-nav">
                                                                <a href="#comment_20425783" class="icon_comment-anchor" title="Ссылка на комментарий"><svg width="12" height="12"><use xlink:href="https://habr.com/images/1565957267/common-svg-sprite.svg#anchor" /></svg></a>
                                                            </li>

                                                            <li class="inline-list__item inline-list__item_comment-nav">
                                                                <a href="#" class="icon_comment-bookmark " onclick="return comments_add_to_favorite(this)" data-type="3" data-id="20425783" data-action="add" title="Только зарегистрированные пользователи могут добавлять комментарии в избранное" disabled><svg width="12" height="12"><use xlink:href="https://habr.com/images/1565957267/common-svg-sprite.svg#book" /></svg></a>
                                                            </li>

                                                            <li class="inline-list__item inline-list__item_comment-nav">
                                                                <span class="icon_comment-branch js-comment_tree" data-id="20425783" data-parent_id="20425417" title="Показать ветку комментариев"><svg width="8" height="12"><use xlink:href="https://habr.com/images/1565957267/common-svg-sprite.svg#tree" /></svg></span>
                                                            </li>

                                                            <li class="inline-list__item inline-list__item_comment-nav">
                                                                <a href="#comment_20425417" class="icon_comment-arrow-up js-comment_parent" data-id="20425783" data-parent_id="20425417" title="Показать предыдущий комментарий"><svg width="12" height="12"><use xlink:href="https://habr.com/images/1565957267/common-svg-sprite.svg#rounded-arrow" /></svg></a>
                                                            </li>

                                                            <li class="inline-list__item inline-list__item_comment-nav hidden js-comment_children">
                                                                <a href="#comment_20425783" class="icon_comment-arrow-down js-comment_parent back_to_children" data-id="20425783" data-parent_id="20425417" title="Вернуться к ответу"><svg width="12" height="12"><use xlink:href="https://habr.com/images/1565957267/common-svg-sprite.svg#rounded-arrow" /></svg></a>
                                                            </li>
                                                        </ul>

                                                        <div class="voting-wjt voting-wjt_comments js-comment-vote" data-id="20425783" data-post-target="460285" data-type="3">
                                                            <button type="button" class="btn voting-wjt__button voting-wjt__button_small " data-action="plus" onclick="comment_vote(this);" title="Голосовать могут только зарегистрированные пользователи" disabled><svg class="icon-svg_arrow-up" width="10" height="16"><use xlink:href="https://habr.com/images/1565957267/common-svg-sprite.svg#vote-arrow" /></svg></button>

                                                            <span class="voting-wjt__counter   js-score" title="Общий рейтинг 0: &uarr;0 и &darr;0">0</span>

                                                            <button type="button" class="btn voting-wjt__button voting-wjt__button_small " data-action="minus" onclick="comment_vote(this);" title="Голосовать могут только зарегистрированные пользователи" disabled><svg class="icon-svg_arrow-down" width="10" height="16"><use xlink:href="https://habr.com/images/1565957267/common-svg-sprite.svg#vote-arrow" /></svg></button>
                                                        </div>

                                                    </div>

                                                    <div class="comment__message "><p>Ну так на то она и "микро", что влияние незначительное</p></div>

                                                    <div class="comment__footer">
                                                    </div>

                                                    <div class="comment__reply-form js-form_placeholder"></div>
                                                </div>

                                                <ul class="content-list content-list_nested-comments content-list_nested-comments-4" id="reply_comments_20425783"></ul>
                                            </li>
                                            <li class="content-list__item content-list__item_comment js-comment " rel="20425943">

                                                <span class="parent_id" data-parent_id="20425417"></span>
                                                <div class="comment" id="comment_20425943">
                                                    <span class="comment__folding-dotholder"></span>
                                                    <div class="comment__head   " rel="20425943">
                                                        <a href="https://habr.com/ru/users/borideni/" class="user-info user-info_inline"  data-user-login="borideni">
                                                            <svg class="default-image default-image_mini default-image_blue" width="24" height="24"><use xlink:href="https://habr.com/images/1565957267/common-svg-sprite.svg#slug" /></svg>
                                                            <span class="user-info__nickname user-info__nickname_small user-info__nickname_comment">borideni</span>
                                                        </a>


                                                        <time class="comment__date-time comment__date-time_published">24 июля 2019 в 00:59</time>
                                                        <ul class="inline-list inline-list_comment-nav">
                                                            <li class="inline-list__item inline-list__item_comment-nav">
                                                                <a href="#comment_20425943" class="icon_comment-anchor" title="Ссылка на комментарий"><svg width="12" height="12"><use xlink:href="https://habr.com/images/1565957267/common-svg-sprite.svg#anchor" /></svg></a>
                                                            </li>

                                                            <li class="inline-list__item inline-list__item_comment-nav">
                                                                <a href="#" class="icon_comment-bookmark " onclick="return comments_add_to_favorite(this)" data-type="3" data-id="20425943" data-action="add" title="Только зарегистрированные пользователи могут добавлять комментарии в избранное" disabled><svg width="12" height="12"><use xlink:href="https://habr.com/images/1565957267/common-svg-sprite.svg#book" /></svg></a>
                                                            </li>

                                                            <li class="inline-list__item inline-list__item_comment-nav">
                                                                <span class="icon_comment-branch js-comment_tree" data-id="20425943" data-parent_id="20425417" title="Показать ветку комментариев"><svg width="8" height="12"><use xlink:href="https://habr.com/images/1565957267/common-svg-sprite.svg#tree" /></svg></span>
                                                            </li>

                                                            <li class="inline-list__item inline-list__item_comment-nav">
                                                                <a href="#comment_20425417" class="icon_comment-arrow-up js-comment_parent" data-id="20425943" data-parent_id="20425417" title="Показать предыдущий комментарий"><svg width="12" height="12"><use xlink:href="https://habr.com/images/1565957267/common-svg-sprite.svg#rounded-arrow" /></svg></a>
                                                            </li>

                                                            <li class="inline-list__item inline-list__item_comment-nav hidden js-comment_children">
                                                                <a href="#comment_20425943" class="icon_comment-arrow-down js-comment_parent back_to_children" data-id="20425943" data-parent_id="20425417" title="Вернуться к ответу"><svg width="12" height="12"><use xlink:href="https://habr.com/images/1565957267/common-svg-sprite.svg#rounded-arrow" /></svg></a>
                                                            </li>
                                                        </ul>

                                                        <div class="voting-wjt voting-wjt_comments js-comment-vote" data-id="20425943" data-post-target="460285" data-type="3">
                                                            <button type="button" class="btn voting-wjt__button voting-wjt__button_small " data-action="plus" onclick="comment_vote(this);" title="Голосовать могут только зарегистрированные пользователи" disabled><svg class="icon-svg_arrow-up" width="10" height="16"><use xlink:href="https://habr.com/images/1565957267/common-svg-sprite.svg#vote-arrow" /></svg></button>

                                                            <span class="voting-wjt__counter   js-score" title="Общий рейтинг 0: &uarr;0 и &darr;0">0</span>

                                                            <button type="button" class="btn voting-wjt__button voting-wjt__button_small " data-action="minus" onclick="comment_vote(this);" title="Голосовать могут только зарегистрированные пользователи" disabled><svg class="icon-svg_arrow-down" width="10" height="16"><use xlink:href="https://habr.com/images/1565957267/common-svg-sprite.svg#vote-arrow" /></svg></button>
                                                        </div>

                                                    </div>

                                                    <div class="comment__message "><p>Эта микрооптимизация работает в случае, когда вы находитесь в контексте класса.<br>
                                                            Т.е. просто анонимная функция биндит контекст по-умолчанию, а статическая — не биндит контекст и не пробрасывает внутрь анониманой функции <code>$this</code>. Вот в чем микрооптимизация</p></div>

                                                    <div class="comment__footer">
                                                    </div>

                                                    <div class="comment__reply-form js-form_placeholder"></div>
                                                </div>

                                                <ul class="content-list content-list_nested-comments content-list_nested-comments-4" id="reply_comments_20425943">  <li class="content-list__item content-list__item_comment js-comment " rel="20427941">

                                                        <span class="parent_id" data-parent_id="20425943"></span>
                                                        <div class="comment" id="comment_20427941">
                                                            <span class="comment__folding-dotholder"></span>
                                                            <div class="comment__head   " rel="20427941">
                                                                <a href="https://habr.com/ru/users/VolCh/" class="user-info user-info_inline"  data-user-login="VolCh">
                                                                    <svg class="default-image default-image_mini default-image_lilac" width="24" height="24"><use xlink:href="https://habr.com/images/1565957267/common-svg-sprite.svg#slug" /></svg>
                                                                    <span class="user-info__nickname user-info__nickname_small user-info__nickname_comment">VolCh</span>
                                                                </a>


                                                                <time class="comment__date-time comment__date-time_published">24 июля 2019 в 13:10</time>
                                                                <ul class="inline-list inline-list_comment-nav">
                                                                    <li class="inline-list__item inline-list__item_comment-nav">
                                                                        <a href="#comment_20427941" class="icon_comment-anchor" title="Ссылка на комментарий"><svg width="12" height="12"><use xlink:href="https://habr.com/images/1565957267/common-svg-sprite.svg#anchor" /></svg></a>
                                                                    </li>

                                                                    <li class="inline-list__item inline-list__item_comment-nav">
                                                                        <a href="#" class="icon_comment-bookmark " onclick="return comments_add_to_favorite(this)" data-type="3" data-id="20427941" data-action="add" title="Только зарегистрированные пользователи могут добавлять комментарии в избранное" disabled><svg width="12" height="12"><use xlink:href="https://habr.com/images/1565957267/common-svg-sprite.svg#book" /></svg></a>
                                                                    </li>

                                                                    <li class="inline-list__item inline-list__item_comment-nav">
                                                                        <span class="icon_comment-branch js-comment_tree" data-id="20427941" data-parent_id="20425943" title="Показать ветку комментариев"><svg width="8" height="12"><use xlink:href="https://habr.com/images/1565957267/common-svg-sprite.svg#tree" /></svg></span>
                                                                    </li>

                                                                    <li class="inline-list__item inline-list__item_comment-nav">
                                                                        <a href="#comment_20425943" class="icon_comment-arrow-up js-comment_parent" data-id="20427941" data-parent_id="20425943" title="Показать предыдущий комментарий"><svg width="12" height="12"><use xlink:href="https://habr.com/images/1565957267/common-svg-sprite.svg#rounded-arrow" /></svg></a>
                                                                    </li>

                                                                    <li class="inline-list__item inline-list__item_comment-nav hidden js-comment_children">
                                                                        <a href="#comment_20427941" class="icon_comment-arrow-down js-comment_parent back_to_children" data-id="20427941" data-parent_id="20425943" title="Вернуться к ответу"><svg width="12" height="12"><use xlink:href="https://habr.com/images/1565957267/common-svg-sprite.svg#rounded-arrow" /></svg></a>
                                                                    </li>
                                                                </ul>

                                                                <div class="voting-wjt voting-wjt_comments js-comment-vote" data-id="20427941" data-post-target="460285" data-type="3">
                                                                    <button type="button" class="btn voting-wjt__button voting-wjt__button_small " data-action="plus" onclick="comment_vote(this);" title="Голосовать могут только зарегистрированные пользователи" disabled><svg class="icon-svg_arrow-up" width="10" height="16"><use xlink:href="https://habr.com/images/1565957267/common-svg-sprite.svg#vote-arrow" /></svg></button>

                                                                    <span class="voting-wjt__counter   js-score" title="Общий рейтинг 0: &uarr;0 и &darr;0">0</span>

                                                                    <button type="button" class="btn voting-wjt__button voting-wjt__button_small " data-action="minus" onclick="comment_vote(this);" title="Голосовать могут только зарегистрированные пользователи" disabled><svg class="icon-svg_arrow-down" width="10" height="16"><use xlink:href="https://habr.com/images/1565957267/common-svg-sprite.svg#vote-arrow" /></svg></button>
                                                                </div>

                                                            </div>

                                                            <div class="comment__message "><p>Шторм подсвечивает только в контесте класса :)</p></div>

                                                            <div class="comment__footer">
                                                            </div>

                                                            <div class="comment__reply-form js-form_placeholder"></div>
                                                        </div>

                                                        <ul class="content-list content-list_nested-comments content-list_nested-comments-5" id="reply_comments_20427941"></ul>
                                                    </li>
                                                </ul>
                                            </li>
                                        </ul>
                                    </li>
                                </ul>
                            </li>
                        </ul>
                    </li>
                </ul>
            </li>
            <li class="content-list__item content-list__item_comment js-comment " rel="20423707">

                <span class="parent_id" data-parent_id="0"></span>
                <div class="comment" id="comment_20423707">
                    <span class="comment__folding-dotholder"></span>
                    <div class="comment__head   " rel="20423707">
                        <a href="https://habr.com/ru/users/dzsysop/" class="user-info user-info_inline"  data-user-login="dzsysop">
                            <svg class="default-image default-image_mini default-image_lilac" width="24" height="24"><use xlink:href="https://habr.com/images/1565957267/common-svg-sprite.svg#slug" /></svg>
                            <span class="user-info__nickname user-info__nickname_small user-info__nickname_comment">dzsysop</span>
                        </a>


                        <time class="comment__date-time comment__date-time_published">23 июля 2019 в 15:40</time>
                        <ul class="inline-list inline-list_comment-nav">
                            <li class="inline-list__item inline-list__item_comment-nav">
                                <a href="#comment_20423707" class="icon_comment-anchor" title="Ссылка на комментарий"><svg width="12" height="12"><use xlink:href="https://habr.com/images/1565957267/common-svg-sprite.svg#anchor" /></svg></a>
                            </li>

                            <li class="inline-list__item inline-list__item_comment-nav">
                                <a href="#" class="icon_comment-bookmark " onclick="return comments_add_to_favorite(this)" data-type="3" data-id="20423707" data-action="add" title="Только зарегистрированные пользователи могут добавлять комментарии в избранное" disabled><svg width="12" height="12"><use xlink:href="https://habr.com/images/1565957267/common-svg-sprite.svg#book" /></svg></a>
                            </li>


                            <li class="inline-list__item inline-list__item_comment-nav hidden js-comment_children">
                                <a href="#comment_20423707" class="icon_comment-arrow-down js-comment_parent back_to_children" data-id="20423707" data-parent_id="0" title="Вернуться к ответу"><svg width="12" height="12"><use xlink:href="https://habr.com/images/1565957267/common-svg-sprite.svg#rounded-arrow" /></svg></a>
                            </li>
                        </ul>

                        <div class="voting-wjt voting-wjt_comments js-comment-vote" data-id="20423707" data-post-target="460285" data-type="3">
                            <button type="button" class="btn voting-wjt__button voting-wjt__button_small " data-action="plus" onclick="comment_vote(this);" title="Голосовать могут только зарегистрированные пользователи" disabled><svg class="icon-svg_arrow-up" width="10" height="16"><use xlink:href="https://habr.com/images/1565957267/common-svg-sprite.svg#vote-arrow" /></svg></button>

                            <span class="voting-wjt__counter voting-wjt__counter_positive  js-score" title="Общий рейтинг +4: &uarr;4 и &darr;0">+4</span>

                            <button type="button" class="btn voting-wjt__button voting-wjt__button_small " data-action="minus" onclick="comment_vote(this);" title="Голосовать могут только зарегистрированные пользователи" disabled><svg class="icon-svg_arrow-down" width="10" height="16"><use xlink:href="https://habr.com/images/1565957267/common-svg-sprite.svg#vote-arrow" /></svg></button>
                        </div>

                    </div>

                    <div class="comment__message ">Перевод на английский был бы очень кстати для ссылок при дискуссиях в англоязычной среде. Если кто-то возьмется, был бы очень благодарен. :-)</div>

                    <div class="comment__footer">
                    </div>

                    <div class="comment__reply-form js-form_placeholder"></div>
                </div>

                <ul class="content-list content-list_nested-comments content-list_nested-comments-0" id="reply_comments_20423707">  <li class="content-list__item content-list__item_comment js-comment " rel="20425359">

                        <span class="parent_id" data-parent_id="20423707"></span>
                        <div class="comment" id="comment_20425359">
                            <span class="comment__folding-dotholder"></span>
                            <div class="comment__head comment__head_topic-author  " rel="20425359">
                                <a href="https://habr.com/ru/users/rjhdby/" class="user-info user-info_inline"  data-user-login="rjhdby">
                                    <img src="//habrastorage.org/r/w48/getpro/habr/avatars/330/b5e/fbd/330b5efbd4f629e93730d497b8d865e1.png" class="user-info__image-pic user-info__image-pic_small" width="24" height="24" />
                                    <span class="user-info__nickname user-info__nickname_small user-info__nickname_comment">rjhdby</span>
                                </a>


                                <time class="comment__date-time comment__date-time_published">23 июля 2019 в 21:45</time>
                                <ul class="inline-list inline-list_comment-nav">
                                    <li class="inline-list__item inline-list__item_comment-nav">
                                        <a href="#comment_20425359" class="icon_comment-anchor" title="Ссылка на комментарий"><svg width="12" height="12"><use xlink:href="https://habr.com/images/1565957267/common-svg-sprite.svg#anchor" /></svg></a>
                                    </li>

                                    <li class="inline-list__item inline-list__item_comment-nav">
                                        <a href="#" class="icon_comment-bookmark " onclick="return comments_add_to_favorite(this)" data-type="3" data-id="20425359" data-action="add" title="Только зарегистрированные пользователи могут добавлять комментарии в избранное" disabled><svg width="12" height="12"><use xlink:href="https://habr.com/images/1565957267/common-svg-sprite.svg#book" /></svg></a>
                                    </li>

                                    <li class="inline-list__item inline-list__item_comment-nav">
                                        <span class="icon_comment-branch js-comment_tree" data-id="20425359" data-parent_id="20423707" title="Показать ветку комментариев"><svg width="8" height="12"><use xlink:href="https://habr.com/images/1565957267/common-svg-sprite.svg#tree" /></svg></span>
                                    </li>

                                    <li class="inline-list__item inline-list__item_comment-nav">
                                        <a href="#comment_20423707" class="icon_comment-arrow-up js-comment_parent" data-id="20425359" data-parent_id="20423707" title="Показать предыдущий комментарий"><svg width="12" height="12"><use xlink:href="https://habr.com/images/1565957267/common-svg-sprite.svg#rounded-arrow" /></svg></a>
                                    </li>

                                    <li class="inline-list__item inline-list__item_comment-nav hidden js-comment_children">
                                        <a href="#comment_20425359" class="icon_comment-arrow-down js-comment_parent back_to_children" data-id="20425359" data-parent_id="20423707" title="Вернуться к ответу"><svg width="12" height="12"><use xlink:href="https://habr.com/images/1565957267/common-svg-sprite.svg#rounded-arrow" /></svg></a>
                                    </li>
                                </ul>

                                <div class="voting-wjt voting-wjt_comments js-comment-vote" data-id="20425359" data-post-target="460285" data-type="3">
                                    <button type="button" class="btn voting-wjt__button voting-wjt__button_small " data-action="plus" onclick="comment_vote(this);" title="Голосовать могут только зарегистрированные пользователи" disabled><svg class="icon-svg_arrow-up" width="10" height="16"><use xlink:href="https://habr.com/images/1565957267/common-svg-sprite.svg#vote-arrow" /></svg></button>

                                    <span class="voting-wjt__counter   js-score" title="Общий рейтинг 0: &uarr;0 и &darr;0">0</span>

                                    <button type="button" class="btn voting-wjt__button voting-wjt__button_small " data-action="minus" onclick="comment_vote(this);" title="Голосовать могут только зарегистрированные пользователи" disabled><svg class="icon-svg_arrow-down" width="10" height="16"><use xlink:href="https://habr.com/images/1565957267/common-svg-sprite.svg#vote-arrow" /></svg></button>
                                </div>

                            </div>

                            <div class="comment__message ">Самому, что ли, взяться… Хотя я такого наперевожу…</div>

                            <div class="comment__footer">
                            </div>

                            <div class="comment__reply-form js-form_placeholder"></div>
                        </div>

                        <ul class="content-list content-list_nested-comments content-list_nested-comments-1" id="reply_comments_20425359">  <li class="content-list__item content-list__item_comment js-comment " rel="20453185">

                                <span class="parent_id" data-parent_id="20425359"></span>
                                <div class="comment" id="comment_20453185">
                                    <span class="comment__folding-dotholder"></span>
                                    <div class="comment__head   " rel="20453185">
                                        <a href="https://habr.com/ru/users/Acuna/" class="user-info user-info_inline"  data-user-login="Acuna">
                                            <svg class="default-image default-image_mini default-image_green" width="24" height="24"><use xlink:href="https://habr.com/images/1565957267/common-svg-sprite.svg#slug" /></svg>
                                            <span class="user-info__nickname user-info__nickname_small user-info__nickname_comment">Acuna</span>
                                        </a>


                                        <time class="comment__date-time comment__date-time_published">30 июля 2019 в 16:17</time>
                                        <ul class="inline-list inline-list_comment-nav">
                                            <li class="inline-list__item inline-list__item_comment-nav">
                                                <a href="#comment_20453185" class="icon_comment-anchor" title="Ссылка на комментарий"><svg width="12" height="12"><use xlink:href="https://habr.com/images/1565957267/common-svg-sprite.svg#anchor" /></svg></a>
                                            </li>

                                            <li class="inline-list__item inline-list__item_comment-nav">
                                                <a href="#" class="icon_comment-bookmark " onclick="return comments_add_to_favorite(this)" data-type="3" data-id="20453185" data-action="add" title="Только зарегистрированные пользователи могут добавлять комментарии в избранное" disabled><svg width="12" height="12"><use xlink:href="https://habr.com/images/1565957267/common-svg-sprite.svg#book" /></svg></a>
                                            </li>

                                            <li class="inline-list__item inline-list__item_comment-nav">
                                                <span class="icon_comment-branch js-comment_tree" data-id="20453185" data-parent_id="20425359" title="Показать ветку комментариев"><svg width="8" height="12"><use xlink:href="https://habr.com/images/1565957267/common-svg-sprite.svg#tree" /></svg></span>
                                            </li>

                                            <li class="inline-list__item inline-list__item_comment-nav">
                                                <a href="#comment_20425359" class="icon_comment-arrow-up js-comment_parent" data-id="20453185" data-parent_id="20425359" title="Показать предыдущий комментарий"><svg width="12" height="12"><use xlink:href="https://habr.com/images/1565957267/common-svg-sprite.svg#rounded-arrow" /></svg></a>
                                            </li>

                                            <li class="inline-list__item inline-list__item_comment-nav hidden js-comment_children">
                                                <a href="#comment_20453185" class="icon_comment-arrow-down js-comment_parent back_to_children" data-id="20453185" data-parent_id="20425359" title="Вернуться к ответу"><svg width="12" height="12"><use xlink:href="https://habr.com/images/1565957267/common-svg-sprite.svg#rounded-arrow" /></svg></a>
                                            </li>
                                        </ul>

                                        <div class="voting-wjt voting-wjt_comments js-comment-vote" data-id="20453185" data-post-target="460285" data-type="3">
                                            <button type="button" class="btn voting-wjt__button voting-wjt__button_small " data-action="plus" onclick="comment_vote(this);" title="Голосовать могут только зарегистрированные пользователи" disabled><svg class="icon-svg_arrow-up" width="10" height="16"><use xlink:href="https://habr.com/images/1565957267/common-svg-sprite.svg#vote-arrow" /></svg></button>

                                            <span class="voting-wjt__counter   js-score" title="Общий рейтинг 0: &uarr;0 и &darr;0">0</span>

                                            <button type="button" class="btn voting-wjt__button voting-wjt__button_small " data-action="minus" onclick="comment_vote(this);" title="Голосовать могут только зарегистрированные пользователи" disabled><svg class="icon-svg_arrow-down" width="10" height="16"><use xlink:href="https://habr.com/images/1565957267/common-svg-sprite.svg#vote-arrow" /></svg></button>
                                        </div>

                                    </div>

                                    <div class="comment__message ">Немного оффтоп: неужели может быть такое, что у кого-то не было английского в школе? Или есть еще какая-то причина? Ибо на таком уровне даже моя девушка сможет перевести, если вы понимаете о чем я :/</div>

                                    <div class="comment__footer">
                                    </div>

                                    <div class="comment__reply-form js-form_placeholder"></div>
                                </div>

                                <ul class="content-list content-list_nested-comments content-list_nested-comments-2" id="reply_comments_20453185">  <li class="content-list__item content-list__item_comment js-comment " rel="20453533">

                                        <span class="parent_id" data-parent_id="20453185"></span>
                                        <div class="comment" id="comment_20453533">
                                            <span class="comment__folding-dotholder"></span>
                                            <div class="comment__head   " rel="20453533">
                                                <a href="https://habr.com/ru/users/dzsysop/" class="user-info user-info_inline"  data-user-login="dzsysop">
                                                    <svg class="default-image default-image_mini default-image_lilac" width="24" height="24"><use xlink:href="https://habr.com/images/1565957267/common-svg-sprite.svg#slug" /></svg>
                                                    <span class="user-info__nickname user-info__nickname_small user-info__nickname_comment">dzsysop</span>
                                                </a>


                                                <time class="comment__date-time comment__date-time_published">30 июля 2019 в 17:10</time>
                                                <ul class="inline-list inline-list_comment-nav">
                                                    <li class="inline-list__item inline-list__item_comment-nav">
                                                        <a href="#comment_20453533" class="icon_comment-anchor" title="Ссылка на комментарий"><svg width="12" height="12"><use xlink:href="https://habr.com/images/1565957267/common-svg-sprite.svg#anchor" /></svg></a>
                                                    </li>

                                                    <li class="inline-list__item inline-list__item_comment-nav">
                                                        <a href="#" class="icon_comment-bookmark " onclick="return comments_add_to_favorite(this)" data-type="3" data-id="20453533" data-action="add" title="Только зарегистрированные пользователи могут добавлять комментарии в избранное" disabled><svg width="12" height="12"><use xlink:href="https://habr.com/images/1565957267/common-svg-sprite.svg#book" /></svg></a>
                                                    </li>

                                                    <li class="inline-list__item inline-list__item_comment-nav">
                                                        <span class="icon_comment-branch js-comment_tree" data-id="20453533" data-parent_id="20453185" title="Показать ветку комментариев"><svg width="8" height="12"><use xlink:href="https://habr.com/images/1565957267/common-svg-sprite.svg#tree" /></svg></span>
                                                    </li>

                                                    <li class="inline-list__item inline-list__item_comment-nav">
                                                        <a href="#comment_20453185" class="icon_comment-arrow-up js-comment_parent" data-id="20453533" data-parent_id="20453185" title="Показать предыдущий комментарий"><svg width="12" height="12"><use xlink:href="https://habr.com/images/1565957267/common-svg-sprite.svg#rounded-arrow" /></svg></a>
                                                    </li>

                                                    <li class="inline-list__item inline-list__item_comment-nav hidden js-comment_children">
                                                        <a href="#comment_20453533" class="icon_comment-arrow-down js-comment_parent back_to_children" data-id="20453533" data-parent_id="20453185" title="Вернуться к ответу"><svg width="12" height="12"><use xlink:href="https://habr.com/images/1565957267/common-svg-sprite.svg#rounded-arrow" /></svg></a>
                                                    </li>
                                                </ul>

                                                <div class="voting-wjt voting-wjt_comments js-comment-vote" data-id="20453533" data-post-target="460285" data-type="3">
                                                    <button type="button" class="btn voting-wjt__button voting-wjt__button_small " data-action="plus" onclick="comment_vote(this);" title="Голосовать могут только зарегистрированные пользователи" disabled><svg class="icon-svg_arrow-up" width="10" height="16"><use xlink:href="https://habr.com/images/1565957267/common-svg-sprite.svg#vote-arrow" /></svg></button>

                                                    <span class="voting-wjt__counter   js-score" title="Общий рейтинг 0: &uarr;0 и &darr;0">0</span>

                                                    <button type="button" class="btn voting-wjt__button voting-wjt__button_small " data-action="minus" onclick="comment_vote(this);" title="Голосовать могут только зарегистрированные пользователи" disabled><svg class="icon-svg_arrow-down" width="10" height="16"><use xlink:href="https://habr.com/images/1565957267/common-svg-sprite.svg#vote-arrow" /></svg></button>
                                                </div>

                                            </div>

                                            <div class="comment__message ">Нет не понимаю. Я работаю в англофонной среде последние 8 лет. По работе общаюсь 100% по-английски. Но взяться за перевод этой статьи не считаю возможным. Если ваша девушка обладает необходимой квалификацией, то пусть попробует. Но я подозреваю что заминусуют ее тут именно за качество перевода слету. <br>
                                                Хотя возможно это я такой туповатый, а вокруг все люди поспособнее и поскромнее ходят и живут.</div>

                                            <div class="comment__footer">
                                            </div>

                                            <div class="comment__reply-form js-form_placeholder"></div>
                                        </div>

                                        <ul class="content-list content-list_nested-comments content-list_nested-comments-3" id="reply_comments_20453533">  <li class="content-list__item content-list__item_comment js-comment " rel="20453577">

                                                <span class="parent_id" data-parent_id="20453533"></span>
                                                <div class="comment" id="comment_20453577">
                                                    <span class="comment__folding-dotholder"></span>
                                                    <div class="comment__head   " rel="20453577">
                                                        <a href="https://habr.com/ru/users/Acuna/" class="user-info user-info_inline"  data-user-login="Acuna">
                                                            <svg class="default-image default-image_mini default-image_green" width="24" height="24"><use xlink:href="https://habr.com/images/1565957267/common-svg-sprite.svg#slug" /></svg>
                                                            <span class="user-info__nickname user-info__nickname_small user-info__nickname_comment">Acuna</span>
                                                        </a>


                                                        <time class="comment__date-time comment__date-time_published">30 июля 2019 в 17:18</time>
                                                        <ul class="inline-list inline-list_comment-nav">
                                                            <li class="inline-list__item inline-list__item_comment-nav">
                                                                <a href="#comment_20453577" class="icon_comment-anchor" title="Ссылка на комментарий"><svg width="12" height="12"><use xlink:href="https://habr.com/images/1565957267/common-svg-sprite.svg#anchor" /></svg></a>
                                                            </li>

                                                            <li class="inline-list__item inline-list__item_comment-nav">
                                                                <a href="#" class="icon_comment-bookmark " onclick="return comments_add_to_favorite(this)" data-type="3" data-id="20453577" data-action="add" title="Только зарегистрированные пользователи могут добавлять комментарии в избранное" disabled><svg width="12" height="12"><use xlink:href="https://habr.com/images/1565957267/common-svg-sprite.svg#book" /></svg></a>
                                                            </li>

                                                            <li class="inline-list__item inline-list__item_comment-nav">
                                                                <span class="icon_comment-branch js-comment_tree" data-id="20453577" data-parent_id="20453533" title="Показать ветку комментариев"><svg width="8" height="12"><use xlink:href="https://habr.com/images/1565957267/common-svg-sprite.svg#tree" /></svg></span>
                                                            </li>

                                                            <li class="inline-list__item inline-list__item_comment-nav">
                                                                <a href="#comment_20453533" class="icon_comment-arrow-up js-comment_parent" data-id="20453577" data-parent_id="20453533" title="Показать предыдущий комментарий"><svg width="12" height="12"><use xlink:href="https://habr.com/images/1565957267/common-svg-sprite.svg#rounded-arrow" /></svg></a>
                                                            </li>

                                                            <li class="inline-list__item inline-list__item_comment-nav hidden js-comment_children">
                                                                <a href="#comment_20453577" class="icon_comment-arrow-down js-comment_parent back_to_children" data-id="20453577" data-parent_id="20453533" title="Вернуться к ответу"><svg width="12" height="12"><use xlink:href="https://habr.com/images/1565957267/common-svg-sprite.svg#rounded-arrow" /></svg></a>
                                                            </li>
                                                        </ul>

                                                        <div class="voting-wjt voting-wjt_comments js-comment-vote" data-id="20453577" data-post-target="460285" data-type="3">
                                                            <button type="button" class="btn voting-wjt__button voting-wjt__button_small " data-action="plus" onclick="comment_vote(this);" title="Голосовать могут только зарегистрированные пользователи" disabled><svg class="icon-svg_arrow-up" width="10" height="16"><use xlink:href="https://habr.com/images/1565957267/common-svg-sprite.svg#vote-arrow" /></svg></button>

                                                            <span class="voting-wjt__counter   js-score" title="Общий рейтинг 0: &uarr;0 и &darr;0">0</span>

                                                            <button type="button" class="btn voting-wjt__button voting-wjt__button_small " data-action="minus" onclick="comment_vote(this);" title="Голосовать могут только зарегистрированные пользователи" disabled><svg class="icon-svg_arrow-down" width="10" height="16"><use xlink:href="https://habr.com/images/1565957267/common-svg-sprite.svg#vote-arrow" /></svg></button>
                                                        </div>

                                                    </div>

                                                    <div class="comment__message ">Просто мне показалось что это типичный технический английский «на уровне чтения документации», который ожидает 99% рекрутеров… А может и показалось…</div>

                                                    <div class="comment__footer">
                                                    </div>

                                                    <div class="comment__reply-form js-form_placeholder"></div>
                                                </div>

                                                <ul class="content-list content-list_nested-comments content-list_nested-comments-4" id="reply_comments_20453577">  <li class="content-list__item content-list__item_comment js-comment " rel="20453649">

                                                        <span class="parent_id" data-parent_id="20453577"></span>
                                                        <div class="comment" id="comment_20453649">
                                                            <span class="comment__folding-dotholder"></span>
                                                            <div class="comment__head   " rel="20453649">
                                                                <a href="https://habr.com/ru/users/VolCh/" class="user-info user-info_inline"  data-user-login="VolCh">
                                                                    <svg class="default-image default-image_mini default-image_lilac" width="24" height="24"><use xlink:href="https://habr.com/images/1565957267/common-svg-sprite.svg#slug" /></svg>
                                                                    <span class="user-info__nickname user-info__nickname_small user-info__nickname_comment">VolCh</span>
                                                                </a>


                                                                <time class="comment__date-time comment__date-time_published">30 июля 2019 в 17:35</time>
                                                                <ul class="inline-list inline-list_comment-nav">
                                                                    <li class="inline-list__item inline-list__item_comment-nav">
                                                                        <a href="#comment_20453649" class="icon_comment-anchor" title="Ссылка на комментарий"><svg width="12" height="12"><use xlink:href="https://habr.com/images/1565957267/common-svg-sprite.svg#anchor" /></svg></a>
                                                                    </li>

                                                                    <li class="inline-list__item inline-list__item_comment-nav">
                                                                        <a href="#" class="icon_comment-bookmark " onclick="return comments_add_to_favorite(this)" data-type="3" data-id="20453649" data-action="add" title="Только зарегистрированные пользователи могут добавлять комментарии в избранное" disabled><svg width="12" height="12"><use xlink:href="https://habr.com/images/1565957267/common-svg-sprite.svg#book" /></svg></a>
                                                                    </li>

                                                                    <li class="inline-list__item inline-list__item_comment-nav">
                                                                        <span class="icon_comment-branch js-comment_tree" data-id="20453649" data-parent_id="20453577" title="Показать ветку комментариев"><svg width="8" height="12"><use xlink:href="https://habr.com/images/1565957267/common-svg-sprite.svg#tree" /></svg></span>
                                                                    </li>

                                                                    <li class="inline-list__item inline-list__item_comment-nav">
                                                                        <a href="#comment_20453577" class="icon_comment-arrow-up js-comment_parent" data-id="20453649" data-parent_id="20453577" title="Показать предыдущий комментарий"><svg width="12" height="12"><use xlink:href="https://habr.com/images/1565957267/common-svg-sprite.svg#rounded-arrow" /></svg></a>
                                                                    </li>

                                                                    <li class="inline-list__item inline-list__item_comment-nav hidden js-comment_children">
                                                                        <a href="#comment_20453649" class="icon_comment-arrow-down js-comment_parent back_to_children" data-id="20453649" data-parent_id="20453577" title="Вернуться к ответу"><svg width="12" height="12"><use xlink:href="https://habr.com/images/1565957267/common-svg-sprite.svg#rounded-arrow" /></svg></a>
                                                                    </li>
                                                                </ul>

                                                                <div class="voting-wjt voting-wjt_comments js-comment-vote" data-id="20453649" data-post-target="460285" data-type="3">
                                                                    <button type="button" class="btn voting-wjt__button voting-wjt__button_small " data-action="plus" onclick="comment_vote(this);" title="Голосовать могут только зарегистрированные пользователи" disabled><svg class="icon-svg_arrow-up" width="10" height="16"><use xlink:href="https://habr.com/images/1565957267/common-svg-sprite.svg#vote-arrow" /></svg></button>

                                                                    <span class="voting-wjt__counter   js-score" title="Общий рейтинг 0: &uarr;0 и &darr;0">0</span>

                                                                    <button type="button" class="btn voting-wjt__button voting-wjt__button_small " data-action="minus" onclick="comment_vote(this);" title="Голосовать могут только зарегистрированные пользователи" disabled><svg class="icon-svg_arrow-down" width="10" height="16"><use xlink:href="https://habr.com/images/1565957267/common-svg-sprite.svg#vote-arrow" /></svg></button>
                                                                </div>

                                                            </div>

                                                            <div class="comment__message "><p>Это совершенно другой процесс переводить текст на английский и читать английский текчт</p></div>

                                                            <div class="comment__footer">
                                                            </div>

                                                            <div class="comment__reply-form js-form_placeholder"></div>
                                                        </div>

                                                        <ul class="content-list content-list_nested-comments content-list_nested-comments-5" id="reply_comments_20453649">  <li class="content-list__item content-list__item_comment js-comment " rel="20453667">

                                                                <span class="parent_id" data-parent_id="20453649"></span>
                                                                <div class="comment" id="comment_20453667">
                                                                    <span class="comment__folding-dotholder"></span>
                                                                    <div class="comment__head   " rel="20453667">
                                                                        <a href="https://habr.com/ru/users/Acuna/" class="user-info user-info_inline"  data-user-login="Acuna">
                                                                            <svg class="default-image default-image_mini default-image_green" width="24" height="24"><use xlink:href="https://habr.com/images/1565957267/common-svg-sprite.svg#slug" /></svg>
                                                                            <span class="user-info__nickname user-info__nickname_small user-info__nickname_comment">Acuna</span>
                                                                        </a>


                                                                        <time class="comment__date-time comment__date-time_published">30 июля 2019 в 17:39</time>
                                                                        <ul class="inline-list inline-list_comment-nav">
                                                                            <li class="inline-list__item inline-list__item_comment-nav">
                                                                                <a href="#comment_20453667" class="icon_comment-anchor" title="Ссылка на комментарий"><svg width="12" height="12"><use xlink:href="https://habr.com/images/1565957267/common-svg-sprite.svg#anchor" /></svg></a>
                                                                            </li>

                                                                            <li class="inline-list__item inline-list__item_comment-nav">
                                                                                <a href="#" class="icon_comment-bookmark " onclick="return comments_add_to_favorite(this)" data-type="3" data-id="20453667" data-action="add" title="Только зарегистрированные пользователи могут добавлять комментарии в избранное" disabled><svg width="12" height="12"><use xlink:href="https://habr.com/images/1565957267/common-svg-sprite.svg#book" /></svg></a>
                                                                            </li>

                                                                            <li class="inline-list__item inline-list__item_comment-nav">
                                                                                <span class="icon_comment-branch js-comment_tree" data-id="20453667" data-parent_id="20453649" title="Показать ветку комментариев"><svg width="8" height="12"><use xlink:href="https://habr.com/images/1565957267/common-svg-sprite.svg#tree" /></svg></span>
                                                                            </li>

                                                                            <li class="inline-list__item inline-list__item_comment-nav">
                                                                                <a href="#comment_20453649" class="icon_comment-arrow-up js-comment_parent" data-id="20453667" data-parent_id="20453649" title="Показать предыдущий комментарий"><svg width="12" height="12"><use xlink:href="https://habr.com/images/1565957267/common-svg-sprite.svg#rounded-arrow" /></svg></a>
                                                                            </li>

                                                                            <li class="inline-list__item inline-list__item_comment-nav hidden js-comment_children">
                                                                                <a href="#comment_20453667" class="icon_comment-arrow-down js-comment_parent back_to_children" data-id="20453667" data-parent_id="20453649" title="Вернуться к ответу"><svg width="12" height="12"><use xlink:href="https://habr.com/images/1565957267/common-svg-sprite.svg#rounded-arrow" /></svg></a>
                                                                            </li>
                                                                        </ul>

                                                                        <div class="voting-wjt voting-wjt_comments js-comment-vote" data-id="20453667" data-post-target="460285" data-type="3">
                                                                            <button type="button" class="btn voting-wjt__button voting-wjt__button_small " data-action="plus" onclick="comment_vote(this);" title="Голосовать могут только зарегистрированные пользователи" disabled><svg class="icon-svg_arrow-up" width="10" height="16"><use xlink:href="https://habr.com/images/1565957267/common-svg-sprite.svg#vote-arrow" /></svg></button>

                                                                            <span class="voting-wjt__counter   js-score" title="Общий рейтинг 0: &uarr;0 и &darr;0">0</span>

                                                                            <button type="button" class="btn voting-wjt__button voting-wjt__button_small " data-action="minus" onclick="comment_vote(this);" title="Голосовать могут только зарегистрированные пользователи" disabled><svg class="icon-svg_arrow-down" width="10" height="16"><use xlink:href="https://habr.com/images/1565957267/common-svg-sprite.svg#vote-arrow" /></svg></button>
                                                                        </div>

                                                                    </div>

                                                                    <div class="comment__message ">Просто я всегда считал что любое русское явление в программировании — это калька с английского, и любой разраб должен знать как оно произносится по-английски, банально чтобы загуглить его в буржунете, ибо в русском материала до сих пор катастрофически мало.</div>

                                                                    <div class="comment__footer">
                                                                    </div>

                                                                    <div class="comment__reply-form js-form_placeholder"></div>
                                                                </div>

                                                                <ul class="content-list content-list_nested-comments content-list_nested-comments-6" id="reply_comments_20453667">  <li class="content-list__item content-list__item_comment js-comment " rel="20453891">

                                                                        <span class="parent_id" data-parent_id="20453667"></span>
                                                                        <div class="comment" id="comment_20453891">
                                                                            <span class="comment__folding-dotholder"></span>
                                                                            <div class="comment__head   " rel="20453891">
                                                                                <a href="https://habr.com/ru/users/dzsysop/" class="user-info user-info_inline"  data-user-login="dzsysop">
                                                                                    <svg class="default-image default-image_mini default-image_lilac" width="24" height="24"><use xlink:href="https://habr.com/images/1565957267/common-svg-sprite.svg#slug" /></svg>
                                                                                    <span class="user-info__nickname user-info__nickname_small user-info__nickname_comment">dzsysop</span>
                                                                                </a>


                                                                                <time class="comment__date-time comment__date-time_published">30 июля 2019 в 18:29</time>
                                                                                <ul class="inline-list inline-list_comment-nav">
                                                                                    <li class="inline-list__item inline-list__item_comment-nav">
                                                                                        <a href="#comment_20453891" class="icon_comment-anchor" title="Ссылка на комментарий"><svg width="12" height="12"><use xlink:href="https://habr.com/images/1565957267/common-svg-sprite.svg#anchor" /></svg></a>
                                                                                    </li>

                                                                                    <li class="inline-list__item inline-list__item_comment-nav">
                                                                                        <a href="#" class="icon_comment-bookmark " onclick="return comments_add_to_favorite(this)" data-type="3" data-id="20453891" data-action="add" title="Только зарегистрированные пользователи могут добавлять комментарии в избранное" disabled><svg width="12" height="12"><use xlink:href="https://habr.com/images/1565957267/common-svg-sprite.svg#book" /></svg></a>
                                                                                    </li>

                                                                                    <li class="inline-list__item inline-list__item_comment-nav">
                                                                                        <span class="icon_comment-branch js-comment_tree" data-id="20453891" data-parent_id="20453667" title="Показать ветку комментариев"><svg width="8" height="12"><use xlink:href="https://habr.com/images/1565957267/common-svg-sprite.svg#tree" /></svg></span>
                                                                                    </li>

                                                                                    <li class="inline-list__item inline-list__item_comment-nav">
                                                                                        <a href="#comment_20453667" class="icon_comment-arrow-up js-comment_parent" data-id="20453891" data-parent_id="20453667" title="Показать предыдущий комментарий"><svg width="12" height="12"><use xlink:href="https://habr.com/images/1565957267/common-svg-sprite.svg#rounded-arrow" /></svg></a>
                                                                                    </li>

                                                                                    <li class="inline-list__item inline-list__item_comment-nav hidden js-comment_children">
                                                                                        <a href="#comment_20453891" class="icon_comment-arrow-down js-comment_parent back_to_children" data-id="20453891" data-parent_id="20453667" title="Вернуться к ответу"><svg width="12" height="12"><use xlink:href="https://habr.com/images/1565957267/common-svg-sprite.svg#rounded-arrow" /></svg></a>
                                                                                    </li>
                                                                                </ul>

                                                                                <div class="voting-wjt voting-wjt_comments js-comment-vote" data-id="20453891" data-post-target="460285" data-type="3">
                                                                                    <button type="button" class="btn voting-wjt__button voting-wjt__button_small " data-action="plus" onclick="comment_vote(this);" title="Голосовать могут только зарегистрированные пользователи" disabled><svg class="icon-svg_arrow-up" width="10" height="16"><use xlink:href="https://habr.com/images/1565957267/common-svg-sprite.svg#vote-arrow" /></svg></button>

                                                                                    <span class="voting-wjt__counter   js-score" title="Общий рейтинг 0: &uarr;0 и &darr;0">0</span>

                                                                                    <button type="button" class="btn voting-wjt__button voting-wjt__button_small " data-action="minus" onclick="comment_vote(this);" title="Голосовать могут только зарегистрированные пользователи" disabled><svg class="icon-svg_arrow-down" width="10" height="16"><use xlink:href="https://habr.com/images/1565957267/common-svg-sprite.svg#vote-arrow" /></svg></button>
                                                                                </div>

                                                                            </div>

                                                                            <div class="comment__message ">Не продолжайте. Просто переведите. Мы все будем рады и благодарны.</div>

                                                                            <div class="comment__footer">
                                                                            </div>

                                                                            <div class="comment__reply-form js-form_placeholder"></div>
                                                                        </div>

                                                                        <ul class="content-list content-list_nested-comments content-list_nested-comments-7" id="reply_comments_20453891">  <li class="content-list__item content-list__item_comment js-comment " rel="20453955">

                                                                                <span class="parent_id" data-parent_id="20453891"></span>
                                                                                <div class="comment" id="comment_20453955">
                                                                                    <span class="comment__folding-dotholder"></span>
                                                                                    <div class="comment__head   " rel="20453955">
                                                                                        <a href="https://habr.com/ru/users/Acuna/" class="user-info user-info_inline"  data-user-login="Acuna">
                                                                                            <svg class="default-image default-image_mini default-image_green" width="24" height="24"><use xlink:href="https://habr.com/images/1565957267/common-svg-sprite.svg#slug" /></svg>
                                                                                            <span class="user-info__nickname user-info__nickname_small user-info__nickname_comment">Acuna</span>
                                                                                        </a>


                                                                                        <time class="comment__date-time comment__date-time_published">30 июля 2019 в 18:40</time>
                                                                                        <ul class="inline-list inline-list_comment-nav">
                                                                                            <li class="inline-list__item inline-list__item_comment-nav">
                                                                                                <a href="#comment_20453955" class="icon_comment-anchor" title="Ссылка на комментарий"><svg width="12" height="12"><use xlink:href="https://habr.com/images/1565957267/common-svg-sprite.svg#anchor" /></svg></a>
                                                                                            </li>

                                                                                            <li class="inline-list__item inline-list__item_comment-nav">
                                                                                                <a href="#" class="icon_comment-bookmark " onclick="return comments_add_to_favorite(this)" data-type="3" data-id="20453955" data-action="add" title="Только зарегистрированные пользователи могут добавлять комментарии в избранное" disabled><svg width="12" height="12"><use xlink:href="https://habr.com/images/1565957267/common-svg-sprite.svg#book" /></svg></a>
                                                                                            </li>

                                                                                            <li class="inline-list__item inline-list__item_comment-nav">
                                                                                                <span class="icon_comment-branch js-comment_tree" data-id="20453955" data-parent_id="20453891" title="Показать ветку комментариев"><svg width="8" height="12"><use xlink:href="https://habr.com/images/1565957267/common-svg-sprite.svg#tree" /></svg></span>
                                                                                            </li>

                                                                                            <li class="inline-list__item inline-list__item_comment-nav">
                                                                                                <a href="#comment_20453891" class="icon_comment-arrow-up js-comment_parent" data-id="20453955" data-parent_id="20453891" title="Показать предыдущий комментарий"><svg width="12" height="12"><use xlink:href="https://habr.com/images/1565957267/common-svg-sprite.svg#rounded-arrow" /></svg></a>
                                                                                            </li>

                                                                                            <li class="inline-list__item inline-list__item_comment-nav hidden js-comment_children">
                                                                                                <a href="#comment_20453955" class="icon_comment-arrow-down js-comment_parent back_to_children" data-id="20453955" data-parent_id="20453891" title="Вернуться к ответу"><svg width="12" height="12"><use xlink:href="https://habr.com/images/1565957267/common-svg-sprite.svg#rounded-arrow" /></svg></a>
                                                                                            </li>
                                                                                        </ul>

                                                                                        <div class="voting-wjt voting-wjt_comments js-comment-vote" data-id="20453955" data-post-target="460285" data-type="3">
                                                                                            <button type="button" class="btn voting-wjt__button voting-wjt__button_small " data-action="plus" onclick="comment_vote(this);" title="Голосовать могут только зарегистрированные пользователи" disabled><svg class="icon-svg_arrow-up" width="10" height="16"><use xlink:href="https://habr.com/images/1565957267/common-svg-sprite.svg#vote-arrow" /></svg></button>

                                                                                            <span class="voting-wjt__counter   js-score" title="Общий рейтинг 0: &uarr;0 и &darr;0">0</span>

                                                                                            <button type="button" class="btn voting-wjt__button voting-wjt__button_small " data-action="minus" onclick="comment_vote(this);" title="Голосовать могут только зарегистрированные пользователи" disabled><svg class="icon-svg_arrow-down" width="10" height="16"><use xlink:href="https://habr.com/images/1565957267/common-svg-sprite.svg#vote-arrow" /></svg></button>
                                                                                        </div>

                                                                                    </div>

                                                                                    <div class="comment__message ">А, ладно, подумаю над этим на досуге…</div>

                                                                                    <div class="comment__footer">
                                                                                    </div>

                                                                                    <div class="comment__reply-form js-form_placeholder"></div>
                                                                                </div>

                                                                                <ul class="content-list content-list_nested-comments content-list_nested-comments-8" id="reply_comments_20453955"></ul>
                                                                            </li>
                                                                        </ul>
                                                                    </li>
                                                                    <li class="content-list__item content-list__item_comment js-comment " rel="20454189">

                                                                        <span class="parent_id" data-parent_id="20453667"></span>
                                                                        <div class="comment" id="comment_20454189">
                                                                            <span class="comment__folding-dotholder"></span>
                                                                            <div class="comment__head   " rel="20454189">
                                                                                <a href="https://habr.com/ru/users/VolCh/" class="user-info user-info_inline"  data-user-login="VolCh">
                                                                                    <svg class="default-image default-image_mini default-image_lilac" width="24" height="24"><use xlink:href="https://habr.com/images/1565957267/common-svg-sprite.svg#slug" /></svg>
                                                                                    <span class="user-info__nickname user-info__nickname_small user-info__nickname_comment">VolCh</span>
                                                                                </a>


                                                                                <time class="comment__date-time comment__date-time_published">30 июля 2019 в 19:38</time>
                                                                                <ul class="inline-list inline-list_comment-nav">
                                                                                    <li class="inline-list__item inline-list__item_comment-nav">
                                                                                        <a href="#comment_20454189" class="icon_comment-anchor" title="Ссылка на комментарий"><svg width="12" height="12"><use xlink:href="https://habr.com/images/1565957267/common-svg-sprite.svg#anchor" /></svg></a>
                                                                                    </li>

                                                                                    <li class="inline-list__item inline-list__item_comment-nav">
                                                                                        <a href="#" class="icon_comment-bookmark " onclick="return comments_add_to_favorite(this)" data-type="3" data-id="20454189" data-action="add" title="Только зарегистрированные пользователи могут добавлять комментарии в избранное" disabled><svg width="12" height="12"><use xlink:href="https://habr.com/images/1565957267/common-svg-sprite.svg#book" /></svg></a>
                                                                                    </li>

                                                                                    <li class="inline-list__item inline-list__item_comment-nav">
                                                                                        <span class="icon_comment-branch js-comment_tree" data-id="20454189" data-parent_id="20453667" title="Показать ветку комментариев"><svg width="8" height="12"><use xlink:href="https://habr.com/images/1565957267/common-svg-sprite.svg#tree" /></svg></span>
                                                                                    </li>

                                                                                    <li class="inline-list__item inline-list__item_comment-nav">
                                                                                        <a href="#comment_20453667" class="icon_comment-arrow-up js-comment_parent" data-id="20454189" data-parent_id="20453667" title="Показать предыдущий комментарий"><svg width="12" height="12"><use xlink:href="https://habr.com/images/1565957267/common-svg-sprite.svg#rounded-arrow" /></svg></a>
                                                                                    </li>

                                                                                    <li class="inline-list__item inline-list__item_comment-nav hidden js-comment_children">
                                                                                        <a href="#comment_20454189" class="icon_comment-arrow-down js-comment_parent back_to_children" data-id="20454189" data-parent_id="20453667" title="Вернуться к ответу"><svg width="12" height="12"><use xlink:href="https://habr.com/images/1565957267/common-svg-sprite.svg#rounded-arrow" /></svg></a>
                                                                                    </li>
                                                                                </ul>

                                                                                <div class="voting-wjt voting-wjt_comments js-comment-vote" data-id="20454189" data-post-target="460285" data-type="3">
                                                                                    <button type="button" class="btn voting-wjt__button voting-wjt__button_small " data-action="plus" onclick="comment_vote(this);" title="Голосовать могут только зарегистрированные пользователи" disabled><svg class="icon-svg_arrow-up" width="10" height="16"><use xlink:href="https://habr.com/images/1565957267/common-svg-sprite.svg#vote-arrow" /></svg></button>

                                                                                    <span class="voting-wjt__counter   js-score" title="Общий рейтинг 0: &uarr;0 и &darr;0">0</span>

                                                                                    <button type="button" class="btn voting-wjt__button voting-wjt__button_small " data-action="minus" onclick="comment_vote(this);" title="Голосовать могут только зарегистрированные пользователи" disabled><svg class="icon-svg_arrow-down" width="10" height="16"><use xlink:href="https://habr.com/images/1565957267/common-svg-sprite.svg#vote-arrow" /></svg></button>
                                                                                </div>

                                                                            </div>

                                                                            <div class="comment__message "><p>Термины — может быть. Но грамматику, да и простые слова, не говоря об идиомах. Вот первая же предложение "Дело было вечером, делать было нечего" — можно перевести дословно, конечно, но наверняка что-то лучше есть.</p></div>

                                                                            <div class="comment__footer">
                                                                            </div>

                                                                            <div class="comment__reply-form js-form_placeholder"></div>
                                                                        </div>

                                                                        <ul class="content-list content-list_nested-comments content-list_nested-comments-7" id="reply_comments_20454189">  <li class="content-list__item content-list__item_comment js-comment " rel="20454229">

                                                                                <span class="parent_id" data-parent_id="20454189"></span>
                                                                                <div class="comment" id="comment_20454229">
                                                                                    <span class="comment__folding-dotholder"></span>
                                                                                    <div class="comment__head   " rel="20454229">
                                                                                        <a href="https://habr.com/ru/users/Acuna/" class="user-info user-info_inline"  data-user-login="Acuna">
                                                                                            <svg class="default-image default-image_mini default-image_green" width="24" height="24"><use xlink:href="https://habr.com/images/1565957267/common-svg-sprite.svg#slug" /></svg>
                                                                                            <span class="user-info__nickname user-info__nickname_small user-info__nickname_comment">Acuna</span>
                                                                                        </a>


                                                                                        <time class="comment__date-time comment__date-time_published">30 июля 2019 в 19:51</time>
                                                                                        <ul class="inline-list inline-list_comment-nav">
                                                                                            <li class="inline-list__item inline-list__item_comment-nav">
                                                                                                <a href="#comment_20454229" class="icon_comment-anchor" title="Ссылка на комментарий"><svg width="12" height="12"><use xlink:href="https://habr.com/images/1565957267/common-svg-sprite.svg#anchor" /></svg></a>
                                                                                            </li>

                                                                                            <li class="inline-list__item inline-list__item_comment-nav">
                                                                                                <a href="#" class="icon_comment-bookmark " onclick="return comments_add_to_favorite(this)" data-type="3" data-id="20454229" data-action="add" title="Только зарегистрированные пользователи могут добавлять комментарии в избранное" disabled><svg width="12" height="12"><use xlink:href="https://habr.com/images/1565957267/common-svg-sprite.svg#book" /></svg></a>
                                                                                            </li>

                                                                                            <li class="inline-list__item inline-list__item_comment-nav">
                                                                                                <span class="icon_comment-branch js-comment_tree" data-id="20454229" data-parent_id="20454189" title="Показать ветку комментариев"><svg width="8" height="12"><use xlink:href="https://habr.com/images/1565957267/common-svg-sprite.svg#tree" /></svg></span>
                                                                                            </li>

                                                                                            <li class="inline-list__item inline-list__item_comment-nav">
                                                                                                <a href="#comment_20454189" class="icon_comment-arrow-up js-comment_parent" data-id="20454229" data-parent_id="20454189" title="Показать предыдущий комментарий"><svg width="12" height="12"><use xlink:href="https://habr.com/images/1565957267/common-svg-sprite.svg#rounded-arrow" /></svg></a>
                                                                                            </li>

                                                                                            <li class="inline-list__item inline-list__item_comment-nav hidden js-comment_children">
                                                                                                <a href="#comment_20454229" class="icon_comment-arrow-down js-comment_parent back_to_children" data-id="20454229" data-parent_id="20454189" title="Вернуться к ответу"><svg width="12" height="12"><use xlink:href="https://habr.com/images/1565957267/common-svg-sprite.svg#rounded-arrow" /></svg></a>
                                                                                            </li>
                                                                                        </ul>

                                                                                        <div class="voting-wjt voting-wjt_comments js-comment-vote" data-id="20454229" data-post-target="460285" data-type="3">
                                                                                            <button type="button" class="btn voting-wjt__button voting-wjt__button_small " data-action="plus" onclick="comment_vote(this);" title="Голосовать могут только зарегистрированные пользователи" disabled><svg class="icon-svg_arrow-up" width="10" height="16"><use xlink:href="https://habr.com/images/1565957267/common-svg-sprite.svg#vote-arrow" /></svg></button>

                                                                                            <span class="voting-wjt__counter   js-score" title="Общий рейтинг 0: &uarr;0 и &darr;0">0</span>

                                                                                            <button type="button" class="btn voting-wjt__button voting-wjt__button_small " data-action="minus" onclick="comment_vote(this);" title="Голосовать могут только зарегистрированные пользователи" disabled><svg class="icon-svg_arrow-down" width="10" height="16"><use xlink:href="https://habr.com/images/1565957267/common-svg-sprite.svg#vote-arrow" /></svg></button>
                                                                                        </div>

                                                                                    </div>

                                                                                    <div class="comment__message ">Я тоже об этом думал, когда просматривал ее уже как носитель языка, а не как разраб, не буду скрывать :) Однако если предназначение статьи — скидывать ее в англоязычных дискуссиях, то на мой взгляд использование в ней идиом (особенно русскоязычных, которые в большинстве случаев в английском имеют совершенно другой смысл) несколько бессмысленно, это все-таки не дискуссии филологов…</div>

                                                                                    <div class="comment__footer">
                                                                                    </div>

                                                                                    <div class="comment__reply-form js-form_placeholder"></div>
                                                                                </div>

                                                                                <ul class="content-list content-list_nested-comments content-list_nested-comments-8" id="reply_comments_20454229"></ul>
                                                                            </li>
                                                                        </ul>
                                                                    </li>
                                                                </ul>
                                                            </li>
                                                        </ul>
                                                    </li>
                                                </ul>
                                            </li>
                                        </ul>
                                    </li>
                                </ul>
                            </li>
                        </ul>
                    </li>
                </ul>
            </li>
            <li class="content-list__item content-list__item_comment js-comment " rel="20426047">

                <span class="parent_id" data-parent_id="0"></span>
                <div class="comment" id="comment_20426047">
                    <span class="comment__folding-dotholder"></span>
                    <div class="comment__head   " rel="20426047">
                        <a href="https://habr.com/ru/users/Compolomus/" class="user-info user-info_inline"  data-user-login="Compolomus">
                            <img src="//habrastorage.org/r/w48/getpro/habr/avatars/b9c/a98/b38/b9ca98b38c949a55603382a42526c327.jpg" class="user-info__image-pic user-info__image-pic_small" width="24" height="24" />
                            <span class="user-info__nickname user-info__nickname_small user-info__nickname_comment">Compolomus</span>
                        </a>


                        <time class="comment__date-time comment__date-time_published">24 июля 2019 в 03:22</time>
                        <ul class="inline-list inline-list_comment-nav">
                            <li class="inline-list__item inline-list__item_comment-nav">
                                <a href="#comment_20426047" class="icon_comment-anchor" title="Ссылка на комментарий"><svg width="12" height="12"><use xlink:href="https://habr.com/images/1565957267/common-svg-sprite.svg#anchor" /></svg></a>
                            </li>

                            <li class="inline-list__item inline-list__item_comment-nav">
                                <a href="#" class="icon_comment-bookmark " onclick="return comments_add_to_favorite(this)" data-type="3" data-id="20426047" data-action="add" title="Только зарегистрированные пользователи могут добавлять комментарии в избранное" disabled><svg width="12" height="12"><use xlink:href="https://habr.com/images/1565957267/common-svg-sprite.svg#book" /></svg></a>
                            </li>


                            <li class="inline-list__item inline-list__item_comment-nav hidden js-comment_children">
                                <a href="#comment_20426047" class="icon_comment-arrow-down js-comment_parent back_to_children" data-id="20426047" data-parent_id="0" title="Вернуться к ответу"><svg width="12" height="12"><use xlink:href="https://habr.com/images/1565957267/common-svg-sprite.svg#rounded-arrow" /></svg></a>
                            </li>
                        </ul>

                        <div class="voting-wjt voting-wjt_comments js-comment-vote" data-id="20426047" data-post-target="460285" data-type="3">
                            <button type="button" class="btn voting-wjt__button voting-wjt__button_small " data-action="plus" onclick="comment_vote(this);" title="Голосовать могут только зарегистрированные пользователи" disabled><svg class="icon-svg_arrow-up" width="10" height="16"><use xlink:href="https://habr.com/images/1565957267/common-svg-sprite.svg#vote-arrow" /></svg></button>

                            <span class="voting-wjt__counter   js-score" title="Общий рейтинг 0: &uarr;0 и &darr;0">0</span>

                            <button type="button" class="btn voting-wjt__button voting-wjt__button_small " data-action="minus" onclick="comment_vote(this);" title="Голосовать могут только зарегистрированные пользователи" disabled><svg class="icon-svg_arrow-down" width="10" height="16"><use xlink:href="https://habr.com/images/1565957267/common-svg-sprite.svg#vote-arrow" /></svg></button>
                        </div>

                    </div>

                    <div class="comment__message "><p>Вроде как ещё можно array_map потрогать. Вроде там что то крутили</p></div>

                    <div class="comment__footer">
                    </div>

                    <div class="comment__reply-form js-form_placeholder"></div>
                </div>

                <ul class="content-list content-list_nested-comments content-list_nested-comments-0" id="reply_comments_20426047">  <li class="content-list__item content-list__item_comment js-comment " rel="20426671">

                        <span class="parent_id" data-parent_id="20426047"></span>
                        <div class="comment" id="comment_20426671">
                            <span class="comment__folding-dotholder"></span>
                            <div class="comment__head   " rel="20426671">
                                <a href="https://habr.com/ru/users/stagnantice/" class="user-info user-info_inline"  data-user-login="stagnantice">
                                    <img src="//habrastorage.org/r/w48/getpro/habr/avatars/618/f74/924/618f7492410722f5c2ee78143a0440cb.jpg" class="user-info__image-pic user-info__image-pic_small" width="24" height="24" />
                                    <span class="user-info__nickname user-info__nickname_small user-info__nickname_comment">stagnantice</span>
                                </a>


                                <time class="comment__date-time comment__date-time_published">24 июля 2019 в 09:55</time>
                                <ul class="inline-list inline-list_comment-nav">
                                    <li class="inline-list__item inline-list__item_comment-nav">
                                        <a href="#comment_20426671" class="icon_comment-anchor" title="Ссылка на комментарий"><svg width="12" height="12"><use xlink:href="https://habr.com/images/1565957267/common-svg-sprite.svg#anchor" /></svg></a>
                                    </li>

                                    <li class="inline-list__item inline-list__item_comment-nav">
                                        <a href="#" class="icon_comment-bookmark " onclick="return comments_add_to_favorite(this)" data-type="3" data-id="20426671" data-action="add" title="Только зарегистрированные пользователи могут добавлять комментарии в избранное" disabled><svg width="12" height="12"><use xlink:href="https://habr.com/images/1565957267/common-svg-sprite.svg#book" /></svg></a>
                                    </li>

                                    <li class="inline-list__item inline-list__item_comment-nav">
                                        <span class="icon_comment-branch js-comment_tree" data-id="20426671" data-parent_id="20426047" title="Показать ветку комментариев"><svg width="8" height="12"><use xlink:href="https://habr.com/images/1565957267/common-svg-sprite.svg#tree" /></svg></span>
                                    </li>

                                    <li class="inline-list__item inline-list__item_comment-nav">
                                        <a href="#comment_20426047" class="icon_comment-arrow-up js-comment_parent" data-id="20426671" data-parent_id="20426047" title="Показать предыдущий комментарий"><svg width="12" height="12"><use xlink:href="https://habr.com/images/1565957267/common-svg-sprite.svg#rounded-arrow" /></svg></a>
                                    </li>

                                    <li class="inline-list__item inline-list__item_comment-nav hidden js-comment_children">
                                        <a href="#comment_20426671" class="icon_comment-arrow-down js-comment_parent back_to_children" data-id="20426671" data-parent_id="20426047" title="Вернуться к ответу"><svg width="12" height="12"><use xlink:href="https://habr.com/images/1565957267/common-svg-sprite.svg#rounded-arrow" /></svg></a>
                                    </li>
                                </ul>

                                <div class="voting-wjt voting-wjt_comments js-comment-vote" data-id="20426671" data-post-target="460285" data-type="3">
                                    <button type="button" class="btn voting-wjt__button voting-wjt__button_small " data-action="plus" onclick="comment_vote(this);" title="Голосовать могут только зарегистрированные пользователи" disabled><svg class="icon-svg_arrow-up" width="10" height="16"><use xlink:href="https://habr.com/images/1565957267/common-svg-sprite.svg#vote-arrow" /></svg></button>

                                    <span class="voting-wjt__counter   js-score" title="Общий рейтинг 0: &uarr;0 и &darr;0">0</span>

                                    <button type="button" class="btn voting-wjt__button voting-wjt__button_small " data-action="minus" onclick="comment_vote(this);" title="Голосовать могут только зарегистрированные пользователи" disabled><svg class="icon-svg_arrow-down" width="10" height="16"><use xlink:href="https://habr.com/images/1565957267/common-svg-sprite.svg#vote-arrow" /></svg></button>
                                </div>

                            </div>

                            <div class="comment__message ">Особенно если надо вызвать какую-то функцию на каждом элементе массива типа array_map('gettype', $arr), мне кажется будет быстрее чем foreach</div>

                            <div class="comment__footer">
                            </div>

                            <div class="comment__reply-form js-form_placeholder"></div>
                        </div>

                        <ul class="content-list content-list_nested-comments content-list_nested-comments-1" id="reply_comments_20426671">  <li class="content-list__item content-list__item_comment js-comment " rel="20426741">

                                <span class="parent_id" data-parent_id="20426671"></span>
                                <div class="comment" id="comment_20426741">
                                    <span class="comment__folding-dotholder"></span>
                                    <div class="comment__head comment__head_topic-author  " rel="20426741">
                                        <a href="https://habr.com/ru/users/rjhdby/" class="user-info user-info_inline"  data-user-login="rjhdby">
                                            <img src="//habrastorage.org/r/w48/getpro/habr/avatars/330/b5e/fbd/330b5efbd4f629e93730d497b8d865e1.png" class="user-info__image-pic user-info__image-pic_small" width="24" height="24" />
                                            <span class="user-info__nickname user-info__nickname_small user-info__nickname_comment">rjhdby</span>
                                        </a>


                                        <time class="comment__date-time comment__date-time_published">24 июля 2019 в 10:11</time>
                                        <ul class="inline-list inline-list_comment-nav">
                                            <li class="inline-list__item inline-list__item_comment-nav">
                                                <a href="#comment_20426741" class="icon_comment-anchor" title="Ссылка на комментарий"><svg width="12" height="12"><use xlink:href="https://habr.com/images/1565957267/common-svg-sprite.svg#anchor" /></svg></a>
                                            </li>

                                            <li class="inline-list__item inline-list__item_comment-nav">
                                                <a href="#" class="icon_comment-bookmark " onclick="return comments_add_to_favorite(this)" data-type="3" data-id="20426741" data-action="add" title="Только зарегистрированные пользователи могут добавлять комментарии в избранное" disabled><svg width="12" height="12"><use xlink:href="https://habr.com/images/1565957267/common-svg-sprite.svg#book" /></svg></a>
                                            </li>

                                            <li class="inline-list__item inline-list__item_comment-nav">
                                                <span class="icon_comment-branch js-comment_tree" data-id="20426741" data-parent_id="20426671" title="Показать ветку комментариев"><svg width="8" height="12"><use xlink:href="https://habr.com/images/1565957267/common-svg-sprite.svg#tree" /></svg></span>
                                            </li>

                                            <li class="inline-list__item inline-list__item_comment-nav">
                                                <a href="#comment_20426671" class="icon_comment-arrow-up js-comment_parent" data-id="20426741" data-parent_id="20426671" title="Показать предыдущий комментарий"><svg width="12" height="12"><use xlink:href="https://habr.com/images/1565957267/common-svg-sprite.svg#rounded-arrow" /></svg></a>
                                            </li>

                                            <li class="inline-list__item inline-list__item_comment-nav hidden js-comment_children">
                                                <a href="#comment_20426741" class="icon_comment-arrow-down js-comment_parent back_to_children" data-id="20426741" data-parent_id="20426671" title="Вернуться к ответу"><svg width="12" height="12"><use xlink:href="https://habr.com/images/1565957267/common-svg-sprite.svg#rounded-arrow" /></svg></a>
                                            </li>
                                        </ul>

                                        <div class="voting-wjt voting-wjt_comments js-comment-vote" data-id="20426741" data-post-target="460285" data-type="3">
                                            <button type="button" class="btn voting-wjt__button voting-wjt__button_small " data-action="plus" onclick="comment_vote(this);" title="Голосовать могут только зарегистрированные пользователи" disabled><svg class="icon-svg_arrow-up" width="10" height="16"><use xlink:href="https://habr.com/images/1565957267/common-svg-sprite.svg#vote-arrow" /></svg></button>

                                            <span class="voting-wjt__counter   js-score" title="Общий рейтинг 0: &uarr;0 и &darr;0">0</span>

                                            <button type="button" class="btn voting-wjt__button voting-wjt__button_small " data-action="minus" onclick="comment_vote(this);" title="Голосовать могут только зарегистрированные пользователи" disabled><svg class="icon-svg_arrow-down" width="10" height="16"><use xlink:href="https://habr.com/images/1565957267/common-svg-sprite.svg#vote-arrow" /></svg></button>
                                        </div>

                                    </div>

                                    <div class="comment__message ">По принципу работы array_map ничем не отличается от array_walk. Нет, быстрее не будет.</div>

                                    <div class="comment__footer">
                                    </div>

                                    <div class="comment__reply-form js-form_placeholder"></div>
                                </div>

                                <ul class="content-list content-list_nested-comments content-list_nested-comments-2" id="reply_comments_20426741">  <li class="content-list__item content-list__item_comment js-comment " rel="20427115">

                                        <span class="parent_id" data-parent_id="20426741"></span>
                                        <div class="comment" id="comment_20427115">
                                            <span class="comment__folding-dotholder"></span>
                                            <div class="comment__head   " rel="20427115">
                                                <a href="https://habr.com/ru/users/Compolomus/" class="user-info user-info_inline"  data-user-login="Compolomus">
                                                    <img src="//habrastorage.org/r/w48/getpro/habr/avatars/b9c/a98/b38/b9ca98b38c949a55603382a42526c327.jpg" class="user-info__image-pic user-info__image-pic_small" width="24" height="24" />
                                                    <span class="user-info__nickname user-info__nickname_small user-info__nickname_comment">Compolomus</span>
                                                </a>


                                                <time class="comment__date-time comment__date-time_published">24 июля 2019 в 11:10</time>
                                                <ul class="inline-list inline-list_comment-nav">
                                                    <li class="inline-list__item inline-list__item_comment-nav">
                                                        <a href="#comment_20427115" class="icon_comment-anchor" title="Ссылка на комментарий"><svg width="12" height="12"><use xlink:href="https://habr.com/images/1565957267/common-svg-sprite.svg#anchor" /></svg></a>
                                                    </li>

                                                    <li class="inline-list__item inline-list__item_comment-nav">
                                                        <a href="#" class="icon_comment-bookmark " onclick="return comments_add_to_favorite(this)" data-type="3" data-id="20427115" data-action="add" title="Только зарегистрированные пользователи могут добавлять комментарии в избранное" disabled><svg width="12" height="12"><use xlink:href="https://habr.com/images/1565957267/common-svg-sprite.svg#book" /></svg></a>
                                                    </li>

                                                    <li class="inline-list__item inline-list__item_comment-nav">
                                                        <span class="icon_comment-branch js-comment_tree" data-id="20427115" data-parent_id="20426741" title="Показать ветку комментариев"><svg width="8" height="12"><use xlink:href="https://habr.com/images/1565957267/common-svg-sprite.svg#tree" /></svg></span>
                                                    </li>

                                                    <li class="inline-list__item inline-list__item_comment-nav">
                                                        <a href="#comment_20426741" class="icon_comment-arrow-up js-comment_parent" data-id="20427115" data-parent_id="20426741" title="Показать предыдущий комментарий"><svg width="12" height="12"><use xlink:href="https://habr.com/images/1565957267/common-svg-sprite.svg#rounded-arrow" /></svg></a>
                                                    </li>

                                                    <li class="inline-list__item inline-list__item_comment-nav hidden js-comment_children">
                                                        <a href="#comment_20427115" class="icon_comment-arrow-down js-comment_parent back_to_children" data-id="20427115" data-parent_id="20426741" title="Вернуться к ответу"><svg width="12" height="12"><use xlink:href="https://habr.com/images/1565957267/common-svg-sprite.svg#rounded-arrow" /></svg></a>
                                                    </li>
                                                </ul>

                                                <div class="voting-wjt voting-wjt_comments js-comment-vote" data-id="20427115" data-post-target="460285" data-type="3">
                                                    <button type="button" class="btn voting-wjt__button voting-wjt__button_small " data-action="plus" onclick="comment_vote(this);" title="Голосовать могут только зарегистрированные пользователи" disabled><svg class="icon-svg_arrow-up" width="10" height="16"><use xlink:href="https://habr.com/images/1565957267/common-svg-sprite.svg#vote-arrow" /></svg></button>

                                                    <span class="voting-wjt__counter   js-score" title="Общий рейтинг 0: &uarr;0 и &darr;0">0</span>

                                                    <button type="button" class="btn voting-wjt__button voting-wjt__button_small " data-action="minus" onclick="comment_vote(this);" title="Голосовать могут только зарегистрированные пользователи" disabled><svg class="icon-svg_arrow-down" width="10" height="16"><use xlink:href="https://habr.com/images/1565957267/common-svg-sprite.svg#vote-arrow" /></svg></button>
                                                </div>

                                            </div>

                                            <div class="comment__message "><p>Принцип то один, гляньте внутри, я сам в коде использую map вместо foreach, а вот подробностей не помню, почему так</p></div>

                                            <div class="comment__footer">
                                            </div>

                                            <div class="comment__reply-form js-form_placeholder"></div>
                                        </div>

                                        <ul class="content-list content-list_nested-comments content-list_nested-comments-3" id="reply_comments_20427115">  <li class="content-list__item content-list__item_comment js-comment " rel="20427441">

                                                <span class="parent_id" data-parent_id="20427115"></span>
                                                <div class="comment" id="comment_20427441">
                                                    <span class="comment__folding-dotholder"></span>
                                                    <div class="comment__head comment__head_topic-author  " rel="20427441">
                                                        <a href="https://habr.com/ru/users/rjhdby/" class="user-info user-info_inline"  data-user-login="rjhdby">
                                                            <img src="//habrastorage.org/r/w48/getpro/habr/avatars/330/b5e/fbd/330b5efbd4f629e93730d497b8d865e1.png" class="user-info__image-pic user-info__image-pic_small" width="24" height="24" />
                                                            <span class="user-info__nickname user-info__nickname_small user-info__nickname_comment">rjhdby</span>
                                                        </a>


                                                        <time class="comment__date-time comment__date-time_published">24 июля 2019 в 11:58</time>
                                                        <ul class="inline-list inline-list_comment-nav">
                                                            <li class="inline-list__item inline-list__item_comment-nav">
                                                                <a href="#comment_20427441" class="icon_comment-anchor" title="Ссылка на комментарий"><svg width="12" height="12"><use xlink:href="https://habr.com/images/1565957267/common-svg-sprite.svg#anchor" /></svg></a>
                                                            </li>

                                                            <li class="inline-list__item inline-list__item_comment-nav">
                                                                <a href="#" class="icon_comment-bookmark " onclick="return comments_add_to_favorite(this)" data-type="3" data-id="20427441" data-action="add" title="Только зарегистрированные пользователи могут добавлять комментарии в избранное" disabled><svg width="12" height="12"><use xlink:href="https://habr.com/images/1565957267/common-svg-sprite.svg#book" /></svg></a>
                                                            </li>

                                                            <li class="inline-list__item inline-list__item_comment-nav">
                                                                <span class="icon_comment-branch js-comment_tree" data-id="20427441" data-parent_id="20427115" title="Показать ветку комментариев"><svg width="8" height="12"><use xlink:href="https://habr.com/images/1565957267/common-svg-sprite.svg#tree" /></svg></span>
                                                            </li>

                                                            <li class="inline-list__item inline-list__item_comment-nav">
                                                                <a href="#comment_20427115" class="icon_comment-arrow-up js-comment_parent" data-id="20427441" data-parent_id="20427115" title="Показать предыдущий комментарий"><svg width="12" height="12"><use xlink:href="https://habr.com/images/1565957267/common-svg-sprite.svg#rounded-arrow" /></svg></a>
                                                            </li>

                                                            <li class="inline-list__item inline-list__item_comment-nav hidden js-comment_children">
                                                                <a href="#comment_20427441" class="icon_comment-arrow-down js-comment_parent back_to_children" data-id="20427441" data-parent_id="20427115" title="Вернуться к ответу"><svg width="12" height="12"><use xlink:href="https://habr.com/images/1565957267/common-svg-sprite.svg#rounded-arrow" /></svg></a>
                                                            </li>
                                                        </ul>

                                                        <div class="voting-wjt voting-wjt_comments js-comment-vote" data-id="20427441" data-post-target="460285" data-type="3">
                                                            <button type="button" class="btn voting-wjt__button voting-wjt__button_small " data-action="plus" onclick="comment_vote(this);" title="Голосовать могут только зарегистрированные пользователи" disabled><svg class="icon-svg_arrow-up" width="10" height="16"><use xlink:href="https://habr.com/images/1565957267/common-svg-sprite.svg#vote-arrow" /></svg></button>

                                                            <span class="voting-wjt__counter   js-score" title="Общий рейтинг 0: &uarr;0 и &darr;0">0</span>

                                                            <button type="button" class="btn voting-wjt__button voting-wjt__button_small " data-action="minus" onclick="comment_vote(this);" title="Голосовать могут только зарегистрированные пользователи" disabled><svg class="icon-svg_arrow-down" width="10" height="16"><use xlink:href="https://habr.com/images/1565957267/common-svg-sprite.svg#vote-arrow" /></svg></button>
                                                        </div>

                                                    </div>

                                                    <div class="comment__message "><p>Так я внутри и смотрю. Тут не в способе перебора дело, а в вызове пользовательской функции, при которой управление передается виртуальной машине.<br>
                                                            И там и там <code>zend_call_function(&amp;fci, &amp;fci_cache)</code> дергается.</p></div>

                                                    <div class="comment__footer">
                                                    </div>

                                                    <div class="comment__reply-form js-form_placeholder"></div>
                                                </div>

                                                <ul class="content-list content-list_nested-comments content-list_nested-comments-4" id="reply_comments_20427441"></ul>
                                            </li>
                                        </ul>
                                    </li>
                                </ul>
                            </li>
                            <li class="content-list__item content-list__item_comment js-comment " rel="20448861">

                                <span class="parent_id" data-parent_id="20426671"></span>
                                <div class="comment" id="comment_20448861">
                                    <span class="comment__folding-dotholder"></span>
                                    <div class="comment__head   " rel="20448861">
                                        <a href="https://habr.com/ru/users/romanitalian_net/" class="user-info user-info_inline"  data-user-login="romanitalian_net">
                                            <img src="//habrastorage.org/r/w48/getpro/habr/avatars/730/6a0/885/7306a0885373a416ecc4aef87663d887.png" class="user-info__image-pic user-info__image-pic_small" width="24" height="24" />
                                            <span class="user-info__nickname user-info__nickname_small user-info__nickname_comment">romanitalian_net</span>
                                        </a>


                                        <time class="comment__date-time comment__date-time_published">29 июля 2019 в 18:30</time>
                                        <ul class="inline-list inline-list_comment-nav">
                                            <li class="inline-list__item inline-list__item_comment-nav">
                                                <a href="#comment_20448861" class="icon_comment-anchor" title="Ссылка на комментарий"><svg width="12" height="12"><use xlink:href="https://habr.com/images/1565957267/common-svg-sprite.svg#anchor" /></svg></a>
                                            </li>

                                            <li class="inline-list__item inline-list__item_comment-nav">
                                                <a href="#" class="icon_comment-bookmark " onclick="return comments_add_to_favorite(this)" data-type="3" data-id="20448861" data-action="add" title="Только зарегистрированные пользователи могут добавлять комментарии в избранное" disabled><svg width="12" height="12"><use xlink:href="https://habr.com/images/1565957267/common-svg-sprite.svg#book" /></svg></a>
                                            </li>

                                            <li class="inline-list__item inline-list__item_comment-nav">
                                                <span class="icon_comment-branch js-comment_tree" data-id="20448861" data-parent_id="20426671" title="Показать ветку комментариев"><svg width="8" height="12"><use xlink:href="https://habr.com/images/1565957267/common-svg-sprite.svg#tree" /></svg></span>
                                            </li>

                                            <li class="inline-list__item inline-list__item_comment-nav">
                                                <a href="#comment_20426671" class="icon_comment-arrow-up js-comment_parent" data-id="20448861" data-parent_id="20426671" title="Показать предыдущий комментарий"><svg width="12" height="12"><use xlink:href="https://habr.com/images/1565957267/common-svg-sprite.svg#rounded-arrow" /></svg></a>
                                            </li>

                                            <li class="inline-list__item inline-list__item_comment-nav hidden js-comment_children">
                                                <a href="#comment_20448861" class="icon_comment-arrow-down js-comment_parent back_to_children" data-id="20448861" data-parent_id="20426671" title="Вернуться к ответу"><svg width="12" height="12"><use xlink:href="https://habr.com/images/1565957267/common-svg-sprite.svg#rounded-arrow" /></svg></a>
                                            </li>
                                        </ul>

                                        <div class="voting-wjt voting-wjt_comments js-comment-vote" data-id="20448861" data-post-target="460285" data-type="3">
                                            <button type="button" class="btn voting-wjt__button voting-wjt__button_small " data-action="plus" onclick="comment_vote(this);" title="Голосовать могут только зарегистрированные пользователи" disabled><svg class="icon-svg_arrow-up" width="10" height="16"><use xlink:href="https://habr.com/images/1565957267/common-svg-sprite.svg#vote-arrow" /></svg></button>

                                            <span class="voting-wjt__counter   js-score" title="Общий рейтинг 0: &uarr;0 и &darr;0">0</span>

                                            <button type="button" class="btn voting-wjt__button voting-wjt__button_small " data-action="minus" onclick="comment_vote(this);" title="Голосовать могут только зарегистрированные пользователи" disabled><svg class="icon-svg_arrow-down" width="10" height="16"><use xlink:href="https://habr.com/images/1565957267/common-svg-sprite.svg#vote-arrow" /></svg></button>
                                        </div>

                                    </div>

                                    <div class="comment__message ">Я под капот не залазил. Но 500 лет назад сравнивал «foreach vs array_map» на примере парсинга csv файла: <a href="http://romanitalian.github.io/sections/php/csv_parse_foreach_vs_array_map/">romanitalian.github.io/sections/php/csv_parse_foreach_vs_array_map</a></div>

                                    <div class="comment__footer">
                                    </div>

                                    <div class="comment__reply-form js-form_placeholder"></div>
                                </div>

                                <ul class="content-list content-list_nested-comments content-list_nested-comments-2" id="reply_comments_20448861"></ul>
                            </li>
                        </ul>
                    </li>
                </ul>
            </li>


        </ul>
        <div class="js-form_placeholder">
            <p class="for_users_only_msg">Только&nbsp;<a href="/info/help/registration/">полноправные пользователи</a> могут оставлять комментарии. <a href="https://habr.com/ru/auth/login/">Войдите</a>, пожалуйста.</p>

        </div>
    </div>
</div>