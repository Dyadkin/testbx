</div>
<div class="sidebar_right sidebar_content-area">


        <div class="default-block default-block_sidebar">
            <div class="default-block__header">
                <h2 class="default-block__header-title">Информация</h2>
            </div>
            <div class="default-block__content default-block__content_profile-summary">
                <ul class="defination-list">
                    <li class="defination-list__item defination-list__item_profile-summary">
                        <h3 class="defination-list__label defination-list__label_profile-summary">Дата основания</h3>
                        <span class="defination-list__value">17 декабря 2004 г.</span>
                    </li>

                    <li class="defination-list__item defination-list__item_profile-summary">
                        <h3 class="defination-list__label defination-list__label_profile-summary">Локация</h3>
                        <span class="defination-list__value">
              Лимассол<br/>Кипр
            </span>
                    </li>

                    <li class="defination-list__item defination-list__item_profile-summary">
                        <h3 class="defination-list__label defination-list__label_profile-summary">Сайт</h3>
                        <span class="defination-list__value"><a href="http://fun.co/rp" target="_blank" class="defination-list__link" title="http://fun.co/rp">fun.co</a></span>
                    </li>

                    <li class="defination-list__item defination-list__item_profile-summary">
                        <h3 class="defination-list__label defination-list__label_profile-summary">Численность</h3>
                        <span class="defination-list__value">51&#8211;100 человек</span>
                    </li>

                    <li class="defination-list__item defination-list__item_profile-summary">
                        <h3 class="defination-list__label defination-list__label_profile-summary">Дата регистрации</h3>
                        <span class="defination-list__value">28 августа 2013 г.</span>
                    </li>

                </ul>
            </div>
        </div>



        <div class="default-block default-block_sidebar">
            <div class="default-block__header">
                <h2 class="default-block__header-title">Ссылки</h2>
            </div>
            <div class="default-block__content">
                <ul class="content-list content-list_company-links">
                    <li class="content-list__item content-list__item_company-links company-links">
                        <a href="http://fun.co/rp" class="company-links__title">FunCorp</a>
                        <div class="company-links__link">fun.co</div>
                    </li>
                    <li class="content-list__item content-list__item_company-links company-links">
                        <a href="https://ifunny.co/" class="company-links__title">iFunny</a>
                        <div class="company-links__link">ifunny.co</div>
                    </li>
                    <li class="content-list__item content-list__item_company-links company-links">
                        <a href="http://funcubator.co/" class="company-links__title">FunCubator</a>
                        <div class="company-links__link">funcubator.co</div>
                    </li>
                </ul>
            </div>
        </div>













        <div class="default-block default-block_sidebar">
            <div class="default-block__header">
                <span class="default-block__header-title">Facebook</span>
            </div>
            <div class="default-block__content sidebar-block">
                <div id="facebook_like_box"></div>
                <script  type="text/javascript">
                    (function () {
                        $('#facebook_like_box').html('<iframe src="https://www.facebook.com/plugins/page.php?href=https%3A%2F%2Fwww.facebook.com%2FFunCorpDev%2F&width=260&height=300&small_header=false&adapt_container_width=true&hide_cover=false&show_facepile=true&appId" width="100%" height="220" style="border:none;overflow:hidden" scrolling="no" frameborder="0" allowTransparency="true"></iframe>');
                    }());
                </script>
            </div>
        </div>












        <div class="default-block default-block_sidebar">
            <div class="default-block__header">
                <span class="default-block__header-title">ВКонтакте</span>
            </div>
            <div class="default-block__content sidebar-block">
                <script type="text/javascript">
                    $(document).ready(function () {
                        var el = document.createElement("script");
                        el.type = "text/javascript";
                        el.src = "https://vk.com/js/api/openapi.js?63";
                        el.async = true;
                        el.addEventListener("load", checkVKAndInit);
                        document.getElementById("vk_api_transport").appendChild(el);

                        function checkVKAndInit() {
                            if (window.VK && window.VK.Widgets && window.VK.Widgets.Group) {
                                VK.Widgets.Group("vk_groups", {mode: 0, width: '260', height: '300'}, 118491258);
                            }
                        }
                    });
                </script>
                <div id="vk_api_transport"></div>
                <div id="vk_groups"></div>
            </div>
        </div>











        <div class="default-block default-block_sidebar">
            <div class="default-block__header">
                <h2 class="default-block__header-title">Приложения</h2>
            </div>
            <div class="default-block__content">
                <ul class="content-list content-list_company-wjt" id="company-wjt">
                    <li class="content-list__item content-list__item_company-wjt ">
                        <div class="media-obj company-app">
                            <div class="media-obj__image media-obj__image_company-wjt">
                                <img src="//habrastorage.org/getpro/habr/app/543/dd6/0eb/543dd60eb8aa8918b4853bb2690d617e.png" class="company-app__image" width="48" height="48" />
                            </div>
                            <div class="media-obj__body media-obj__body_company-wjt">
                                <h3 class="company-app__title">iFunny</h3>
                                <div class="company-app__description">Love and iFunny — the only two things you can enjoy without being good at. And if for some reason you are not having much of the first one you can still indulge yourself in the world of fun.</div>
                            </div>
                        </div>
                        <ul class="app-links">
                            <li class="app-links__item">
                                <a href="https://itunes.apple.com/app/id429610587?mt=8&&referrer=click%3D1b1485e9-3b5a-4326-9015-ca499d38d709" class="app-links__item-link">
                                    <span class="app-links__item-name">iOS</span>
                                </a>
                            </li>
                            <li class="app-links__item">
                                <a href="https://play.google.com/store/apps/details?id=mobi.ifunny" class="app-links__item-link">
                                    <span class="app-links__item-name">Android</span>
                                </a>
                            </li>
                        </ul>

                    </li>
                    <li class="content-list__item content-list__item_company-wjt ">
                        <div class="media-obj company-app">
                            <div class="media-obj__image media-obj__image_company-wjt">
                                <img src="//habrastorage.org/getpro/habr/app/5bd/2b6/ba9/5bd2b6ba99d09d9a0f1b5af2a0eba4ef.png" class="company-app__image" width="48" height="48" />
                            </div>
                            <div class="media-obj__body media-obj__body_company-wjt">
                                <h3 class="company-app__title">АйДаПрикол</h3>
                                <div class="company-app__description">АйДаПрикол – бесплатное приложение с коллекцией ежедневно обновляющегося юмора. Мы отбираем для вас лучшие юмористические картинки, GIF-анимации и видео! Отдыхайте от суеты и смейтесь от души!</div>
                            </div>
                        </div>
                        <ul class="app-links">
                            <li class="app-links__item">
                                <a href="https://itunes.apple.com/us/app/id412412148?l=ru&ls=1&mt=8" class="app-links__item-link">
                                    <span class="app-links__item-name">iOS</span>
                                </a>
                            </li>
                            <li class="app-links__item">
                                <a href="https://play.google.com/store/apps/details?id=ru.idaprikol" class="app-links__item-link">
                                    <span class="app-links__item-name">Android</span>
                                </a>
                            </li>
                        </ul>

                    </li>
                </ul>
            </div>
        </div>





        <div class="js-ad_sticky">
            <div class="default-block">
                <div class="default-block__header">
                    <h2 class="default-block__header-title">Блог на Хабре</h2>
                </div>
                <div class="default-block__content">
                    <ul class="content-list">
                        <li class="content-list__item content-list__item_devided post-info">
                            <h3 class="post-info__title">
                                <a href="https://habr.com/ru/company/funcorp/blog/463505/" class="post-info__title">Почему системные администраторы должны становиться DevOps-инженерами</a>
                            </h3>
                            <div class="post-info__meta">
                <span class="post-info__meta-item">
                  <svg class="post-info__meta-icon icon-svg_views" width="16" height="9"><use xlink:href="https://habr.com/images/1565957267/common-svg-sprite.svg#eye" /></svg><span class="post-info__meta-counter post-info__meta-counter_small">28k</span>
                </span>

                                <a href="https://habr.com/ru/company/funcorp/blog/463505/#comments" class="post-info__meta-item" rel="nofollow">
                                    <svg class="post-info__meta-icon icon-svg_comments" width="14" height="13"><use xlink:href="https://habr.com/images/1565957267/common-svg-sprite.svg#comment" /></svg><span class="post-info__meta-counter post-info__meta-counter_small">23</span>
                                </a>
                            </div>
                        </li>
                        <li class="content-list__item content-list__item_devided post-info">
                            <h3 class="post-info__title">
                                <a href="https://habr.com/ru/company/funcorp/blog/462825/" class="post-info__title">Видео докладов с Summer Droid Meetup</a>
                            </h3>
                            <div class="post-info__meta">
                <span class="post-info__meta-item">
                  <svg class="post-info__meta-icon icon-svg_views" width="16" height="9"><use xlink:href="https://habr.com/images/1565957267/common-svg-sprite.svg#eye" /></svg><span class="post-info__meta-counter post-info__meta-counter_small">1,8k</span>
                </span>

                                <a href="https://habr.com/ru/company/funcorp/blog/462825/#comments" class="post-info__meta-item" rel="nofollow">
                                    <svg class="post-info__meta-icon icon-svg_comments" width="14" height="13"><use xlink:href="https://habr.com/images/1565957267/common-svg-sprite.svg#comment" /></svg><span class="post-info__meta-counter post-info__meta-counter_small">1</span>
                                </a>
                            </div>
                        </li>
                        <li class="content-list__item content-list__item_devided post-info">
                            <h3 class="post-info__title">
                                <a href="https://habr.com/ru/company/funcorp/blog/461881/" class="post-info__title">Руководство по логированию в Node.js</a>
                            </h3>
                            <div class="post-info__meta">
                <span class="post-info__meta-item">
                  <svg class="post-info__meta-icon icon-svg_views" width="16" height="9"><use xlink:href="https://habr.com/images/1565957267/common-svg-sprite.svg#eye" /></svg><span class="post-info__meta-counter post-info__meta-counter_small">9k</span>
                </span>

                                <a href="https://habr.com/ru/company/funcorp/blog/461881/#comments" class="post-info__meta-item" rel="nofollow">
                                    <svg class="post-info__meta-icon icon-svg_comments" width="14" height="13"><use xlink:href="https://habr.com/images/1565957267/common-svg-sprite.svg#comment" /></svg><span class="post-info__meta-counter post-info__meta-counter_small">6</span>
                                </a>
                            </div>
                        </li>
                        <li class="content-list__item content-list__item_devided post-info">
                            <h3 class="post-info__title">
                                <a href="https://habr.com/ru/company/funcorp/blog/460285/" class="post-info__title">Препарируем PHP. Как устроены while, foreach, array_walk и некоторые другие страшные слова</a>
                            </h3>
                            <div class="post-info__meta">
                <span class="post-info__meta-item">
                  <svg class="post-info__meta-icon icon-svg_views" width="16" height="9"><use xlink:href="https://habr.com/images/1565957267/common-svg-sprite.svg#eye" /></svg><span class="post-info__meta-counter post-info__meta-counter_small">14,2k</span>
                </span>

                                <a href="https://habr.com/ru/company/funcorp/blog/460285/#comments" class="post-info__meta-item" rel="nofollow">
                                    <svg class="post-info__meta-icon icon-svg_comments" width="14" height="13"><use xlink:href="https://habr.com/images/1565957267/common-svg-sprite.svg#comment" /></svg><span class="post-info__meta-counter post-info__meta-counter_small">25</span>
                                </a>
                            </div>
                        </li>
                        <li class="content-list__item content-list__item_devided post-info">
                            <h3 class="post-info__title">
                                <a href="https://habr.com/ru/company/funcorp/blog/460539/" class="post-info__title">Обработка ошибок во Vue</a>
                            </h3>
                            <div class="post-info__meta">
                <span class="post-info__meta-item">
                  <svg class="post-info__meta-icon icon-svg_views" width="16" height="9"><use xlink:href="https://habr.com/images/1565957267/common-svg-sprite.svg#eye" /></svg><span class="post-info__meta-counter post-info__meta-counter_small">9,5k</span>
                </span>

                                <a href="https://habr.com/ru/company/funcorp/blog/460539/#comments" class="post-info__meta-item" rel="nofollow">
                                    <svg class="post-info__meta-icon icon-svg_comments" width="14" height="13"><use xlink:href="https://habr.com/images/1565957267/common-svg-sprite.svg#comment" /></svg><span class="post-info__meta-counter post-info__meta-counter_small">5</span>
                                </a>
                            </div>
                        </li>
                        <li class="content-list__item content-list__item_devided post-info">
                            <h3 class="post-info__title">
                                <a href="https://habr.com/ru/company/funcorp/blog/459670/" class="post-info__title">GOTO Amsterdam</a>
                            </h3>
                            <div class="post-info__meta">
                <span class="post-info__meta-item">
                  <svg class="post-info__meta-icon icon-svg_views" width="16" height="9"><use xlink:href="https://habr.com/images/1565957267/common-svg-sprite.svg#eye" /></svg><span class="post-info__meta-counter post-info__meta-counter_small">3,8k</span>
                </span>

                                <a href="https://habr.com/ru/company/funcorp/blog/459670/#comments" class="post-info__meta-item" rel="nofollow">
                                    <svg class="post-info__meta-icon icon-svg_comments" width="14" height="13"><use xlink:href="https://habr.com/images/1565957267/common-svg-sprite.svg#comment" /></svg><span class="post-info__meta-counter post-info__meta-counter_small">8</span>
                                </a>
                            </div>
                        </li>
                        <li class="content-list__item content-list__item_devided post-info">
                            <h3 class="post-info__title">
                                <a href="https://habr.com/ru/company/funcorp/blog/459174/" class="post-info__title">Справочник по источникам событий в Rx</a>
                            </h3>
                            <div class="post-info__meta">
                <span class="post-info__meta-item">
                  <svg class="post-info__meta-icon icon-svg_views" width="16" height="9"><use xlink:href="https://habr.com/images/1565957267/common-svg-sprite.svg#eye" /></svg><span class="post-info__meta-counter post-info__meta-counter_small">6,4k</span>
                </span>

                                <a href="https://habr.com/ru/company/funcorp/blog/459174/#comments" class="post-info__meta-item" rel="nofollow">
                                    <svg class="post-info__meta-icon icon-svg_comments" width="14" height="13"><use xlink:href="https://habr.com/images/1565957267/common-svg-sprite.svg#comment" /></svg><span class="post-info__meta-counter post-info__meta-counter_small">4</span>
                                </a>
                            </div>
                        </li>
                        <li class="content-list__item content-list__item_devided post-info">
                            <h3 class="post-info__title">
                                <a href="https://habr.com/ru/company/funcorp/blog/458888/" class="post-info__title">Summer Droid Meetup</a>
                            </h3>
                            <div class="post-info__meta">
                <span class="post-info__meta-item">
                  <svg class="post-info__meta-icon icon-svg_views" width="16" height="9"><use xlink:href="https://habr.com/images/1565957267/common-svg-sprite.svg#eye" /></svg><span class="post-info__meta-counter post-info__meta-counter_small">1,7k</span>
                </span>

                                <a href="https://habr.com/ru/company/funcorp/blog/458888/#comments" class="post-info__meta-item" rel="nofollow">
                                    <svg class="post-info__meta-icon icon-svg_comments" width="14" height="13"><use xlink:href="https://habr.com/images/1565957267/common-svg-sprite.svg#comment" /></svg><span class="post-info__meta-counter post-info__meta-counter_small">0</span>
                                </a>
                            </div>
                        </li>
                        <li class="content-list__item content-list__item_devided post-info">
                            <h3 class="post-info__title">
                                <a href="https://habr.com/ru/company/funcorp/blog/456024/" class="post-info__title">PHP в 2019: лучше, чем вы о нём думаете</a>
                            </h3>
                            <div class="post-info__meta">
                <span class="post-info__meta-item">
                  <svg class="post-info__meta-icon icon-svg_views" width="16" height="9"><use xlink:href="https://habr.com/images/1565957267/common-svg-sprite.svg#eye" /></svg><span class="post-info__meta-counter post-info__meta-counter_small">21,7k</span>
                </span>

                                <a href="https://habr.com/ru/company/funcorp/blog/456024/#comments" class="post-info__meta-item" rel="nofollow">
                                    <svg class="post-info__meta-icon icon-svg_comments" width="14" height="13"><use xlink:href="https://habr.com/images/1565957267/common-svg-sprite.svg#comment" /></svg><span class="post-info__meta-counter post-info__meta-counter_small">97</span>
                                </a>
                            </div>
                        </li>
                        <li class="content-list__item content-list__item_devided post-info">
                            <h3 class="post-info__title">
                                <a href="https://habr.com/ru/company/funcorp/blog/455938/" class="post-info__title">Стоит ли высокое качество ПО затрат на его разработку?</a>
                            </h3>
                            <div class="post-info__meta">
                <span class="post-info__meta-item">
                  <svg class="post-info__meta-icon icon-svg_views" width="16" height="9"><use xlink:href="https://habr.com/images/1565957267/common-svg-sprite.svg#eye" /></svg><span class="post-info__meta-counter post-info__meta-counter_small">26k</span>
                </span>

                                <a href="https://habr.com/ru/company/funcorp/blog/455938/#comments" class="post-info__meta-item" rel="nofollow">
                                    <svg class="post-info__meta-icon icon-svg_comments" width="14" height="13"><use xlink:href="https://habr.com/images/1565957267/common-svg-sprite.svg#comment" /></svg><span class="post-info__meta-counter post-info__meta-counter_small">100</span>
                                </a>
                            </div>
                        </li>
                        <li class="content-list__item content-list__item_devided post-info">
                            <h3 class="post-info__title">
                                <a href="https://habr.com/ru/company/funcorp/blog/454410/" class="post-info__title">Новое в PHP 7.4</a>
                            </h3>
                            <div class="post-info__meta">
                <span class="post-info__meta-item">
                  <svg class="post-info__meta-icon icon-svg_views" width="16" height="9"><use xlink:href="https://habr.com/images/1565957267/common-svg-sprite.svg#eye" /></svg><span class="post-info__meta-counter post-info__meta-counter_small">25k</span>
                </span>

                                <a href="https://habr.com/ru/company/funcorp/blog/454410/#comments" class="post-info__meta-item" rel="nofollow">
                                    <svg class="post-info__meta-icon icon-svg_comments" width="14" height="13"><use xlink:href="https://habr.com/images/1565957267/common-svg-sprite.svg#comment" /></svg><span class="post-info__meta-counter post-info__meta-counter_small">90</span>
                                </a>
                            </div>
                        </li>
                        <li class="content-list__item content-list__item_devided post-info">
                            <h3 class="post-info__title">
                                <a href="https://habr.com/ru/company/funcorp/blog/454044/" class="post-info__title">Творчество на iPad и iPhone</a>
                            </h3>
                            <div class="post-info__meta">
                <span class="post-info__meta-item">
                  <svg class="post-info__meta-icon icon-svg_views" width="16" height="9"><use xlink:href="https://habr.com/images/1565957267/common-svg-sprite.svg#eye" /></svg><span class="post-info__meta-counter post-info__meta-counter_small">5,5k</span>
                </span>

                                <a href="https://habr.com/ru/company/funcorp/blog/454044/#comments" class="post-info__meta-item" rel="nofollow">
                                    <svg class="post-info__meta-icon icon-svg_comments" width="14" height="13"><use xlink:href="https://habr.com/images/1565957267/common-svg-sprite.svg#comment" /></svg><span class="post-info__meta-counter post-info__meta-counter_small">16</span>
                                </a>
                            </div>
                        </li>
                        <li class="content-list__item content-list__item_devided post-info">
                            <h3 class="post-info__title">
                                <a href="https://habr.com/ru/company/funcorp/blog/453318/" class="post-info__title">5 ошибок в реализации push-уведомлений для мобильных приложений</a>
                            </h3>
                            <div class="post-info__meta">
                <span class="post-info__meta-item">
                  <svg class="post-info__meta-icon icon-svg_views" width="16" height="9"><use xlink:href="https://habr.com/images/1565957267/common-svg-sprite.svg#eye" /></svg><span class="post-info__meta-counter post-info__meta-counter_small">8,5k</span>
                </span>

                                <a href="https://habr.com/ru/company/funcorp/blog/453318/#comments" class="post-info__meta-item" rel="nofollow">
                                    <svg class="post-info__meta-icon icon-svg_comments" width="14" height="13"><use xlink:href="https://habr.com/images/1565957267/common-svg-sprite.svg#comment" /></svg><span class="post-info__meta-counter post-info__meta-counter_small">14</span>
                                </a>
                            </div>
                        </li>
                        <li class="content-list__item content-list__item_devided post-info">
                            <h3 class="post-info__title">
                                <a href="https://habr.com/ru/company/funcorp/blog/450120/" class="post-info__title">Поиск похожих изображений, разбор одного алгоритма</a>
                            </h3>
                            <div class="post-info__meta">
                <span class="post-info__meta-item">
                  <svg class="post-info__meta-icon icon-svg_views" width="16" height="9"><use xlink:href="https://habr.com/images/1565957267/common-svg-sprite.svg#eye" /></svg><span class="post-info__meta-counter post-info__meta-counter_small">11,1k</span>
                </span>

                                <a href="https://habr.com/ru/company/funcorp/blog/450120/#comments" class="post-info__meta-item" rel="nofollow">
                                    <svg class="post-info__meta-icon icon-svg_comments" width="14" height="13"><use xlink:href="https://habr.com/images/1565957267/common-svg-sprite.svg#comment" /></svg><span class="post-info__meta-counter post-info__meta-counter_small">25</span>
                                </a>
                            </div>
                        </li>
                        <li class="content-list__item content-list__item_devided post-info">
                            <h3 class="post-info__title">
                                <a href="https://habr.com/ru/company/funcorp/blog/452236/" class="post-info__title">Гармония скриптов внутри Android приложения</a>
                            </h3>
                            <div class="post-info__meta">
                <span class="post-info__meta-item">
                  <svg class="post-info__meta-icon icon-svg_views" width="16" height="9"><use xlink:href="https://habr.com/images/1565957267/common-svg-sprite.svg#eye" /></svg><span class="post-info__meta-counter post-info__meta-counter_small">4,5k</span>
                </span>

                                <a href="https://habr.com/ru/company/funcorp/blog/452236/#comments" class="post-info__meta-item" rel="nofollow">
                                    <svg class="post-info__meta-icon icon-svg_comments" width="14" height="13"><use xlink:href="https://habr.com/images/1565957267/common-svg-sprite.svg#comment" /></svg><span class="post-info__meta-counter post-info__meta-counter_small">2</span>
                                </a>
                            </div>
                        </li>
                        <li class="content-list__item content-list__item_devided post-info">
                            <h3 class="post-info__title">
                                <a href="https://habr.com/ru/company/funcorp/blog/451622/" class="post-info__title">Про подсчёт битов, беззнаковые типы в Kotlin и про ситуации, когда экономия на спичках оправдана</a>
                            </h3>
                            <div class="post-info__meta">
                <span class="post-info__meta-item">
                  <svg class="post-info__meta-icon icon-svg_views" width="16" height="9"><use xlink:href="https://habr.com/images/1565957267/common-svg-sprite.svg#eye" /></svg><span class="post-info__meta-counter post-info__meta-counter_small">5,8k</span>
                </span>

                                <a href="https://habr.com/ru/company/funcorp/blog/451622/#comments" class="post-info__meta-item" rel="nofollow">
                                    <svg class="post-info__meta-icon icon-svg_comments" width="14" height="13"><use xlink:href="https://habr.com/images/1565957267/common-svg-sprite.svg#comment" /></svg><span class="post-info__meta-counter post-info__meta-counter_small">35</span>
                                </a>
                            </div>
                        </li>
                        <li class="content-list__item content-list__item_devided post-info">
                            <h3 class="post-info__title">
                                <a href="https://habr.com/ru/company/funcorp/blog/450868/" class="post-info__title">Видео докладов с FunTech QA-automation meetup</a>
                            </h3>
                            <div class="post-info__meta">
                <span class="post-info__meta-item">
                  <svg class="post-info__meta-icon icon-svg_views" width="16" height="9"><use xlink:href="https://habr.com/images/1565957267/common-svg-sprite.svg#eye" /></svg><span class="post-info__meta-counter post-info__meta-counter_small">5,3k</span>
                </span>

                                <a href="https://habr.com/ru/company/funcorp/blog/450868/#comments" class="post-info__meta-item" rel="nofollow">
                                    <svg class="post-info__meta-icon icon-svg_comments" width="14" height="13"><use xlink:href="https://habr.com/images/1565957267/common-svg-sprite.svg#comment" /></svg><span class="post-info__meta-counter post-info__meta-counter_small">16</span>
                                </a>
                            </div>
                        </li>
                        <li class="content-list__item content-list__item_devided post-info">
                            <h3 class="post-info__title">
                                <a href="https://habr.com/ru/company/funcorp/blog/449012/" class="post-info__title">Мир! Труд! iOS! Да здравствует оффер за 1 день</a>
                            </h3>
                            <div class="post-info__meta">
                <span class="post-info__meta-item">
                  <svg class="post-info__meta-icon icon-svg_views" width="16" height="9"><use xlink:href="https://habr.com/images/1565957267/common-svg-sprite.svg#eye" /></svg><span class="post-info__meta-counter post-info__meta-counter_small">4,2k</span>
                </span>

                                <a href="https://habr.com/ru/company/funcorp/blog/449012/#comments" class="post-info__meta-item" rel="nofollow">
                                    <svg class="post-info__meta-icon icon-svg_comments" width="14" height="13"><use xlink:href="https://habr.com/images/1565957267/common-svg-sprite.svg#comment" /></svg><span class="post-info__meta-counter post-info__meta-counter_small">3</span>
                                </a>
                            </div>
                        </li>
                        <li class="content-list__item content-list__item_devided post-info">
                            <h3 class="post-info__title">
                                <a href="https://habr.com/ru/company/funcorp/blog/447208/" class="post-info__title">Обзор конференции SQA Days EU</a>
                            </h3>
                            <div class="post-info__meta">
                <span class="post-info__meta-item">
                  <svg class="post-info__meta-icon icon-svg_views" width="16" height="9"><use xlink:href="https://habr.com/images/1565957267/common-svg-sprite.svg#eye" /></svg><span class="post-info__meta-counter post-info__meta-counter_small">1,5k</span>
                </span>

                                <a href="https://habr.com/ru/company/funcorp/blog/447208/#comments" class="post-info__meta-item" rel="nofollow">
                                    <svg class="post-info__meta-icon icon-svg_comments" width="14" height="13"><use xlink:href="https://habr.com/images/1565957267/common-svg-sprite.svg#comment" /></svg><span class="post-info__meta-counter post-info__meta-counter_small">0</span>
                                </a>
                            </div>
                        </li>
                        <li class="content-list__item content-list__item_devided post-info">
                            <h3 class="post-info__title">
                                <a href="https://habr.com/ru/company/funcorp/blog/446248/" class="post-info__title">Анонс FunTech QA-automation meetup</a>
                            </h3>
                            <div class="post-info__meta">
                <span class="post-info__meta-item">
                  <svg class="post-info__meta-icon icon-svg_views" width="16" height="9"><use xlink:href="https://habr.com/images/1565957267/common-svg-sprite.svg#eye" /></svg><span class="post-info__meta-counter post-info__meta-counter_small">3,3k</span>
                </span>

                                <a href="https://habr.com/ru/company/funcorp/blog/446248/#comments" class="post-info__meta-item" rel="nofollow">
                                    <svg class="post-info__meta-icon icon-svg_comments" width="14" height="13"><use xlink:href="https://habr.com/images/1565957267/common-svg-sprite.svg#comment" /></svg><span class="post-info__meta-counter post-info__meta-counter_small">1</span>
                                </a>
                            </div>
                        </li>
                    </ul>
                </div>
            </div>
        </div>



    </div>
    </div>



    <script type="text/javascript">
        var userLabel = 'reader';
    </script>

    </div>
    </div>

    <div class="layout__row layout__row_promo-blocks">
        <div class="layout__cell">
            <div class="column-wrapper column-wrapper_bottom column-wrapper_bordered">
                <div class="content_left">


                    <div class="default-block default-block_content">
                        <div class="default-block__header default-block__header_large">
                            <h2 class="default-block__header-title default-block__header-title_large">Самое читаемое</h2>
                        </div>
                        <div class="default-block__content default-block__content_most-read" id="broadcast_tabs_posts">
                            <ul class="toggle-menu toggle-menu__most-read">
                                <li class="toggle-menu__item">
                                    <a href="#broadcast_posts_today" class="toggle-menu__item-link active" rel="nofollow">Сутки</a>
                                </li>
                                <li class="toggle-menu__item">
                                    <a href="#broadcast_posts_week" class="toggle-menu__item-link" rel="nofollow">Неделя</a>
                                </li>
                                <li class="toggle-menu__item">
                                    <a href="#broadcast_posts_month" class="toggle-menu__item-link" rel="nofollow">Месяц</a>
                                </li>
                            </ul>

                            <div class="tabs__content tabs__content_reading" id="broadcast_posts_today">
                                <ul class="content-list content-list_most-read">
                                    <li class="content-list__item content-list__item_devided post-info">
                                        <a href="https://habr.com/ru/post/464265/" class="post-info__title post-info__title_large" onclick="if (typeof ga === 'function') { ga('send', 'event', 'tm_block', 'most_read', 'today'); }">Тирания позитивного мышления угрожает вашему здоровью и счастью</a>
                                        <div class="post-info__meta">
                    <span class="post-info__meta-item">
                      <svg class="post-info__meta-icon icon-svg_rating" width="11" height="13"><use xlink:href="https://habr.com/images/1565957267/common-svg-sprite.svg#arrow-bold" /></svg><span class="post-info__meta-counter">&plus;70</span>
                    </span>
                                            <span class="post-info__meta-item">
                      <svg class="post-info__meta-icon icon-svg_views" width="16" height="9"><use xlink:href="https://habr.com/images/1565957267/common-svg-sprite.svg#eye" /></svg><span class="post-info__meta-counter">39,1k</span>
                    </span>
                                            <span class="post-info__meta-item">
                      <svg class="post-info__meta-icon icon-svg_bookmark-mini" width="8" height="13"><use xlink:href="https://habr.com/images/1565957267/common-svg-sprite.svg#book" /></svg><span class="post-info__meta-counter">173</span>
                    </span>
                                            <a href="https://habr.com/ru/post/464265/#comments" class="post-info__meta-item" rel="nofollow">
                                                <svg class="post-info__meta-icon icon-svg_comments" width="14" height="13"><use xlink:href="https://habr.com/images/1565957267/common-svg-sprite.svg#comment" /></svg><span class="post-info__meta-counter">174</span>
                                            </a>
                                        </div>
                                    </li>
                                    <li class="content-list__item content-list__item_devided post-info">
                                        <a href="https://habr.com/ru/post/464409/" class="post-info__title post-info__title_large" onclick="if (typeof ga === 'function') { ga('send', 'event', 'tm_block', 'most_read', 'today'); }">Как политика 19 века повлияла на расположение дата-центров сегодня</a>
                                        <div class="post-info__meta">
                    <span class="post-info__meta-item">
                      <svg class="post-info__meta-icon icon-svg_rating" width="11" height="13"><use xlink:href="https://habr.com/images/1565957267/common-svg-sprite.svg#arrow-bold" /></svg><span class="post-info__meta-counter">&plus;87</span>
                    </span>
                                            <span class="post-info__meta-item">
                      <svg class="post-info__meta-icon icon-svg_views" width="16" height="9"><use xlink:href="https://habr.com/images/1565957267/common-svg-sprite.svg#eye" /></svg><span class="post-info__meta-counter">23,3k</span>
                    </span>
                                            <span class="post-info__meta-item">
                      <svg class="post-info__meta-icon icon-svg_bookmark-mini" width="8" height="13"><use xlink:href="https://habr.com/images/1565957267/common-svg-sprite.svg#book" /></svg><span class="post-info__meta-counter">44</span>
                    </span>
                                            <a href="https://habr.com/ru/post/464409/#comments" class="post-info__meta-item" rel="nofollow">
                                                <svg class="post-info__meta-icon icon-svg_comments" width="14" height="13"><use xlink:href="https://habr.com/images/1565957267/common-svg-sprite.svg#comment" /></svg><span class="post-info__meta-counter">18</span>
                                            </a>
                                        </div>
                                    </li>
                                    <li class="content-list__item content-list__item_devided post-info">
                                        <a href="https://habr.com/ru/post/464261/" class="post-info__title post-info__title_large" onclick="if (typeof ga === 'function') { ga('send', 'event', 'tm_block', 'most_read', 'today'); }">Что такое API</a>
                                        <div class="post-info__meta">
                    <span class="post-info__meta-item">
                      <svg class="post-info__meta-icon icon-svg_rating" width="11" height="13"><use xlink:href="https://habr.com/images/1565957267/common-svg-sprite.svg#arrow-bold" /></svg><span class="post-info__meta-counter">&plus;26</span>
                    </span>
                                            <span class="post-info__meta-item">
                      <svg class="post-info__meta-icon icon-svg_views" width="16" height="9"><use xlink:href="https://habr.com/images/1565957267/common-svg-sprite.svg#eye" /></svg><span class="post-info__meta-counter">27,2k</span>
                    </span>
                                            <span class="post-info__meta-item">
                      <svg class="post-info__meta-icon icon-svg_bookmark-mini" width="8" height="13"><use xlink:href="https://habr.com/images/1565957267/common-svg-sprite.svg#book" /></svg><span class="post-info__meta-counter">244</span>
                    </span>
                                            <a href="https://habr.com/ru/post/464261/#comments" class="post-info__meta-item" rel="nofollow">
                                                <svg class="post-info__meta-icon icon-svg_comments" width="14" height="13"><use xlink:href="https://habr.com/images/1565957267/common-svg-sprite.svg#comment" /></svg><span class="post-info__meta-counter">9</span>
                                            </a>
                                        </div>
                                    </li>
                                    <li class="content-list__item content-list__item_devided post-info">
                                        <a href="https://habr.com/ru/post/464445/" class="post-info__title post-info__title_large" onclick="if (typeof ga === 'function') { ga('send', 'event', 'tm_block', 'most_read', 'today'); }">Почему быть вегетарианцем на самом деле невозможно</a>
                                        <div class="post-info__meta">
                    <span class="post-info__meta-item">
                      <svg class="post-info__meta-icon icon-svg_rating" width="11" height="13"><use xlink:href="https://habr.com/images/1565957267/common-svg-sprite.svg#arrow-bold" /></svg><span class="post-info__meta-counter">&plus;24</span>
                    </span>
                                            <span class="post-info__meta-item">
                      <svg class="post-info__meta-icon icon-svg_views" width="16" height="9"><use xlink:href="https://habr.com/images/1565957267/common-svg-sprite.svg#eye" /></svg><span class="post-info__meta-counter">19,9k</span>
                    </span>
                                            <span class="post-info__meta-item">
                      <svg class="post-info__meta-icon icon-svg_bookmark-mini" width="8" height="13"><use xlink:href="https://habr.com/images/1565957267/common-svg-sprite.svg#book" /></svg><span class="post-info__meta-counter">44</span>
                    </span>
                                            <a href="https://habr.com/ru/post/464445/#comments" class="post-info__meta-item" rel="nofollow">
                                                <svg class="post-info__meta-icon icon-svg_comments" width="14" height="13"><use xlink:href="https://habr.com/images/1565957267/common-svg-sprite.svg#comment" /></svg><span class="post-info__meta-counter">163</span>
                                            </a>
                                        </div>
                                    </li>
                                    <li class="content-list__item content-list__item_devided post-info">
                                        <a href="https://habr.com/ru/news/t/464271/" class="post-info__title post-info__title_large" onclick="if (typeof ga === 'function') { ga('send', 'event', 'tm_block', 'most_read', 'today'); }">Представлен крупнейший в мире процессор размером 22×22 сантиметра с 400 000 ядрами и 18 ГБ локальной RAM</a>
                                        <div class="post-info__meta">
                    <span class="post-info__meta-item">
                      <svg class="post-info__meta-icon icon-svg_rating" width="11" height="13"><use xlink:href="https://habr.com/images/1565957267/common-svg-sprite.svg#arrow-bold" /></svg><span class="post-info__meta-counter">&plus;40</span>
                    </span>
                                            <span class="post-info__meta-item">
                      <svg class="post-info__meta-icon icon-svg_views" width="16" height="9"><use xlink:href="https://habr.com/images/1565957267/common-svg-sprite.svg#eye" /></svg><span class="post-info__meta-counter">37k</span>
                    </span>
                                            <span class="post-info__meta-item">
                      <svg class="post-info__meta-icon icon-svg_bookmark-mini" width="8" height="13"><use xlink:href="https://habr.com/images/1565957267/common-svg-sprite.svg#book" /></svg><span class="post-info__meta-counter">23</span>
                    </span>
                                            <a href="https://habr.com/ru/news/t/464271/#comments" class="post-info__meta-item" rel="nofollow">
                                                <svg class="post-info__meta-icon icon-svg_comments" width="14" height="13"><use xlink:href="https://habr.com/images/1565957267/common-svg-sprite.svg#comment" /></svg><span class="post-info__meta-counter">100</span>
                                            </a>
                                        </div>
                                    </li>
                                </ul>
                            </div>
                            <div class="tabs__content tabs__content_reading" id="broadcast_posts_week">
                                <ul class="content-list content-list_most-read">
                                    <li class="content-list__item content-list__item_devided post-info">
                                        <a href="https://habr.com/ru/company/dodopizzaio/blog/462747/" class="post-info__title post-info__title_large" onclick="if (typeof ga === 'function') { ga('send', 'event', 'tm_block', 'most_read', 'week'); }">Я написал эту статью, ни разу не посмотрев на клавиатуру</a>
                                        <div class="post-info__meta">
                    <span class="post-info__meta-item">
                      <svg class="post-info__meta-icon icon-svg_rating" width="11" height="13"><use xlink:href="https://habr.com/images/1565957267/common-svg-sprite.svg#arrow-bold" /></svg><span class="post-info__meta-counter">&plus;90</span>
                    </span>
                                            <span class="post-info__meta-item">
                      <svg class="post-info__meta-icon icon-svg_views" width="16" height="9"><use xlink:href="https://habr.com/images/1565957267/common-svg-sprite.svg#eye" /></svg><span class="post-info__meta-counter">65,2k</span>
                    </span>
                                            <span class="post-info__meta-item">
                      <svg class="post-info__meta-icon icon-svg_bookmark-mini" width="8" height="13"><use xlink:href="https://habr.com/images/1565957267/common-svg-sprite.svg#book" /></svg><span class="post-info__meta-counter">488</span>
                    </span>
                                            <a href="https://habr.com/ru/company/dodopizzaio/blog/462747/#comments" class="post-info__meta-item" rel="nofollow">
                                                <svg class="post-info__meta-icon icon-svg_comments" width="14" height="13"><use xlink:href="https://habr.com/images/1565957267/common-svg-sprite.svg#comment" /></svg><span class="post-info__meta-counter">286</span>
                                            </a>
                                        </div>
                                    </li>
                                    <li class="content-list__item content-list__item_devided post-info">
                                        <a href="https://habr.com/ru/post/463799/" class="post-info__title post-info__title_large" onclick="if (typeof ga === 'function') { ga('send', 'event', 'tm_block', 'most_read', 'week'); }">Что мне не нравится в Windows 10</a>
                                        <div class="post-info__meta">
                    <span class="post-info__meta-item">
                      <svg class="post-info__meta-icon icon-svg_rating" width="11" height="13"><use xlink:href="https://habr.com/images/1565957267/common-svg-sprite.svg#arrow-bold" /></svg><span class="post-info__meta-counter">&plus;61</span>
                    </span>
                                            <span class="post-info__meta-item">
                      <svg class="post-info__meta-icon icon-svg_views" width="16" height="9"><use xlink:href="https://habr.com/images/1565957267/common-svg-sprite.svg#eye" /></svg><span class="post-info__meta-counter">62,6k</span>
                    </span>
                                            <span class="post-info__meta-item">
                      <svg class="post-info__meta-icon icon-svg_bookmark-mini" width="8" height="13"><use xlink:href="https://habr.com/images/1565957267/common-svg-sprite.svg#book" /></svg><span class="post-info__meta-counter">120</span>
                    </span>
                                            <a href="https://habr.com/ru/post/463799/#comments" class="post-info__meta-item" rel="nofollow">
                                                <svg class="post-info__meta-icon icon-svg_comments" width="14" height="13"><use xlink:href="https://habr.com/images/1565957267/common-svg-sprite.svg#comment" /></svg><span class="post-info__meta-counter">718</span>
                                            </a>
                                        </div>
                                    </li>
                                    <li class="content-list__item content-list__item_devided post-info">
                                        <a href="https://habr.com/ru/company/mosigra/blog/463773/" class="post-info__title post-info__title_large" onclick="if (typeof ga === 'function') { ga('send', 'event', 'tm_block', 'most_read', 'week'); }">Как устроена защита детей от информации — и феерическая история про то, откуда она сначала взялась (18+)</a>
                                        <div class="post-info__meta">
                    <span class="post-info__meta-item">
                      <svg class="post-info__meta-icon icon-svg_rating" width="11" height="13"><use xlink:href="https://habr.com/images/1565957267/common-svg-sprite.svg#arrow-bold" /></svg><span class="post-info__meta-counter">&plus;154</span>
                    </span>
                                            <span class="post-info__meta-item">
                      <svg class="post-info__meta-icon icon-svg_views" width="16" height="9"><use xlink:href="https://habr.com/images/1565957267/common-svg-sprite.svg#eye" /></svg><span class="post-info__meta-counter">55,7k</span>
                    </span>
                                            <span class="post-info__meta-item">
                      <svg class="post-info__meta-icon icon-svg_bookmark-mini" width="8" height="13"><use xlink:href="https://habr.com/images/1565957267/common-svg-sprite.svg#book" /></svg><span class="post-info__meta-counter">155</span>
                    </span>
                                            <a href="https://habr.com/ru/company/mosigra/blog/463773/#comments" class="post-info__meta-item" rel="nofollow">
                                                <svg class="post-info__meta-icon icon-svg_comments" width="14" height="13"><use xlink:href="https://habr.com/images/1565957267/common-svg-sprite.svg#comment" /></svg><span class="post-info__meta-counter">597</span>
                                            </a>
                                        </div>
                                    </li>
                                    <li class="content-list__item content-list__item_devided post-info">
                                        <a href="https://habr.com/ru/post/464055/" class="post-info__title post-info__title_large" onclick="if (typeof ga === 'function') { ga('send', 'event', 'tm_block', 'most_read', 'week'); }">Изучаем данные, собранные Xiaomi Mi Band за год</a>
                                        <div class="post-info__meta">
                    <span class="post-info__meta-item">
                      <svg class="post-info__meta-icon icon-svg_rating" width="11" height="13"><use xlink:href="https://habr.com/images/1565957267/common-svg-sprite.svg#arrow-bold" /></svg><span class="post-info__meta-counter">&plus;41</span>
                    </span>
                                            <span class="post-info__meta-item">
                      <svg class="post-info__meta-icon icon-svg_views" width="16" height="9"><use xlink:href="https://habr.com/images/1565957267/common-svg-sprite.svg#eye" /></svg><span class="post-info__meta-counter">46,8k</span>
                    </span>
                                            <span class="post-info__meta-item">
                      <svg class="post-info__meta-icon icon-svg_bookmark-mini" width="8" height="13"><use xlink:href="https://habr.com/images/1565957267/common-svg-sprite.svg#book" /></svg><span class="post-info__meta-counter">128</span>
                    </span>
                                            <a href="https://habr.com/ru/post/464055/#comments" class="post-info__meta-item" rel="nofollow">
                                                <svg class="post-info__meta-icon icon-svg_comments" width="14" height="13"><use xlink:href="https://habr.com/images/1565957267/common-svg-sprite.svg#comment" /></svg><span class="post-info__meta-counter">72</span>
                                            </a>
                                        </div>
                                    </li>
                                    <li class="content-list__item content-list__item_devided post-info">
                                        <a href="https://habr.com/ru/post/463957/" class="post-info__title post-info__title_large" onclick="if (typeof ga === 'function') { ga('send', 'event', 'tm_block', 'most_read', 'week'); }">Как развернуть односвязный список на собеседовании</a>
                                        <div class="post-info__meta">
                    <span class="post-info__meta-item">
                      <svg class="post-info__meta-icon icon-svg_rating" width="11" height="13"><use xlink:href="https://habr.com/images/1565957267/common-svg-sprite.svg#arrow-bold" /></svg><span class="post-info__meta-counter">&plus;154</span>
                    </span>
                                            <span class="post-info__meta-item">
                      <svg class="post-info__meta-icon icon-svg_views" width="16" height="9"><use xlink:href="https://habr.com/images/1565957267/common-svg-sprite.svg#eye" /></svg><span class="post-info__meta-counter">46,7k</span>
                    </span>
                                            <span class="post-info__meta-item">
                      <svg class="post-info__meta-icon icon-svg_bookmark-mini" width="8" height="13"><use xlink:href="https://habr.com/images/1565957267/common-svg-sprite.svg#book" /></svg><span class="post-info__meta-counter">155</span>
                    </span>
                                            <a href="https://habr.com/ru/post/463957/#comments" class="post-info__meta-item" rel="nofollow">
                                                <svg class="post-info__meta-icon icon-svg_comments" width="14" height="13"><use xlink:href="https://habr.com/images/1565957267/common-svg-sprite.svg#comment" /></svg><span class="post-info__meta-counter">353</span>
                                            </a>
                                        </div>
                                    </li>
                                </ul>
                            </div>
                            <div class="tabs__content tabs__content_reading" id="broadcast_posts_month">
                                <ul class="content-list content-list_most-read">
                                    <li class="content-list__item content-list__item_devided post-info">
                                        <a href="https://habr.com/ru/post/460901/" class="post-info__title post-info__title_large" onclick="if (typeof ga === 'function') { ga('send', 'event', 'tm_block', 'most_read', 'month'); }">Почему Senior Developer'ы не могут устроиться на работу</a>
                                        <div class="post-info__meta">
                    <span class="post-info__meta-item">
                      <svg class="post-info__meta-icon icon-svg_rating" width="11" height="13"><use xlink:href="https://habr.com/images/1565957267/common-svg-sprite.svg#arrow-bold" /></svg><span class="post-info__meta-counter">&plus;137</span>
                    </span>
                                            <span class="post-info__meta-item">
                      <svg class="post-info__meta-icon icon-svg_views" width="16" height="9"><use xlink:href="https://habr.com/images/1565957267/common-svg-sprite.svg#eye" /></svg><span class="post-info__meta-counter">115k</span>
                    </span>
                                            <span class="post-info__meta-item">
                      <svg class="post-info__meta-icon icon-svg_bookmark-mini" width="8" height="13"><use xlink:href="https://habr.com/images/1565957267/common-svg-sprite.svg#book" /></svg><span class="post-info__meta-counter">318</span>
                    </span>
                                            <a href="https://habr.com/ru/post/460901/#comments" class="post-info__meta-item" rel="nofollow">
                                                <svg class="post-info__meta-icon icon-svg_comments" width="14" height="13"><use xlink:href="https://habr.com/images/1565957267/common-svg-sprite.svg#comment" /></svg><span class="post-info__meta-counter">898</span>
                                            </a>
                                        </div>
                                    </li>
                                    <li class="content-list__item content-list__item_devided post-info">
                                        <a href="https://habr.com/ru/company/medium-isp/blog/461979/" class="post-info__title post-info__title_large" onclick="if (typeof ga === 'function') { ga('send', 'event', 'tm_block', 'most_read', 'month'); }">Дорогая, мы убиваем Интернет</a>
                                        <div class="post-info__meta">
                    <span class="post-info__meta-item">
                      <svg class="post-info__meta-icon icon-svg_rating" width="11" height="13"><use xlink:href="https://habr.com/images/1565957267/common-svg-sprite.svg#arrow-bold" /></svg><span class="post-info__meta-counter">&plus;307</span>
                    </span>
                                            <span class="post-info__meta-item">
                      <svg class="post-info__meta-icon icon-svg_views" width="16" height="9"><use xlink:href="https://habr.com/images/1565957267/common-svg-sprite.svg#eye" /></svg><span class="post-info__meta-counter">113k</span>
                    </span>
                                            <span class="post-info__meta-item">
                      <svg class="post-info__meta-icon icon-svg_bookmark-mini" width="8" height="13"><use xlink:href="https://habr.com/images/1565957267/common-svg-sprite.svg#book" /></svg><span class="post-info__meta-counter">619</span>
                    </span>
                                            <a href="https://habr.com/ru/company/medium-isp/blog/461979/#comments" class="post-info__meta-item" rel="nofollow">
                                                <svg class="post-info__meta-icon icon-svg_comments" width="14" height="13"><use xlink:href="https://habr.com/images/1565957267/common-svg-sprite.svg#comment" /></svg><span class="post-info__meta-counter">932</span>
                                            </a>
                                        </div>
                                    </li>
                                    <li class="content-list__item content-list__item_devided post-info">
                                        <a href="https://habr.com/ru/post/461745/" class="post-info__title post-info__title_large" onclick="if (typeof ga === 'function') { ga('send', 'event', 'tm_block', 'most_read', 'month'); }">Цены российского черного рынка на пробив персональных данных (плюс ответ на ответ Тинькофф Банка)</a>
                                        <div class="post-info__meta">
                    <span class="post-info__meta-item">
                      <svg class="post-info__meta-icon icon-svg_rating" width="11" height="13"><use xlink:href="https://habr.com/images/1565957267/common-svg-sprite.svg#arrow-bold" /></svg><span class="post-info__meta-counter">&plus;250</span>
                    </span>
                                            <span class="post-info__meta-item">
                      <svg class="post-info__meta-icon icon-svg_views" width="16" height="9"><use xlink:href="https://habr.com/images/1565957267/common-svg-sprite.svg#eye" /></svg><span class="post-info__meta-counter">105k</span>
                    </span>
                                            <span class="post-info__meta-item">
                      <svg class="post-info__meta-icon icon-svg_bookmark-mini" width="8" height="13"><use xlink:href="https://habr.com/images/1565957267/common-svg-sprite.svg#book" /></svg><span class="post-info__meta-counter">235</span>
                    </span>
                                            <a href="https://habr.com/ru/post/461745/#comments" class="post-info__meta-item" rel="nofollow">
                                                <svg class="post-info__meta-icon icon-svg_comments" width="14" height="13"><use xlink:href="https://habr.com/images/1565957267/common-svg-sprite.svg#comment" /></svg><span class="post-info__meta-counter">317</span>
                                            </a>
                                        </div>
                                    </li>
                                    <li class="content-list__item content-list__item_devided post-info">
                                        <a href="https://habr.com/ru/post/462969/" class="post-info__title post-info__title_large" onclick="if (typeof ga === 'function') { ga('send', 'event', 'tm_block', 'most_read', 'month'); }">Как власти Казахстана пытаются прикрыть свой провал с внедрением сертификата</a>
                                        <div class="post-info__meta">
                    <span class="post-info__meta-item">
                      <svg class="post-info__meta-icon icon-svg_rating" width="11" height="13"><use xlink:href="https://habr.com/images/1565957267/common-svg-sprite.svg#arrow-bold" /></svg><span class="post-info__meta-counter">&plus;105</span>
                    </span>
                                            <span class="post-info__meta-item">
                      <svg class="post-info__meta-icon icon-svg_views" width="16" height="9"><use xlink:href="https://habr.com/images/1565957267/common-svg-sprite.svg#eye" /></svg><span class="post-info__meta-counter">104k</span>
                    </span>
                                            <span class="post-info__meta-item">
                      <svg class="post-info__meta-icon icon-svg_bookmark-mini" width="8" height="13"><use xlink:href="https://habr.com/images/1565957267/common-svg-sprite.svg#book" /></svg><span class="post-info__meta-counter">52</span>
                    </span>
                                            <a href="https://habr.com/ru/post/462969/#comments" class="post-info__meta-item" rel="nofollow">
                                                <svg class="post-info__meta-icon icon-svg_comments" width="14" height="13"><use xlink:href="https://habr.com/images/1565957267/common-svg-sprite.svg#comment" /></svg><span class="post-info__meta-counter">77</span>
                                            </a>
                                        </div>
                                    </li>
                                    <li class="content-list__item content-list__item_devided post-info">
                                        <a href="https://habr.com/ru/post/463165/" class="post-info__title post-info__title_large" onclick="if (typeof ga === 'function') { ga('send', 'event', 'tm_block', 'most_read', 'month'); }">Telegram наносит ответный удар DPI и блокировкам — Fake TLS</a>
                                        <div class="post-info__meta">
                    <span class="post-info__meta-item">
                      <svg class="post-info__meta-icon icon-svg_rating" width="11" height="13"><use xlink:href="https://habr.com/images/1565957267/common-svg-sprite.svg#arrow-bold" /></svg><span class="post-info__meta-counter">&plus;153</span>
                    </span>
                                            <span class="post-info__meta-item">
                      <svg class="post-info__meta-icon icon-svg_views" width="16" height="9"><use xlink:href="https://habr.com/images/1565957267/common-svg-sprite.svg#eye" /></svg><span class="post-info__meta-counter">100k</span>
                    </span>
                                            <span class="post-info__meta-item">
                      <svg class="post-info__meta-icon icon-svg_bookmark-mini" width="8" height="13"><use xlink:href="https://habr.com/images/1565957267/common-svg-sprite.svg#book" /></svg><span class="post-info__meta-counter">205</span>
                    </span>
                                            <a href="https://habr.com/ru/post/463165/#comments" class="post-info__meta-item" rel="nofollow">
                                                <svg class="post-info__meta-icon icon-svg_comments" width="14" height="13"><use xlink:href="https://habr.com/images/1565957267/common-svg-sprite.svg#comment" /></svg><span class="post-info__meta-counter">198</span>
                                            </a>
                                        </div>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>

                </div>

                <div class="sidebar_right">
                </div>
            </div>

        </div>
    </div>

    <div class="layout__row layout__row_footer-links">
        <div class="layout__cell">
            <div class="footer-grid footer-grid_menu">
                <div class="footer-grid__item footer-block">
                    <h3 class="footer-block__title">
                        Ваш аккаунт
                    </h3>
                    <div class="footer-block__content">
                        <ul class="footer-menu">
                            <li class="footer-menu__item">
                                <a href="https://habr.com/ru/auth/login/" class="footer-menu__item-link">Войти</a>
                            </li>
                            <li class="footer-menu__item">
                                <a href="https://habr.com/ru/auth/register/" class="footer-menu__item-link">Регистрация</a>
                            </li>
                        </ul>
                    </div>
                </div>

                <div class="footer-grid__item footer-block">
                    <h3 class="footer-block__title">Разделы</h3>
                    <div class="footer-block__content">
                        <ul class="footer-menu">
                            <li class="footer-menu__item">
                                <a href="https://habr.com/ru/posts/top/" class="footer-menu__item-link">Публикации</a>
                            </li>
                            <li class="footer-menu__item">
                                <a href="https://habr.com/ru/news/" class="footer-menu__item-link">Новости</a>
                            </li>
                            <li class="footer-menu__item">
                                <a href="https://habr.com/ru/hubs/" class="footer-menu__item-link">Хабы</a>
                            </li>
                            <li class="footer-menu__item">
                                <a href="https://habr.com/ru/companies/" class="footer-menu__item-link">Компании</a>
                            </li>
                            <li class="footer-menu__item">
                                <a href="https://habr.com/ru/users/" class="footer-menu__item-link">Пользователи</a>
                            </li>
                            <li class="footer-menu__item">
                                <a href="https://habr.com/ru/sandbox/" class="footer-menu__item-link">Песочница</a>
                            </li>
                        </ul>
                    </div>
                </div>

                <div class="footer-grid__item footer-block">
                    <h3 class="footer-block__title">Информация</h3>
                    <div class="footer-block__content">
                        <ul class="footer-menu">
                            <li class="footer-menu__item">
                                <a href="https://habr.com/ru/info/help/rules/" class="footer-menu__item-link">Правила</a>
                            </li>
                            <li class="footer-menu__item">
                                <a href="https://habr.com/ru/info/help/" class="footer-menu__item-link">Помощь</a>
                            </li>
                            <li class="footer-menu__item">
                                <a href="https://habr.com/ru/info/topics/madskillz/" class="footer-menu__item-link">Документация</a>
                            </li>
                            <li class="footer-menu__item">
                                <a href="https://account.habr.com/info/agreement/?hl=ru_RU" class="footer-menu__item-link">Соглашение</a>
                            </li>
                            <li class="footer-menu__item">
                                <a href="https://account.habr.com/info/confidential/?hl=ru_RU" class="footer-menu__item-link">Конфиденциальность</a>
                            </li>
                        </ul>
                    </div>
                </div>

                <div class="footer-grid__item footer-block">
                    <h3 class="footer-block__title">Услуги</h3>
                    <div class="footer-block__content">
                        <ul class="footer-menu">
                            <li class="footer-menu__item">
                                <a href="https://tmtm.ru/services/advertising/" target="_blank" class="footer-menu__item-link">Реклама</a>
                            </li>
                            <li class="footer-menu__item">
                                <a href="https://tmtm.ru/services/corpblog/" target="_blank" class="footer-menu__item-link">Тарифы</a>
                            </li>
                            <li class="footer-menu__item">
                                <a href="https://tmtm.ru/services/content/" target="_blank" class="footer-menu__item-link">Контент</a>
                            </li>
                            <li class="footer-menu__item">
                                <a href="https://tmtm.ru/workshops/" target="_blank" class="footer-menu__item-link">Семинары</a>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>

            <div class="footer-misprints">
                Если нашли опечатку в&nbsp;посте, выделите ее&nbsp;и&nbsp;нажмите Ctrl+Enter, чтобы сообщить автору.
            </div>

        </div>
    </div>

    <div class="layout__row layout__row_footer">
        <div class="layout__cell">
            <div class="footer-grid footer">
                <div class="footer-grid__item footer-grid__item_copyright">
                    <span class="footer__copyright">&copy; 2006 &ndash; 2019 «<a href="https://tmtm.ru/" class="footer__link">TM</a>»</span>
                </div>
                <div class="footer-grid__item footer-grid__item_link footer-grid__item_lang">
                    <svg class="icon-svg icon-svg_lang-footer" width="16" height="16">
                        <use xlink:href="https://habr.com/images/1565957267/common-svg-sprite.svg#globus" />
                    </svg>
                    <a href="#" class="footer__link js-show_lang_settings">Настройка языка</a>
                </div>
                <div class="footer-grid__item footer-grid__item_link">
                    <a href="https://habr.com/ru/about/" class="footer__link">О сайте</a>
                </div>
                <div class="footer-grid__item footer-grid__item_link">
                    <a href="https://habr.com/ru/feedback/" class="footer__link">Служба поддержки</a>
                </div>
                <div class="footer-grid__item footer-grid__item_link">
                    <a href="https://m.habr.com/post/460285/?mobile=yes" class="footer__link">Мобильная версия</a>
                </div>

                <div class="footer-grid__item footer-grid__item_social">
                    <ul class="social-icons">
                        <li class="social-icons__item">
                            <a href="https://twitter.com/habr_com"
                               class="social-icons__item-link social-icons__item-link_normal social-icons__item-link_twitter"
                               target="_blank"
                               onclick="if (typeof ga === 'function') { ga('send', 'event', 'footer', 'Social_icons', 'twitter'); }"
                            >
                                <svg class="icon-svg" aria-hidden="true" aria-labelledby="title" version="1.1" role="img" width="24" height="24" viewBox="0 0 24 24"><path d="M17.414 8.642c-.398.177-.826.296-1.276.35.459-.275.811-.71.977-1.229-.43.254-.905.439-1.41.539-.405-.432-.982-.702-1.621-.702-1.227 0-2.222.994-2.222 2.222 0 .174.019.344.058.506-1.846-.093-3.484-.978-4.579-2.322-.191.328-.301.71-.301 1.117 0 .77.392 1.45.988 1.849-.363-.011-.706-.111-1.006-.278v.028c0 1.077.766 1.974 1.782 2.178-.187.051-.383.078-.586.078-.143 0-.282-.014-.418-.04.282.882 1.103 1.525 2.075 1.542-.76.596-1.718.951-2.759.951-.179 0-.356-.01-.53-.031.983.63 2.15.998 3.406.998 4.086 0 6.321-3.386 6.321-6.321l-.006-.287c.433-.314.81-.705 1.107-1.15z"/></svg>
                            </a>
                        </li>
                        <li class="social-icons__item">
                            <a href="https://www.facebook.com/habrahabr.ru"
                               class="social-icons__item-link social-icons__item-link_normal social-icons__item-link_facebook"
                               target="_blank"
                               onclick="if (typeof ga === 'function') { ga('send', 'event', 'footer', 'Social_icons', 'facebook'); }"
                            >
                                <svg class="icon-svg" aria-hidden="true" aria-labelledby="title" version="1.1" role="img" width="24" height="24" viewBox="0 0 24 24"><path d="M14.889 8.608h-1.65c-.195 0-.413.257-.413.6v1.192h2.063v1.698h-2.063v5.102h-1.948v-5.102h-1.766v-1.698h1.766v-1c0-1.434.995-2.6 2.361-2.6h1.65v1.808z"/></svg>
                            </a>
                        </li>
                        <li class="social-icons__item">
                            <a href="https://vk.com/habr"
                               class="social-icons__item-link social-icons__item-link_normal social-icons__item-link_vkontakte"
                               target="_blank"
                               onclick="if (typeof ga === 'function') { ga('send', 'event', 'footer', 'Social_icons', 'vkontakte'); }"
                            >
                                <svg class="icon-svg" aria-hidden="true" aria-labelledby="title" version="1.1" role="img" width="24" height="24" viewBox="0 0 24 24"><path d="M16.066 11.93s1.62-2.286 1.782-3.037c.054-.268-.064-.418-.343-.418h-1.406c-.322 0-.44.139-.537.343 0 0-.76 1.619-1.685 2.64-.297.33-.448.429-.612.429-.132 0-.193-.11-.193-.408v-2.607c0-.365-.043-.472-.343-.472h-2.254c-.172 0-.279.1-.279.236 0 .343.526.421.526 1.352v1.921c0 .386-.022.537-.204.537-.483 0-1.631-1.663-2.274-3.552-.129-.386-.268-.494-.633-.494h-1.406c-.204 0-.354.139-.354.343 0 .375.44 2.114 2.167 4.442 1.159 1.566 2.683 2.414 4.056 2.414.838 0 1.041-.139 1.041-.494v-1.202c0-.301.118-.429.29-.429.193 0 .534.062 1.33.848.945.901 1.01 1.276 1.525 1.276h1.578c.161 0 .311-.075.311-.343 0-.354-.462-.987-1.17-1.738-.29-.386-.762-.805-.912-.998-.215-.226-.151-.354-.001-.59z"/></svg>
                            </a>
                        </li>
                        <li class="social-icons__item">
                            <a href="https://telegram.me/habr_com"
                               class="social-icons__item-link social-icons__item-link_normal social-icons__item-link_telegram"
                               target="_blank"
                               onclick="if (typeof ga === 'function') { ga('send', 'event', 'footer', 'Social_icons', 'telegram'); }"
                            >
                                <svg class="icon-svg" aria-hidden="true" aria-labelledby="title" version="1.1" role="img" width="24" height="24" viewBox="0 0 24 24"><path d="M17.17 7.621l-10.498 3.699c-.169.059-.206.205-.006.286l2.257.904 1.338.536 6.531-4.796s.189.057.125.126l-4.68 5.062-.27.299.356.192 2.962 1.594c.173.093.397.016.447-.199.058-.254 1.691-7.29 1.728-7.447.047-.204-.087-.328-.291-.256zm-6.922 8.637c0 .147.082.188.197.084l1.694-1.522-1.891-.978v2.416z"/></svg>
                            </a>
                        </li>
                        <li class="social-icons__item">
                            <a href="https://www.youtube.com/channel/UCd_sTwKqVrweTt4oAKY5y4w"
                               class="social-icons__item-link social-icons__item-link_normal social-icons__item-link_youtube"
                               target="_blank"
                               onclick="if (typeof ga === 'function') { ga('send', 'event', 'footer', 'Social_icons', 'youtube'); }"
                            >
                                <svg class="icon-svg" aria-hidden="true" aria-labelledby="title" version="1.1" role="img" width="32" height="32" viewBox="0 0 32 32"><path d="M3.2 0h25.6c1.767 0 3.2 1.433 3.2 3.2v25.6c0 1.767-1.433 3.2-3.2 3.2h-25.6c-1.767 0-3.2-1.433-3.2-3.2v-25.6c0-1.767 1.433-3.2 3.2-3.2zm18.133 16l-10.667-5.333v10.667l10.667-5.333z"/></svg>

                            </a>
                        </li>
                        <li class="social-icons__item">
                            <a href="https://zen.yandex.ru/habr"
                               class="social-icons__item-link social-icons__item-link_normal social-icons__item-link_zen"
                               target="_blank"
                               onclick="if (typeof ga === 'function') { ga('send', 'event', 'footer', 'Social_icons', 'zen'); }"
                            >
                                <svg xmlns="http://www.w3.org/2000/svg" class="icon-svg" width="32" height="32" viewBox="0 0 32 32"><path fill="#f00" d="M3.2 0h25.6c1.767 0 3.2 1.433 3.2 3.2v25.6c0 1.767-1.433 3.2-3.2 3.2h-25.6c-1.767 0-3.2-1.433-3.2-3.2v-25.6c0-1.767 1.433-3.2 3.2-3.2z"/><path fill="#d00000" d="M13.661 32h-6.157l12.409-9.537 2.44 2.811-8.693 6.726zm3.782-32h5.883l-10.515 8.777-2.44-2.811 7.072-5.966z"/><path fill="#f8b3b2" d="M10.4 17.879l9.474-7.314 2.441 2.812-9.475 7.314z"/><path fill="#fff" d="M10.417 5.954l11.909 3.897v3.543l-11.909-3.897v-3.543zm0 11.909l11.909 3.897v3.543l-11.909-3.897v-3.543z"/></svg>

                            </a>
                        </li>
                    </ul>
                </div>
            </div>

        </div>
    </div>

    <a href="#" class="layout__elevator hidden" id="scroll_to_top" title="Наверх"  onclick="if (typeof ga === 'function') { ga('send', 'event', 'navigation_button', 'down'); }">
        <svg class="icon-svg icon-svg_scroll-up" width="32" height="32" viewBox="0 0 32 32" aria-hidden="true" version="1.1" role="img"><path d="M16 0C7.164 0 0 7.164 0 16s7.164 16 16 16 16-7.164 16-16S24.836 0 16 0zm8.412 19.523c-.517.512-1.355.512-1.872 0L16 13.516l-6.54 6.01c-.518.51-1.356.51-1.873 0-.516-.513-.517-1.343 0-1.855l7.476-7.326c.517-.512 1.356-.512 1.873 0l7.476 7.327c.516.513.516 1.342 0 1.854z"/></svg>
    </a>
    </div>

    <div class="overlay hidden" id="js-lang_settings">
        <div class="popup">
            <div class="popup__head popup__head_lang-settings">
                <span class="popup__head-title js-popup_title" data-section="1">Настройка языка</span>
                <button type="button" class="btn btn_small btn_popup-close js-hide_lang_settings">
                    <svg class="icon-svg" width="12" height="12">
                        <use xlink:href="https://habr.com/images/1565957267/common-svg-sprite.svg#close" />
                    </svg>
                </button>
            </div>
            <div class="popup__body">
                <form action="/json/settings/i18n/" method="post" class="form form_lang-settings" id="lang-settings-form">
                    <fieldset class="form__fieldset form__fieldset_thin" data-section="2">
                        <legend class="form__legend form__legend_lang-settings js-popup_hl_legend">Интерфейс</legend>
                        <div class="form-field form-field_lang-settings">
                            <label class="radio radio_custom ">
                                <input type="radio" name="hl" id="hl_langs_ru" class="radio__input js-hl_langs" value="ru" checked>
                                <span class="radio__label radio__label_another">Русский</span>
                            </label>
                        </div>
                        <div class="form-field form-field_lang-settings">
                            <label class="radio radio_custom ">
                                <input type="radio" name="hl" id="hl_langs_en" class="radio__input js-hl_langs" value="en" >
                                <span class="radio__label radio__label_another">English</span>
                            </label>
                        </div>
                    </fieldset>

                    <fieldset class="form__fieldset form__fieldset_thin">
                        <legend class="form__legend form__legend_lang-settings js-popup_fl_legend" data-section="3">Язык публикаций</legend>
                        <div class="form-field form-field_lang-settings">
                            <label class="checkbox checkbox_custom">
                                <input type="checkbox" name="fl[]" id="fl_langs_ru" class="checkbox__input js-fl_langs" value="ru" checked>
                                <span class="checkbox__label checkbox__label_another js-popup_feed_ru">Русский</span>
                            </label>
                        </div>
                        <div class="form-field form-field_lang-settings">
                            <label class="checkbox checkbox_custom">
                                <input type="checkbox" name="fl[]" id="fl_langs_en" class="checkbox__input js-fl_langs" value="en" >
                                <span class="checkbox__label checkbox__label_another js-popup_feed_en">Английский</span>
                            </label>
                        </div>
                    </fieldset>

                    <div class="form__footer form__footer_lang-settings">
                        <button type="submit" class="btn btn_blue btn_huge btn_full-width js-popup_save_btn">Сохранить настройки</button>
                    </div>
                </form>
            </div>
        </div>
    </div>



    <script type="text/javascript">
        // global vars
        var g_base_url = 'habr.com/ru';
        var g_base_fullurl = 'https://habr.com/ru/';
        var g_tmid_fullurl = 'https://account.habr.com/';
        var g_is_guest = false;
        var g_show_xpanel = false;
        var g_is_enableShortcuts = '1';
        var g_current_hl = 'ru';
        var g_current_fl = 'ru';

    </script>

    <script src="https://dr.habracdn.net/habrcom/javascripts/1565957267/vendors.bundle.js"></script>
    <script src="https://dr.habracdn.net/habrcom/javascripts/1565957267/main.bundle.js"></script>
    <script type="text/x-mathjax-config">
  MathJax.Hub.Config({
    showProcessingMessages: false,
    showMathMenu: true,
    tex2jax: {
      inlineMath: [['$inline$','$inline$']],
      displayMath: [['$$display$$','$$display$$']],
      processEscapes: true
    },
    MathMenu: {
      showRenderer: true,
      showContext:  true
    }
  });

  MathJax.Extension.Img2jax = {
    PreProcess: function (element) {
      var hasMath = false;
      var images = element.querySelectorAll('[data-tex]');
      for (var i = images.length - 1; i >= 0; i--) {
        var img = images[i];
        var tex = img.alt.replace(/(\r\n|\n|\r)/gm, " ");
        if (tex && tex[0] === '$'){
          var script = document.createElement("script"); script.type = "math/tex";
          hasMath = true;
          if (img.getAttribute('data-tex') == "display"){script.type += ";mode=display"}
          MathJax.HTML.setScript(script, tex.substring(1,tex.length-1));
          img.parentNode.replaceChild(script,img);
        }
      }
    }
  };

  MathJax.Hub.Register.PreProcessor(["PreProcess", MathJax.Extension.Img2jax]);
</script>

    <script type="text/javascript"
            src="https://cdnjs.cloudflare.com/ajax/libs/mathjax/2.7.1/MathJax.js?config=TeX-AMS_SVG-full&locale=ru">
    </script>

    <script type="text/javascript" src="https://dr.habracdn.net/habrcom/javascripts/1565957267/libs/highlight.pack.js"></script>
    <script type="text/javascript" src="https://dr.habracdn.net/habrcom/javascripts/1565957267/libs/highlight.langs.js"></script>


    <script src="https://dr.habracdn.net/habrcom/javascripts/1565957267/check-login.js"></script>

    <script type="text/javascript" src="https://habr.com/ru/viewcount/post/460285/"></script>


    <!-- Yandex.Metrika counter -->
    <script type="text/javascript">
        (function (d, w, c) {
            (w[c] = w[c] || []).push(function() {
                try {
                    if (typeof (_yaparams) != 'undefined') {
                        w.yaCounter24049213 = new Ya.Metrika({id:24049213,
                            webvisor:true,
                            clickmap:true,
                            trackLinks:true,
                            accurateTrackBounce:true,
                            params:_yaparams});
                    } else {
                        w.yaCounter24049213 = new Ya.Metrika({id:24049213,
                            webvisor:true,
                            clickmap:true,
                            trackLinks:true,
                            accurateTrackBounce:true});
                    }

                } catch(e) { }
            });

            var n = d.getElementsByTagName("script")[0],
                s = d.createElement("script"),
                f = function () { n.parentNode.insertBefore(s, n); };
            s.type = "text/javascript";
            s.async = true;
            s.src = (d.location.protocol == "https:" ? "https:" : "http:") + "//mc.yandex.ru/metrika/watch.js";

            if (w.opera == "[object Opera]") {
                d.addEventListener("DOMContentLoaded", f, false);
            } else { f(); }
        })(document, window, "yandex_metrika_callbacks");
    </script>
    <noscript><div><img src="//mc.yandex.ru/watch/24049213" style="position:absolute; left:-9999px;" alt="" /></div></noscript>
    <!-- /Yandex.Metrika counter -->

    <!-- Facebook Pixel Code -->
    <script>
        !function(f,b,e,v,n,t,s)
        {if(f.fbq)return;n=f.fbq=function(){n.callMethod?
            n.callMethod.apply(n,arguments):n.queue.push(arguments)};
            if(!f._fbq)f._fbq=n;n.push=n;n.loaded=!0;n.version='2.0';
            n.queue=[];t=b.createElement(e);t.async=!0;
            t.src=v;s=b.getElementsByTagName(e)[0];
            s.parentNode.insertBefore(t,s)}(window,document,'script',
            'https://connect.facebook.net/en_US/fbevents.js');

        fbq('init', '317458588730613');
        fbq('track', 'PageView');
    </script>
    <noscript>
        <img height="1" width="1" src="https://www.facebook.com/tr?id=317458588730613&ev=PageView&noscript=1"/>
    </noscript>
    <!-- End Facebook Pixel Code -->
</body>
</html>