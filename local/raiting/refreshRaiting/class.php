<?php require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");
use Bitrix\Main\Loader;
class UpdateRating
{
    public function executeComponent($id, $raise)
    {
        Loader::includeModule('iblock');

//        $rating = CIBlockElement::GetList(
//            [],
//            [
//                'IBLOCK_CODE' => 'news',
//                'ID' => $id,
//            ],
//            false,
//            [],
//            [
//                "PROPERTY_RATING"
//            ]
//        )->Fetch();

        $rating  = CIBlockElement::GetProperty(
            1,
                $id,
            [],
            ["CODE" => "RATING"]
      );
        $rating_ar = $rating->Fetch();
            $value =  IntVal($rating_ar["VALUE"]);

        if ($raise == 1) {
            $value = (int)$value + 1;

        }
        elseif ($raise == null) {
            $value = (int)$value;
        }
        else {
            $value = (int)$value - 1;
        }

        CIBlockElement::SetPropertyValuesEx(
            $id,
            false,
            [
                "RATING" => $value,
            ]
        );

        print_r($value);
    }

}
$id = $_POST['id'];
$raise = $_POST['raise'];

(new UpdateRating())->executeComponent($id, $raise);

