<?php require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");
use Bitrix\Highloadblock\HighloadBlockTable as HLBT;
use Bitrix\Main\Loader;
/**
 * Class setnews
 * CBitrixComponent - является оболочкой компонента. Для каждого подключаемого компонента создаётся свой экземпляр класса
 */
class SetNews extends CBitrixComponent
{
    public function executeComponent()
    {
        Loader::includeModule('iblock');
        Loader::includeModule('highloadblock');

        $elements = [];
        $id_hl = 5;

        //запрос основных парметров инфоблока--------------------------------------------------------------------------
        $news = CIBlockElement::GetList(
            [
                'CREATED' => "ASC",
            ],
            [
                'IBLOCK_CODE' => 'news',
                ">=" . 'PROPERTY_RATING' => (int)$_GET['RATING'],
            ],
            false,
            [
                "nPageSize" => 3,
            ],
            ['ID', 'IBLOCK_ID', 'NAME',
                'PREVIEW_PICTURE', 'PREVIEW_TEXT',
                'PREVIEW_PICTURE', 'DETAIL_TEXT',
                'TIMESTAMP_X', 'PROPERTY_RATING',
                'PROPERTY_TAGS', 'CREATED_USER_NAME'
            ]
        );

        $news->NavStart(0);

            $array = [];
        while ($element = $news->GetNextElement()) {
            $array['FIELDS'] = $element->GetFields();
            $array['FIELDS']['PREVIEW_PICTURE'] = CFile::GetPath($array['FIELDS']['PREVIEW_PICTURE']);
            $elements[] = $array;
        }

        foreach ($elements as $element){
            foreach ($element['FIELDS']['PROPERTY_TAGS_VALUE'] as $tag){
                $tagIds[] = $tag;
            }
        }

        $tagIds = array_unique($tagIds);  //собраны уникальные UF_XML_ID в одномерный массив


        $hlblock = HLBT::getById($id_hl)->fetch();
        $entity = HLBT::compileEntity($hlblock);
        $entity_data_class = $entity->getDataClass();

        $rsData = $entity_data_class::getList(array(
            'select' => array('UF_LINK', 'UF_NAME'),
            'filter' => array('UF_XML_ID' => $tagIds),
        ));

        foreach ($tagIds as $tag){
            $tags = [];
            while ($el = $rsData->Fetch()) {
                $tags[$tag] = $el;
            }
            ?><pre><?var_dump($tags); ?></pre><?
        }




//        foreach ($tagIds as $tag){
//
//            while ($el = $rsData->Fetch()) {
//                $tags[] = $el;
//            }
//            $resTags[$tag] = $tags;
//
//        }

        ?><pre><?//var_dump($resTags); ?></pre><?

        $this->arResult['elements'] = $elements;
        $this->arResult['nav'] = $news;

        $this->includeComponentTemplate();

    }
}


